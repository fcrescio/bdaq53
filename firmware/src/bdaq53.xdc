
# ------------------------------------------------------------
#  Copyright (c) SILAB , Physics Institute of Bonn University
# ------------------------------------------------------------

#
#   Constraints for the BDAQ53 PCB with the Mercury+ KX2(160T-2) FPGA board
#

#
#   VCC_IO
#   ------
#   Bank 12: VCCO_12        1.0 - 3.3 V
#   Bank 13: VCCO_13        1.0 - 3.3 V
#   Bank 14: VCCO_0/VCCO_14 1.8 - 2.5 V     ETH-PHY
#   Bank 15: VCCO_15        1.0 - 3.3 V
#   Bank 16: VCCO_16        1.8 - 3.3 V
#

create_clock -period 10.000 -name clkin -add [get_ports clkin]
create_clock -period 8.000 -name rgmii_rxc -add [get_ports rgmii_rxc]
create_clock -period 5.000 -name CLK200_P -add [get_ports CLK200_P]
create_clock -period 6.250 -name MGT_REFCLK0_P -add [get_ports MGT_REFCLK0_P]
create_clock -period 6.250 [get_pins -hier -filter name=~*aurora_64b66b_1lane_wrapper_i*aurora_64b66b_1lane_multi_gt_i*aurora_64b66b_1lane_gtx_inst/gtxe2_i/TXOUTCLK]
create_clock -period 6.250 [get_pins -hier -filter name=~*aurora_64b66b_1lane_wrapper_i*aurora_64b66b_1lane_multi_gt_i*aurora_64b66b_1lane_gtx_inst/gtxe2_i/RXOUTCLK]



create_generated_clock -name i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK -source [get_pins PLLE2_BASE_inst/CLKOUT0] -divide_by 1500 [get_pins i_bdaq53_core/i_clock_divisor_i2c/CLOCK_reg/Q]
create_generated_clock -name rgmii_txc -source [get_pins rgmii/ODDR_inst/C] -divide_by 1 [get_ports rgmii_txc]

set_false_path -from [get_clocks CLK125PLLTX] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks CLK125PLLTX]
set_false_path -from [get_clocks BUS_CLK_PLL] -to [get_clocks rgmii_rxc]
set_false_path -from [get_clocks rgmii_rxc] -to [get_clocks BUS_CLK_PLL]
set_false_path -from [get_clocks CLK200_P] -to [get_clocks MGT_REFCLK0_P]
set_false_path -from [get_clocks MGT_REFCLK0_P] -to [get_clocks CLK200_P]
set_false_path -from [get_clocks i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks i_bdaq53_core/i_clock_divisor_i2c/I2C_CLK]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks CLK200_P]

set_clock_groups -asynchronous -group [get_clocks CLK200_P] -group [get_clocks user_clk_i]
set_clock_groups -asynchronous -group [get_clocks rgmii_rxc] -group [get_clocks CLK125PLLTX]
set_clock_groups -asynchronous -group [get_clocks user_clk_i] -group [get_clocks CLK200_P]

set_property ASYNC_REG true [get_cells sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_0]
set_property ASYNC_REG true [get_cells sitcp/SiTCP/GMII/GMII_TXCNT/irMacPauseExe_1]


#Oscillator 100MHz
set_property PACKAGE_PIN AA4 [get_ports clkin]
set_property IOSTANDARD LVCMOS15 [get_ports clkin]


#Oscillator 200MHz
set_property PACKAGE_PIN AC11 [get_ports CLK200_N]
set_property PACKAGE_PIN AB11 [get_ports CLK200_P]
set_property IOSTANDARD LVDS [get_ports CLK200_*]


#CLK Mux
set_property PACKAGE_PIN D23 [get_ports CLK_SEL]
set_property IOSTANDARD LVCMOS33 [get_ports CLK_SEL]
set_property PULLUP true [get_ports CLK_SEL]


#Reset push button
set_property PACKAGE_PIN G9 [get_ports RESET_BUTTON]
set_property IOSTANDARD LVCMOS25 [get_ports RESET_BUTTON]
set_property PULLUP true [get_ports RESET_BUTTON]


#USER push button
set_property PACKAGE_PIN Y26 [get_ports USER_BUTTON]
set_property IOSTANDARD LVCMOS25 [get_ports USER_BUTTON]
set_property PULLUP true [get_ports USER_BUTTON]


#SITCP
set_property SLEW FAST [get_ports mdio_phy_mdc]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_phy_mdc]
set_property PACKAGE_PIN B25 [get_ports mdio_phy_mdc]

set_property SLEW FAST [get_ports mdio_phy_mdio]
set_property IOSTANDARD LVCMOS33 [get_ports mdio_phy_mdio]
set_property PACKAGE_PIN B26 [get_ports mdio_phy_mdio]

set_property SLEW FAST [get_ports phy_rst_n]
set_property IOSTANDARD LVCMOS33 [get_ports phy_rst_n]
#M20 is routed to Connector C. The Ethernet PHY on th KX2 board has NO reset connection to an FPGA pin
set_property PACKAGE_PIN M20 [get_ports phy_rst_n]

set_property IOSTANDARD LVCMOS33 [get_ports rgmii_rxc]
set_property PACKAGE_PIN G22 [get_ports rgmii_rxc]

set_property IOSTANDARD LVCMOS33 [get_ports rgmii_rx_ctl]
set_property PACKAGE_PIN F23 [get_ports rgmii_rx_ctl]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[0]}]
set_property PACKAGE_PIN H23 [get_ports {rgmii_rxd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[1]}]
set_property PACKAGE_PIN H24 [get_ports {rgmii_rxd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[2]}]
set_property PACKAGE_PIN J21 [get_ports {rgmii_rxd[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_rxd[3]}]
set_property PACKAGE_PIN H22 [get_ports {rgmii_rxd[3]}]

set_property SLEW FAST [get_ports rgmii_txc]
set_property IOSTANDARD LVCMOS33 [get_ports rgmii_txc]
set_property PACKAGE_PIN K23 [get_ports rgmii_txc]

set_property SLEW FAST [get_ports rgmii_tx_ctl]
set_property IOSTANDARD LVCMOS33 [get_ports rgmii_tx_ctl]
set_property PACKAGE_PIN J23 [get_ports rgmii_tx_ctl]

set_property SLEW FAST [get_ports {rgmii_txd[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[0]}]
set_property PACKAGE_PIN J24 [get_ports {rgmii_txd[0]}]
set_property SLEW FAST [get_ports {rgmii_txd[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[1]}]
set_property PACKAGE_PIN J25 [get_ports {rgmii_txd[1]}]
set_property SLEW FAST [get_ports {rgmii_txd[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[2]}]
set_property PACKAGE_PIN L22 [get_ports {rgmii_txd[2]}]
set_property SLEW FAST [get_ports {rgmii_txd[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {rgmii_txd[3]}]
set_property PACKAGE_PIN K22 [get_ports {rgmii_txd[3]}]


# Aurora related signals
set_property PACKAGE_PIN H5 [get_ports MGT_REFCLK0_N]
set_property PACKAGE_PIN H6 [get_ports MGT_REFCLK0_P]
set_property PACKAGE_PIN K6 [get_ports MGT_REFCLK1_P]
set_property PACKAGE_PIN K5 [get_ports MGT_REFCLK1_N]

set_property PACKAGE_PIN R4 [get_ports {MGT_RX_P[0]}]
set_property PACKAGE_PIN R3 [get_ports {MGT_RX_N[0]}]
set_property PACKAGE_PIN N4 [get_ports {MGT_RX_P[1]}]
set_property PACKAGE_PIN N3 [get_ports {MGT_RX_N[1]}]
set_property PACKAGE_PIN L4 [get_ports {MGT_RX_P[2]}]
set_property PACKAGE_PIN L3 [get_ports {MGT_RX_N[2]}]

#set_property LOC GTXE2_CHANNEL_X0Y3 [get_cells  aurora_64b66b_1lane_block_i/aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/aurora_64b66b_1lane_multi_gt_i/aurora_64b66b_1lane_gtx_inst/gtxe2_i]
#set_property LOC GTXE2_CHANNEL_X0Y3 [get_cells  {name=~*aurora_64b66b_1lane_wrapper_i*aurora_64b66b_1lane_multi_gt_i*aurora_64b66b_1lane_gtx_inst/gtxe2_i}]


# DP_ML ("DP2") connected to SelectIOs
set_property PACKAGE_PIN A18 [get_ports DP_GPIO_LANE0_P]
set_property PACKAGE_PIN A19 [get_ports DP_GPIO_LANE0_N]
set_property PACKAGE_PIN C19 [get_ports DP_GPIO_LANE1_P]
set_property PACKAGE_PIN B19 [get_ports DP_GPIO_LANE1_N]
set_property PACKAGE_PIN E18 [get_ports DP_GPIO_LANE2_P]
set_property PACKAGE_PIN D18 [get_ports DP_GPIO_LANE2_N]
#set_property PACKAGE_PIN B17 [get_ports DP_GPIO_LANE3_P]
#set_property PACKAGE_PIN A17 [get_ports DP_GPIO_LANE3_N]
set_property IOSTANDARD LVDS_25 [get_ports DP_GPIO_LANE*]
#set_property PACKAGE_PIN C16 [get_ports DP_GPIO_AUX_P]
#set_property PACKAGE_PIN B16 [get_ports DP_GPIO_AUX_N]
#set_property IOSTANDARD LVDS_25 [get_ports DP_GPIO_AUX*]


# DP_ML ("DP1") connected to MGTs
set_property PACKAGE_PIN G20 [get_ports DP_ML_AUX_N]
set_property PACKAGE_PIN H19 [get_ports DP_ML_AUX_P]
set_property IOSTANDARD LVDS_25 [get_ports DP_ML_AUX*]


# Displayport RESET signals 0:DP1, 1:DP3, 2:DP4, 3:DP5, 4:mDP
#set_property PACKAGE_PIN G10 [get_ports GPIO_RESET[4]]
#set_property PACKAGE_PIN K18 [get_ports {GPIO_RESET[3]}]
#set_property PACKAGE_PIN L17 [get_ports {GPIO_RESET[2]}]
#set_property PACKAGE_PIN H11 [get_ports {GPIO_RESET[1]}]
#set_property PACKAGE_PIN H12 [get_ports {GPIO_RESET[0]}]
#set_property IOSTANDARD LVCMOS25 [get_ports GPIO_RESET*]
#set_property PULLUP TRUE [get_ports GPIO_RESET*]

# Displayport VDD_SENSE signals 0:DP1, 1:DP3, 2:DP4, 3:DP5
set_property PACKAGE_PIN V22 [get_ports {GPIO_SENSE[3]}]
set_property PACKAGE_PIN W23 [get_ports {GPIO_SENSE[2]}]
set_property PACKAGE_PIN W24 [get_ports {GPIO_SENSE[1]}]
set_property PACKAGE_PIN AD26 [get_ports {GPIO_SENSE[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports GPIO_SENSE*]
set_property PULLUP TRUE [get_ports GPIO_SENSE*]


# Debug LEDs
#LED 0..3 are onboard LEDs: Bank 32, 33 running at 1.5 V)
set_property PACKAGE_PIN U9 [get_ports {LED[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[0]}]
set_property PACKAGE_PIN V12 [get_ports {LED[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[1]}]
set_property PACKAGE_PIN V13 [get_ports {LED[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[2]}]
set_property PACKAGE_PIN W13 [get_ports {LED[3]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[3]}]
#LED 4..7 are LEDs on the BDAQ53 base board. They have pull-ups to 1.8 V.
set_property PACKAGE_PIN E21 [get_ports {LED[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[4]}]
set_property PACKAGE_PIN E22 [get_ports {LED[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[5]}]
set_property PACKAGE_PIN D21 [get_ports {LED[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[6]}]
set_property PACKAGE_PIN C22 [get_ports {LED[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {LED[7]}]
set_property SLEW SLOW [get_ports LED*]

set_property PACKAGE_PIN AB21 [get_ports LEMO_TX0]
set_property PACKAGE_PIN AD25 [get_ports LEMO_TX1]
set_property IOSTANDARD LVCMOS25 [get_ports LEMO_TX*]
set_property SLEW FAST [get_ports LEMO_TX*]


# PMOD
#set_property IOSTANDARD LVDS_25 [get_ports {PMOD[10]}]
#set_property PACKAGE_PIN AC23 [get_ports {PMOD[10]}]
#set_property PACKAGE_PIN AC24 [get_ports {PMOD[9]}]
#set_property IOSTANDARD LVDS_25 [get_ports {PMOD[9]}]
#set_property PACKAGE_PIN W25  [get_ports {PMOD[8]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {PMOD[8]}]
#set_property PACKAGE_PIN W26  [get_ports {PMOD[7]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {PMOD[7]}]
#THERE IS NO PMOD[6]
#THERE IS NO PMOD[5]
#set_property PACKAGE_PIN U26  [get_ports {PMOD[4]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {PMOD[4]}]
#set_property PACKAGE_PIN V26  [get_ports {PMOD[3]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {PMOD[3]}]
#set_property PACKAGE_PIN AE25 [get_ports {PMOD[2]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {PMOD[2]}]
#set_property PACKAGE_PIN AD24 [get_ports {PMOD[1]}]
#set_property IOSTANDARD LVCMOS25 [get_ports {PMOD[1]}]
#THERE IS NO PMOD[0]


# I2C pins
set_property PACKAGE_PIN L23 [get_ports I2C_SCL]
set_property PACKAGE_PIN C24 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS33 [get_ports I2C_*]
set_property SLEW SLOW [get_ports I2C_*]


# EEPROM (SPI for SiTCP)
set_property PACKAGE_PIN C26 [get_ports EEPROM_CS]
set_property PACKAGE_PIN D26 [get_ports EEPROM_SK]
set_property PACKAGE_PIN B21 [get_ports EEPROM_DI]
set_property PACKAGE_PIN C21 [get_ports EEPROM_DO]
set_property IOSTANDARD LVCMOS33 [get_ports EEPROM_*]


set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks MGT_REFCLK0_P]
set_false_path -from [get_clocks MGT_REFCLK0_P] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]]
set_false_path -from [get_clocks -of_objects [get_pins PLLE2_BASE_inst/CLKOUT0]] -to [get_clocks -of_objects [get_pins i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/clock_module_i/mmcm_adv_inst/CLKOUT0]]

set_property CONFIG_MODE SPIx4 [current_design]
set_property LOC GTXE2_CHANNEL_X0Y3 [get_cells i_bdaq53_core/i_aurora_rx/i_aurora_rx_core/aurora_frame/aurora_64b66b_1lane_block_i/aurora_64b66b_1lane_i/inst/aurora_64b66b_1lane_wrapper_i/aurora_64b66b_1lane_multi_gt_i/aurora_64b66b_1lane_gtx_inst/gtxe2_i]
set_property PACKAGE_PIN J4 [get_ports {MGT_RX_P[3]}]
set_property PACKAGE_PIN J3 [get_ports {MGT_RX_N[3]}]


set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 33 [current_design]
