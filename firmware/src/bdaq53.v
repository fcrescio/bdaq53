/**
 * ------------------------------------------------------------
 * Copyright (c) SILAB , Physics Institute of Bonn University
 * ------------------------------------------------------------
 */

`timescale 1ns / 1ps
`default_nettype wire

/**
*   MMC3 FPGA:   XC7K160T-1-FBG676-C    (select xc7k160tfbg676-1)
*   BDAQ53 FPGA: XC7K160T-2-FFG676-I    (select xc7k160tffg676-2)    Quad-SPI flash: S25FL512S
*   KC705 FPGA:  XC7K325T-2-FFG900-C    (select xc7k325tffg900-2)    Quad-SPI flash: N25Q128A13BSF40F (select MT25QL128)
**/

/** SELECT THE HARDWARE PLATFORM **/
//`define BDAQ53
`define GENESYS2


/** SELECT THE AURORA LANE CONFIGURATION **/
`define AURORA_1LANE
//`define AURORA_2LANE
//`define AURORA_4LANE


// Use GMII instead of RGMII for KC705
`ifdef KC705
    `define GMII
`elsif GENESYS2
    `include "utils/rgmii_io.v"
`elsif BDAQ53
     `include "utils/rgmii_io.v"
`endif


`include "utils/fifo_32_to_8.v"
`include "utils/rbcp_to_bus.v"
`include "utils/clock_divider.v"

`ifndef SYNTHESIS	`include "bdaq53_core.v"	`endif


module bdaq53(
    input wire RESET_BUTTON,
    //input wire SMA_MGT_REFCLK_N, SMA_MGT_REFCLK_P,
    input wire MGT_REFCLK0_P, MGT_REFCLK0_N,
    `ifdef KC705
        input wire CLKSi570_P, CLKSi570_N,
        output wire SI5324_RST,
        input wire Si5324_N, Si5324_P,
        input wire GPIO_SW_C,
    `elsif GENESYS2
        input wire USER_BUTTON,
    
    `elsif BDAQ53
        input wire USER_BUTTON,
        input wire MGT_REFCLK1_P, MGT_REFCLK1_N,
        input wire clkin,
        output wire CLK_SEL,

        // DP_ML ("DP2") connected to SelectIOs
        input wire DP_GPIO_LANE0_P, DP_GPIO_LANE0_N,
        input wire DP_GPIO_LANE1_P, DP_GPIO_LANE1_N,
        input wire DP_GPIO_LANE2_P, DP_GPIO_LANE2_N,
        input wire DP_GPIO_LANE3_P, DP_GPIO_LANE3_N,
//        output wire DP_GPIO_AUX_P, DP_GPIO_AUX_N,

        // DP_ML connected to MGTs
        output wire DP_ML_AUX_P, DP_ML_AUX_N,
/*
        // DP_SL connected to MGTs
        output wire DP_SL1_AUX_P, DP_SL1_AUX_N,
        output wire DP_SL2_AUX_P, DP_SL2_AUX_N,
        output wire DP_SL3_AUX_P, DP_SL3_AUX_N,
*/
        // Displayport "slow control" signals
//        output wire [3:0] GPIO_RESET,
        input wire [3:0]  GPIO_SENSE,

    `endif

    input wire CLK200_P, CLK200_N,

    // Ethernet
    `ifdef GMII
        inout  wire       mdio_phy_mdio,
        output wire       mdio_phy_mdc,
        output wire       phy_rst_n,
        input  wire       gmii_crs,
        input  wire       gmii_col,
        output wire [7:0] gmii_txd,
        output wire       gmii_tx_clk,
        output wire       gmii_tx_en,
        output wire       gmii_tx_er,
        input  wire [7:0] gmii_rxd,
        input  wire       gmii_rx_clk,
        input  wire       gmii_rx_er,
        input  wire       gmii_rx_dv,
    `else
        output wire [3:0] rgmii_txd,
        output wire       rgmii_tx_ctl,
        output wire       rgmii_txc,
        input  wire [3:0] rgmii_rxd,
        input  wire       rgmii_rx_ctl,
        input  wire       rgmii_rxc,
        output wire       mdio_phy_mdc,
        inout  wire       mdio_phy_mdio,
        output wire       phy_rst_n,
    `endif
/*
    // DDR3 Memory Interface
    output wire [14:0]ddr3_addr,
    output wire [2:0] ddr3_ba,
    output wire       ddr3_cas_n,
    output wire       ddr3_ras_n,
    output wire       ddr3_ck_n, ddr3_ck_p,
    output wire [0:0] ddr3_cke,
    output wire       ddr3_reset_n,
    inout  wire [7:0] ddr3_dq,
    inout  wire       ddr3_dqs_n, ddr3_dqs_p,
    output wire [0:0] ddr3_dm,
    output wire       ddr3_we_n,
    output wire [0:0] ddr3_cs_n,
    output wire [0:0] ddr3_odt,
*/
    // Aurora lanes
    input wire [3:0]  MGT_RX_P, MGT_RX_N,
   // output wire [3:0]  MGT_TX_P, MGT_TX_N,

    // CMD encoder
    `ifdef KC705
        output wire USER_SMA_P, USER_SMA_N,
    `elsif GENESYS2
        output wire DP_AUX_P, DP_AUX_N,
    `elsif BDAQ53
        output wire LEMO_TX0, LEMO_TX1,    //debug signals
    `endif

    `ifdef BDAQ53
        // SiTCP EEPROM
        output wire EEPROM_CS, EEPROM_SK, EEPROM_DI,
        input wire EEPROM_DO,
    `endif

    `ifdef KC705   output wire SM_FAN_PWM,   `endif

    // I2C bus
    `ifndef GENESYS2
    inout wire I2C_SDA,
    output wire I2C_SCL,
    `endif

    // Debug signals
    output wire [7:0] LED

);


wire USER_CLK,TX_OUT_CLK, OUT_CLK;     // ------------------------------------------------------------------> DEBUGGING

wire RESET_N;
wire RST;
wire AURORA_RESET;
wire BUS_CLK_PLL, CLK200PLL, CLK125PLLTX, CLK125PLLTX90, CLK125PLLRX, CLK_INIT_PLL;
wire PLL_FEEDBACK, LOCKED;
wire clkin1;
wire REFCLK1_OUT;   // Internal copy of the MGT ref clock

// Clock OUTPUT driver
wire MGT_REF_CLK_P, MGT_REF_CLK_N;
OBUFDS #(
    .IOSTANDARD("LVDS_25"),    // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate
) i_OBUFDS_MGT_REF (
    .O(MGT_REF_CLK_P),      // Diff_p output (connect directly to top-level port)
    .OB(MGT_REF_CLK_N),     // Diff_n output (connect directly to top-level port)
    .I(REFCLK1_OUT)         // Buffer input
);

wire TX_OUT_CLK_P, TX_OUT_CLK_N;
OBUFDS #(
    .IOSTANDARD("LVDS_25"),    // Specify the output I/O standard
    .SLEW("FAST")           // Specify the output slew rate
) i_OBUFDS_TX_CLK (
    .O(TX_OUT_CLK_P),     // Diff_p output (connect directly to top-level port)
    .OB(TX_OUT_CLK_N),    // Diff_n output (connect directly to top-level port)
    .I(TX_OUT_CLK)          // Buffer input
);

`ifdef KC705
    assign RESET_N = ~RESET_BUTTON;
    assign AURORA_RESET = GPIO_SW_C;
    wire CLK_SEL = 0;   // dummy signal for the ref clk mux only present on the mmc3 hardware
    localparam CONF_CLKIN1_PERIOD = 5.000;
    localparam CONF_CLKFBOUT_MULT = 5;

    wire clkin_buf;
    IBUFDS #(
      .DIFF_TERM("FALSE"),       // Differential Termination
      .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE"
      .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
    ) IBUFDS_inst (
      .O(clkin_buf),  // Buffer output
      .I(CLK200_P),  // Diff_p buffer input (connect directly to top-level port)
      .IB(CLK200_N) // Diff_n buffer input (connect directly to top-level port)
    );

    assign clkin1 = clkin_buf;
    assign SI5324_RST = !RST;

`elsif GENESYS2
    assign RESET_N = RESET_BUTTON;
    assign AURORA_RESET = USER_BUTTON;
    localparam CONF_CLKIN1_PERIOD = 5.000;
    localparam CONF_CLKFBOUT_MULT = 5;

    wire clkin_buf;
    IBUFDS #(
      .DIFF_TERM("FALSE"),       // Differential Termination
      .IBUF_LOW_PWR("TRUE"),     // Low power="TRUE", Highest performance="FALSE"
      .IOSTANDARD("DEFAULT")     // Specify the input I/O standard
    ) IBUFDS_inst (
      .O(clkin_buf),  // Buffer output
      .I(CLK200_P),  // Diff_p buffer input (connect directly to top-level port)
      .IB(CLK200_N) // Diff_n buffer input (connect directly to top-level port)
    );

    assign clkin1 = clkin_buf;
`elsif BDAQ53
    assign RESET_N = RESET_BUTTON;
    assign AURORA_RESET = ~USER_BUTTON;
    localparam CONF_CLKIN1_PERIOD = 10.000;
    localparam CONF_CLKFBOUT_MULT = 10;
    assign clkin1 = clkin;
`endif

PLLE2_BASE #(
    .BANDWIDTH("OPTIMIZED"),    // OPTIMIZED, HIGH, LOW
    .CLKFBOUT_MULT(CONF_CLKFBOUT_MULT),         // Multiply value for all CLKOUT, (2-64)
    .CLKFBOUT_PHASE(0.0),       // Phase offset in degrees of CLKFB, (-360.000-360.000).
    .CLKIN1_PERIOD(CONF_CLKIN1_PERIOD),     // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).

    .CLKOUT0_DIVIDE(5),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT0_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT0_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT1_DIVIDE(6),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT1_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT1_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT2_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT2_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT2_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT3_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT3_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT3_PHASE(90.0),       // Phase offset for CLKOUT0 (-360.000-360.000).

    .CLKOUT4_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
    .CLKOUT4_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT4_PHASE(-5.625),     // Phase offset for CLKOUT0 (-360.000-360.000).     // resolution is 45°/[CLKOUTn_DIVIDE]
    //-65 -> 0?; - 45 -> 39;  -25 -> 100; -5 -> 0;

    .CLKOUT5_DIVIDE(10),        // Divide amount for CLKOUT0 (1-128)
    .CLKOUT5_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
    .CLKOUT5_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

    .DIVCLK_DIVIDE(1),          // Master division value, (1-56)
    .REF_JITTER1(0.0),          // Reference input jitter in UI, (0.000-0.999).
    .STARTUP_WAIT("FALSE")      // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
 )
 PLLE2_BASE_inst (              // VCO = 1 GHz
     .CLKOUT0(CLK200PLL),     // 142.857 MHz
     .CLKOUT1(BUS_CLK_PLL),       // 200 MHz for MIG
     .CLKOUT2(CLK125PLLTX),     // 125 MHz
     .CLKOUT3(CLK125PLLTX90),
     .CLKOUT4(CLK125PLLRX),
     .CLKOUT5(CLK_INIT_PLL),    // 100 MHz init clock for the Aurora core

     .CLKFBOUT(PLL_FEEDBACK),

     .LOCKED(LOCKED),           // 1-bit output: LOCK

     // Input clock
     .CLKIN1(clkin1),

     // Control Ports
     .PWRDWN(0),
     .RST(!RESET_N),

     // Feedback
     .CLKFBIN(PLL_FEEDBACK)
 );
 
 `ifdef GENESYS2
 wire CLK160PLL2;
 wire CLK200PLL2;
 wire PLL_FEEDBACK2;
 PLLE2_BASE #(
     .BANDWIDTH("OPTIMIZED"),    // OPTIMIZED, HIGH, LOW
     .CLKFBOUT_MULT(8),         // Multiply value for all CLKOUT, (2-64)
     .CLKFBOUT_PHASE(0.0),       // Phase offset in degrees of CLKFB, (-360.000-360.000).
     .CLKIN1_PERIOD(5),     // Input clock period in ns to ps resolution (i.e. 33.333 is 30 MHz).
 
     .CLKOUT0_DIVIDE(10),         // Divide amount for CLKOUT0 (1-128)
     .CLKOUT0_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
     .CLKOUT0_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000).

     .CLKOUT1_DIVIDE(8),         // Divide amount for CLKOUT0 (1-128)
     .CLKOUT1_DUTY_CYCLE(0.5),   // Duty cycle for CLKOUT0 (0.001-0.999).
     .CLKOUT1_PHASE(0.0),        // Phase offset for CLKOUT0 (-360.000-360.000). 
 
     .DIVCLK_DIVIDE(1),          // Master division value, (1-56)
     .REF_JITTER1(0.0),          // Reference input jitter in UI, (0.000-0.999).
     .STARTUP_WAIT("FALSE")      // Delay DONE until PLL Locks, ("TRUE"/"FALSE")
  )
  PLLE2_BASE_inst_gen2 (              // VCO = 1 GHz
      .CLKOUT0(CLK160PLL2),     // 160 MHz
      .CLKOUT1(CLK200PLL2),     // 200 MHz
 
      .CLKFBOUT(PLL_FEEDBACK2),
 
      //.LOCKED(LOCKED),           // 1-bit output: LOCK
 
      // Input clock
      .CLKIN1(CLK200PLL),
 
      // Control Ports
      .PWRDWN(0),
      .RST(!RESET_N),
 
      // Feedback
      .CLKFBIN(PLL_FEEDBACK2)
  );


 `endif

assign RST = !RESET_N | !LOCKED;

wire BUS_CLK, CLK125TX, CLK125TX90, CLK125RX, CLKCMD, CLK_INIT;
BUFG BUFG_inst_BUS_CKL (.O(BUS_CLK), .I(BUS_CLK_PLL) );
BUFG BUFG_inst_INIT_CKL (.O(CLK_INIT), .I(CLK_INIT_PLL) );

`ifdef GMII
    BUFG BUFG_inst_CLK125RX   ( .O(CLK125RX),   .I(gmii_rx_clk)   );
    assign CLK125TX = CLK125RX;
    assign gmii_tx_clk = CLK125TX;
`else
    BUFG BUFG_inst_CLK125RX   ( .O(CLK125RX),   .I(rgmii_rxc)     );
    BUFG BUFG_inst_CLK125TX   ( .O(CLK125TX),   .I(CLK125PLLTX)   );
    BUFG BUFG_inst_CLK125TX90 ( .O(CLK125TX90), .I(CLK125PLLTX90) );
`endif

BUFG BUFG_inst_CLKCMDENC  ( .O(CLKCMD), .I(REFCLK1_OUT) );

wire RX_CLK_IN_P, RX_CLK_IN_N;
`ifdef GENESYS2
wire INIT_CLK_IN;  // 50...200 MHz
`else
wire INIT_CLK_IN_P, INIT_CLK_IN_N;  // 50...200 MHz
`endif

`ifdef KC705
    assign INIT_CLK_IN_P = CLKSi570_P;
    assign INIT_CLK_IN_N = CLKSi570_N;
    assign RX_CLK_IN_P   = Si5324_P;
    assign RX_CLK_IN_N   = Si5324_N;
`elsif GENESYS2
    assign INIT_CLK_IN = CLK200PLL2;        // from 200 MHz oscillator
//    assign RX_CLK_IN_P   = CLK160PLL2;   // from 160 MHz
    assign RX_CLK_IN_P   = MGT_REFCLK0_P;
    assign RX_CLK_IN_N   = MGT_REFCLK0_N;
`elsif BDAQ53
    assign INIT_CLK_IN_P = CLK200_P;        // from 200 MHz oscillator
    assign INIT_CLK_IN_N = CLK200_N;
    assign RX_CLK_IN_P   = MGT_REFCLK0_P;   // from Si570
    assign RX_CLK_IN_N   = MGT_REFCLK0_N;
`endif


// -------  MDIO interface  ------- //
wire   mdio_gem_mdc;
wire   mdio_gem_i;
wire   mdio_gem_o;
wire   mdio_gem_t;
wire   link_status;
wire  [1:0] clock_speed;
wire   duplex_status;

`ifndef GMII
    // -------  RGMII interface  ------- //
    wire   gmii_tx_clk;
    wire   gmii_tx_en;
    wire  [7:0] gmii_txd;
    wire   gmii_tx_er;
    wire   gmii_crs;
    wire   gmii_col;
    wire   gmii_rx_clk;
    wire   gmii_rx_dv;
    wire  [7:0] gmii_rxd;
    wire   gmii_rx_er;

    rgmii_io rgmii
    (
        .rgmii_txd(rgmii_txd),
        .rgmii_tx_ctl(rgmii_tx_ctl),
        .rgmii_txc(rgmii_txc),

        .rgmii_rxd(rgmii_rxd),
        .rgmii_rx_ctl(rgmii_rx_ctl),

        .gmii_txd_int(gmii_txd),        // Internal gmii_txd signal.
        .gmii_tx_en_int(gmii_tx_en),
        .gmii_tx_er_int(gmii_tx_er),
        .gmii_col_int(gmii_col),
        .gmii_crs_int(gmii_crs),
        .gmii_rxd_reg(gmii_rxd),        // RGMII double data rate data valid.
        .gmii_rx_dv_reg(gmii_rx_dv),    // gmii_rx_dv_ibuf registered in IOBs.
        .gmii_rx_er_reg(gmii_rx_er),    // gmii_rx_er_ibuf registered in IOBs.

        .eth_link_status(link_status),
        .eth_clock_speed(clock_speed),
        .eth_duplex_status(duplex_status),

                                        // FOllowing are generated by DCMs
        .tx_rgmii_clk_int(CLK125TX),    // Internal RGMII transmitter clock.
        .tx_rgmii_clk90_int(CLK125TX90),// Internal RGMII transmitter clock w/ 90 deg phase
        .rx_rgmii_clk_int(CLK125RX),    // Internal RGMII receiver clock

        .reset(!phy_rst_n)
    );
`endif

// Instantiate tri-state buffer for MDIO
IOBUF i_iobuf_mdio(
    .O(mdio_gem_i),
    .IO(mdio_phy_mdio),
    .I(mdio_gem_o),
    .T(mdio_gem_t));


// -------  SiTCP module  ------- //
wire TCP_OPEN_ACK, TCP_CLOSE_REQ;
wire TCP_RX_WR, TCP_TX_WR;
wire TCP_TX_FULL, TCP_ERROR;
wire [7:0] TCP_RX_DATA, TCP_TX_DATA;
reg [10:0] TCP_RX_WC_11B;
wire RBCP_ACK, RBCP_ACT, RBCP_WE, RBCP_RE;
wire [7:0] RBCP_WD, RBCP_RD;
wire [31:0] RBCP_ADDR;
wire SiTCP_RST;
wire EEPROM_CS_int, EEPROM_SK_int, EEPROM_DI_int, EEPROM_DO_int;

`ifdef KC705
    localparam CONST_phy_addr = 5'd7;
`elsif GENESYS2
    localparam CONST_phy_addr = 5'd1;
`elsif BDAQ53 // connect the physical EEPROM pins only for the BDAQ53 board
    localparam CONST_phy_addr = 5'd3;
    assign EEPROM_CS = EEPROM_CS_int;
    assign EEPROM_SK = EEPROM_SK_int;
    assign EEPROM_DI = EEPROM_DI_int;
    assign EEPROM_DO_int = EEPROM_DO;
`endif

WRAP_SiTCP_GMII_XC7K_32K sitcp(
    .CLK(BUS_CLK)               ,    // in    : System Clock >129MHz
    .RST(RST)                   ,    // in    : System reset
    // Configuration parameters
    .FORCE_DEFAULTn(1'b0)       ,    // in    : Load default parameters
    .EXT_IP_ADDR(32'hc0a80a10)  ,    // in    : IP address[31:0] //192.168.10.16
    .EXT_TCP_PORT(16'd24)       ,    // in    : TCP port #[15:0]
    .EXT_RBCP_PORT(16'd4660)    ,    // in    : RBCP port #[15:0]
    .PHY_ADDR(CONST_phy_addr)   ,    // in    : PHY-device MIF address[4:0]
    // EEPROM
    .EEPROM_CS(EEPROM_CS_int)   ,    // out    : Chip select
    .EEPROM_SK(EEPROM_SK_int)   ,    // out    : Serial data clock
    .EEPROM_DI(EEPROM_DI_int)   ,    // out    : Serial write data
    .EEPROM_DO(EEPROM_DO_int)   ,    // in     : Serial read data
    // user data, intialial values are stored in the EEPROM, 0xFFFF_FC3C-3F
    .USR_REG_X3C()              ,    // out    : Stored at 0xFFFF_FF3C
    .USR_REG_X3D()              ,    // out    : Stored at 0xFFFF_FF3D
    .USR_REG_X3E()              ,    // out    : Stored at 0xFFFF_FF3E
    .USR_REG_X3F()              ,    // out    : Stored at 0xFFFF_FF3F
    // MII interface
    .GMII_RSTn(phy_rst_n)       ,    // out    : PHY reset
    .GMII_1000M(1'b1)           ,    // in    : GMII mode (0:MII, 1:GMII)
    // TX
    .GMII_TX_CLK(CLK125TX)      ,    // in    : Tx clock
    .GMII_TX_EN(gmii_tx_en)     ,    // out    : Tx enable
    .GMII_TXD(gmii_txd)         ,    // out    : Tx data[7:0]
    .GMII_TX_ER(gmii_tx_er)     ,    // out    : TX error
    // RX
    .GMII_RX_CLK(CLK125RX)      ,    // in    : Rx clock
    .GMII_RX_DV(gmii_rx_dv)     ,    // in    : Rx data valid
    .GMII_RXD(gmii_rxd)         ,    // in    : Rx data[7:0]
    .GMII_RX_ER(gmii_rx_er)     ,    // in    : Rx error
    .GMII_CRS(gmii_crs)         ,    // in    : Carrier sense
    .GMII_COL(gmii_col)         ,    // in    : Collision detected
    // Management IF
    .GMII_MDC(mdio_phy_mdc)     ,    // out    : Clock for MDIO
    .GMII_MDIO_IN(mdio_gem_i)   ,    // in    : Data
    .GMII_MDIO_OUT(mdio_gem_o)  ,    // out    : Data
    .GMII_MDIO_OE(mdio_gem_t)   ,    // out    : MDIO output enable
    // User I/F
    .SiTCP_RST(SiTCP_RST)       ,    // out    : Reset for SiTCP and related circuits
    // TCP connection control
    .TCP_OPEN_REQ(1'b0)         ,    // in    : Reserved input, shoud be 0
    .TCP_OPEN_ACK(TCP_OPEN_ACK) ,    // out    : Acknowledge for open (=Socket busy)
    .TCP_ERROR(TCP_ERROR)       ,    // out    : TCP error, its active period is equal to MSL
    .TCP_CLOSE_REQ(TCP_CLOSE_REQ)   ,    // out    : Connection close request
    .TCP_CLOSE_ACK(TCP_CLOSE_REQ)   ,    // in    : Acknowledge for closing
    // FIFO I/F
    .TCP_RX_WC({5'b1,TCP_RX_WC_11B}),    // in    : Rx FIFO write count[15:0] (Unused bits should be set 1)
    .TCP_RX_WR(TCP_RX_WR)       ,    // out    : Write enable
    .TCP_RX_DATA(TCP_RX_DATA)   ,    // out    : Write data[7:0]
    .TCP_TX_FULL(TCP_TX_FULL)   ,    // out    : Almost full flag
    .TCP_TX_WR(TCP_TX_WR)       ,    // in    : Write enable
    .TCP_TX_DATA(TCP_TX_DATA)   ,    // in    : Write data[7:0]
    // RBCP
    .RBCP_ACT(RBCP_ACT)         ,    // out    : RBCP active
    .RBCP_ADDR(RBCP_ADDR)       ,    // out    : Address[31:0]
    .RBCP_WD(RBCP_WD)           ,    // out    : Data[7:0]
    .RBCP_WE(RBCP_WE)           ,    // out    : Write enable
    .RBCP_RE(RBCP_RE)           ,    // out    : Read enable
    .RBCP_ACK(RBCP_ACK)         ,    // in    : Access acknowledge
    .RBCP_RD(RBCP_RD)                // in    : Read data[7:0]
);


// -------  BUS SIGNALING  ------- //
wire BUS_WR, BUS_RD, BUS_RST;
wire [31:0] BUS_ADD;
wire [31:0] BUS_DATA;
assign BUS_RST = SiTCP_RST;

rbcp_to_bus irbcp_to_bus(
    .BUS_RST(BUS_RST),
    .BUS_CLK(BUS_CLK),

    .RBCP_ACT(RBCP_ACT),
    .RBCP_ADDR(RBCP_ADDR),
    .RBCP_WD(RBCP_WD),
    .RBCP_WE(RBCP_WE),
    .RBCP_RE(RBCP_RE),
    .RBCP_ACK(RBCP_ACK),
    .RBCP_RD(RBCP_RD),

    .BUS_WR(BUS_WR),
    .BUS_RD(BUS_RD),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA[7:0])
);



// ----- bdaq53 CORE ----- //
wire [31:0] AURORA_RX_FIFO_DATA;
wire FIFO_EMPTY, FIFO_FULL;
wire FIFO_WRITE;
wire BUS_BYTE_ACCESS;
wire DUT_RESET, RESET_TB;
wire CMD_P, CMD_N, CMD_DATA, CMD_WRITING;
wire AUR_RX_LANE_UP;
wire AUR_RX_CHANNEL_UP;
wire AUR_PLL_LOCKED;

bdaq53_core i_bdaq53_core(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),    // full 32 bit bus
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_BYTE_ACCESS(BUS_BYTE_ACCESS),

    // Clocks from oscillators and mux select
    `ifdef GENESYS2
    .CLK200(INIT_CLK_IN),
    .CLK100(CLK_INIT_PLL),
    `else
    .CLK200_P(INIT_CLK_IN_P), .CLK200_N(INIT_CLK_IN_N),
    `endif
    .RX_CLK_IN_P(RX_CLK_IN_P), .RX_CLK_IN_N(RX_CLK_IN_N),
    `ifdef KC705    .CLK_SEL(),
    `elsif GENESYS2    .CLK_SEL(),
    `elsif BDAQ53   .CLK_SEL(CLK_SEL),
    `endif

    // PLL
    .CLK_CMD(CLKCMD),
    .REFCLK1_OUT(REFCLK1_OUT),
    .TX_OUT_CLK(TX_OUT_CLK),

    // Aurora lanes
    .MGT_RX_P(MGT_RX_P), .MGT_RX_N(MGT_RX_N),

    // CMD encoder
    .CMD_WRITING(CMD_WRITING),
    .CMD_DATA(CMD_DATA),
    .CMD_P(CMD_P), .CMD_N(CMD_N),

    `ifndef KC705
        // Displayport "slow control" signals
//        .GPIO_RESET(GPIO_RESET),
//        .GPIO_SENSE(GPIO_SENSE),
    `endif

    // Debug signals
    `ifdef KC705    .DEBUG_TX0(), .DEBUG_TX1(),
    `elsif BDAQ53   .DEBUG_TX0(), .DEBUG_TX1(),
    `elsif GENESYS2   .DEBUG_TX0(), .DEBUG_TX1(),
    `endif

    .RX_LANE_UP(AUR_RX_LANE_UP),
    .RX_CHANNEL_UP(AUR_RX_CHANNEL_UP),
    .PLL_LOCKED(AUR_PLL_LOCKED),

    // I2C bus
    `ifdef GENESYS2
        .I2C_SDA(), .I2C_SCL(),
    `else
    .I2C_SDA(I2C_SDA), .I2C_SCL(I2C_SCL),
    `endif

    // FIFO cpontrol signals (TX FIFO of DAQ)
    .FIFO_DATA(AURORA_RX_FIFO_DATA),
    .FIFO_WRITE(FIFO_WRITE),
    .FIFO_EMPTY(FIFO_EMPTY),
    .FIFO_FULL(FIFO_FULL),

    .BX_CLK_EN(),
    .DUT_RESET(DUT_RESET),
    .RESET_TB(RESET_TB),
    .AURORA_RESET(AURORA_RESET)
);


// Command encoder outputs
`ifdef KC705
    assign USER_SMA_P = CMD_P;
    assign USER_SMA_N = CMD_N;
`elsif GENESYS2
    assign DP_AUX_P = CMD_P;
    assign DP_AUX_N = CMD_N;
`elsif BDAQ53
    assign DP_ML_AUX_P = CMD_P;
    assign DP_ML_AUX_N = CMD_N;
`endif


// ----- General FIFO ----- //
fifo_32_to_8 #(.DEPTH(128*1024)) i_data_fifo (
    .RST(BUS_RST),
    .CLK(BUS_CLK),

    .WRITE(FIFO_WRITE), //input
    .READ(TCP_TX_WR),       //input
    .DATA_IN(AURORA_RX_FIFO_DATA),

    .FULL(FIFO_FULL),
    .EMPTY(FIFO_EMPTY),
    .DATA_OUT(TCP_TX_DATA)
    );


// draining the TCP-FIFO
assign TCP_TX_WR = !TCP_TX_FULL && !FIFO_EMPTY;

always@ (posedge BUS_CLK) begin
        // RX FIFO word counter
    if(TCP_RX_WR) begin
        TCP_RX_WC_11B <= TCP_RX_WC_11B + 1;
    end
    else begin
        TCP_RX_WC_11B <= 11'd0;
    end
end


wire [7:0] LED_int;
assign LED_int = {TCP_OPEN_ACK, TCP_RX_WR, TCP_TX_WR, FIFO_FULL, AUR_RX_LANE_UP, AUR_RX_CHANNEL_UP, AUR_PLL_LOCKED, LOCKED};
`ifdef KC705
    assign LED = LED_int;
`elsif GENESYS2
    assign LED = LED_int;
`elsif BDAQ53
    assign LED = ~LED_int;  // BDAQ uses active-low LED drivers
`endif

// DEBUGGING PORTS
`ifdef KC705
//    assign XADC_GPIO_23_P = MGT_REF_CLK_P;
//    assign XADC_GPIO_23_N = MGT_REF_CLK_N;
//    assign XADC_GPIO_01_P = TX_OUT_CLK_P;
//    assign XADC_GPIO_01_N = TX_OUT_CLK_N;
`elsif BDAQ53
//    assign DP_GPIO_LANE3_P = MGT_REF_CLK_P;
//    assign DP_GPIO_LANE3_N = MGT_REF_CLK_N;
//    assign PMOD[10] = TX_OUT_CLK_P;
//    assign PMOD[9]  = TX_OUT_CLK_N;
    assign LEMO_TX0 = 0;
    assign LEMO_TX1 = 0;
`endif


// Reduce the fan speed (KC705)
`ifdef KC705
    clock_divider #(
    .DIVISOR(142860)
    ) i_clock_divisor_fan (
        .CLK(BUS_CLK),
        .RESET(1'b0),
        .CE(),
        .CLOCK(SM_FAN_PWM)
    );
`endif

endmodule
