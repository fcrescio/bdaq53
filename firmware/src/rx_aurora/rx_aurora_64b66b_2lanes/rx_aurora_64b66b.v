/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ps/1ps
`default_nettype none

`include "rx_aurora_64b66b_2lanes_kintex/rx_aurora_64b66b_core.v"
`include "rrp_arbiter/rrp_arbiter.v"


module rx_aurora_64b66b
#(
    parameter BASEADDR = 16'h0000,
    parameter HIGHADDR = 16'h0000,
    parameter ABUSWIDTH = 16,
    parameter IDENTYFIER = 0
)(
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    inout wire [7:0] BUS_DATA,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    input wire [3:0] RXP, RXN,
    output wire RX_CLK,
    input wire RX_CLK_IN_P, RX_CLK_IN_N,
    input wire INIT_CLK_IN_P, INIT_CLK_IN_N,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,

    output wire RX_READY,
    output wire LOST_ERROR,

    input wire AURORA_RESET

);

//localparam AURORA_NR_OF_LANES = 4;     //as defined in python test
`ifdef AURORA_4LANE
    localparam AURORA_NR_OF_LANES = 4;
`elsif AURORA_2LANE
    localparam AURORA_NR_OF_LANES = 2;
`elsif AURORA_1LANE
    localparam AURORA_NR_OF_LANES = 1;
`endif

wire IP_RD, IP_WR;
wire [ABUSWIDTH-1:0] IP_ADD;
wire [7:0] IP_DATA_IN;
wire [7:0] IP_DATA_OUT;

bus_to_ip #( .BASEADDR(BASEADDR), .HIGHADDR(HIGHADDR), .ABUSWIDTH(ABUSWIDTH) ) i_bus_to_ip
(
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),

    .IP_RD(IP_RD),
    .IP_WR(IP_WR),
    .IP_ADD(IP_ADD),
    .IP_DATA_IN(IP_DATA_IN),
    .IP_DATA_OUT(IP_DATA_OUT)
);


localparam ARB_WIDTH = 2;

wire FIFO_USER_K_EMPTY;
wire [31:0] FIFO_USER_K_DATA;

wire ARB_READY_OUT;
wire ARB_WRITE_OUT;
wire ARB_GRANT_USER_K, ARB_GRANT_DATA;
wire [31:0] ARB_DATA_OUT, FIFO_DATA_DATA;
wire FIFO_DATA_EMPTY;

// map FIFO signals to arbiter
assign ARB_READY_OUT = FIFO_READ;
assign FIFO_DATA = {ARB_GRANT_USER_K, ARB_DATA_OUT[23:0]};    //  8 bits of additional IDs, flags etc | 8 bits frame information | 16 bits of data,
assign FIFO_EMPTY = ~ARB_WRITE_OUT;

rrp_arbiter
#(
    .WIDTH(ARB_WIDTH)
) i_rrp_arbiter
(
    .RST(BUS_RST),
    .CLK(BUS_CLK),

    .WRITE_REQ({!FIFO_USER_K_EMPTY, !FIFO_DATA_EMPTY}),      // indicate will of writing data
    .HOLD_REQ({2'b0}),                                  // wait for writing for given stream (priority)
    .DATA_IN({FIFO_USER_K_DATA, FIFO_DATA_DATA}),     // incoming data for arbitration
    .READ_GRANT({ARB_GRANT_USER_K, ARB_GRANT_DATA}),  // indicate to stream that data has been accepted

    .READY_OUT(ARB_READY_OUT),              // indicates ready for outgoing stream
    .WRITE_OUT(ARB_WRITE_OUT),              // indicates will of write to outgoing stream
    .DATA_OUT(ARB_DATA_OUT)                 // outgoing data stream
    );


rx_aurora_64b66b_core
#(
    .ABUSWIDTH(ABUSWIDTH),
    .IDENTYFIER(IDENTYFIER),
    .AURORA_LANES(AURORA_NR_OF_LANES)
) i_aurora_rx_core
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(IP_ADD),
    .BUS_DATA_IN(IP_DATA_IN),
    .BUS_RD(IP_RD),
    .BUS_WR(IP_WR),
    .BUS_DATA_OUT(IP_DATA_OUT),

    .RXP(RXP), .RXN(RXN),
    .RX_CLK(RX_CLK),
    .RX_CLK_IN_P(RX_CLK_IN_P), .RX_CLK_IN_N(RX_CLK_IN_N),
    .INIT_CLK_IN_P(INIT_CLK_IN_P), .INIT_CLK_IN_N(INIT_CLK_IN_N),

    .FIFO_READ(ARB_GRANT_DATA),   // FIFO_READ
    .FIFO_EMPTY(FIFO_DATA_EMPTY),
    .FIFO_DATA(FIFO_DATA_DATA),

    .USERK_FIFO_READ(ARB_GRANT_USER_K),
    .USERK_FIFO_EMPTY(FIFO_USER_K_EMPTY),
    .USERK_FIFO_DATA(FIFO_USER_K_DATA),

    .LOST_ERROR(LOST_ERROR),
    .RX_READY(RX_READY),
    .AURORA_RESET(AURORA_RESET)
);

endmodule
