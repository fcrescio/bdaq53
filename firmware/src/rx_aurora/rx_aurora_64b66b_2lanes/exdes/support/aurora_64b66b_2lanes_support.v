
 ///////////////////////////////////////////////////////////////////////////////
 //
 // Project:  Aurora 64B/66B
 // Company:  Xilinx
 //
 //
 //
 // (c) Copyright 2008 - 2009 Xilinx, Inc. All rights reserved.
 //
 // This file contains confidential and proprietary information
 // of Xilinx, Inc. and is protected under U.S. and
 // international copyright and other intellectual property
 // laws.
 //
 // DISCLAIMER
 // This disclaimer is not a license and does not grant any
 // rights to the materials distributed herewith. Except as
 // otherwise provided in a valid license issued to you by
 // Xilinx, and to the maximum extent permitted by applicable
 // law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
 // WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
 // AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
 // BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
 // INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
 // (2) Xilinx shall not be liable (whether in contract or tort,
 // including negligence, or under any other theory of
 // liability) for any loss or damage of any kind or nature
 // related to, arising under or in connection with these
 // materials, including for any direct, or any indirect,
 // special, incidental, or consequential loss or damage
 // (including loss of data, profits, goodwill, or any type of
 // loss or damage suffered as a result of any action brought
 // by a third party) even if such damage or loss was
 // reasonably foreseeable or Xilinx had been advised of the
 // possibility of the same.
 //
 // CRITICAL APPLICATIONS
 // Xilinx products are not designed or intended to be fail-
 // safe, or for use in any application requiring fail-safe
 // performance, such as life-support or safety devices or
 // systems, Class III medical devices, nuclear facilities,
 // applications related to the deployment of airbags, or any
 // other applications that could lead to death, personal
 // injury, or severe property or environmental damage
 // (individually and collectively, "Critical
 // Applications"). Customer assumes the sole risk and
 // liability of any use of Xilinx products in Critical
 // Applications, subject only to applicable laws and
 // regulations governing limitations on product liability.
 //
 // THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
 // PART OF THIS FILE AT ALL TIMES.
 
 //
 ///////////////////////////////////////////////////////////////////////////////
 //
 //
 //  Description:  
 //                
 //                
 ///////////////////////////////////////////////////////////////////////////////
 `timescale 1 ns / 10 ps

   (* core_generation_info = "aurora_64b66b_2lanes,aurora_64b66b_v11_0_1,{c_aurora_lanes=2,c_column_used=left,c_gt_clock_1=GTXQ0,c_gt_clock_2=None,c_gt_loc_1=1,c_gt_loc_10=X,c_gt_loc_11=X,c_gt_loc_12=X,c_gt_loc_13=X,c_gt_loc_14=X,c_gt_loc_15=X,c_gt_loc_16=X,c_gt_loc_17=X,c_gt_loc_18=X,c_gt_loc_19=X,c_gt_loc_2=X,c_gt_loc_20=X,c_gt_loc_21=X,c_gt_loc_22=X,c_gt_loc_23=X,c_gt_loc_24=X,c_gt_loc_25=X,c_gt_loc_26=X,c_gt_loc_27=X,c_gt_loc_28=X,c_gt_loc_29=X,c_gt_loc_3=2,c_gt_loc_30=X,c_gt_loc_31=X,c_gt_loc_32=X,c_gt_loc_33=X,c_gt_loc_34=X,c_gt_loc_35=X,c_gt_loc_36=X,c_gt_loc_37=X,c_gt_loc_38=X,c_gt_loc_39=X,c_gt_loc_4=X,c_gt_loc_40=X,c_gt_loc_41=X,c_gt_loc_42=X,c_gt_loc_43=X,c_gt_loc_44=X,c_gt_loc_45=X,c_gt_loc_46=X,c_gt_loc_47=X,c_gt_loc_48=X,c_gt_loc_5=X,c_gt_loc_6=X,c_gt_loc_7=X,c_gt_loc_8=X,c_gt_loc_9=X,c_lane_width=4,c_line_rate=1.6,c_gt_type=gtx,c_qpll=false,c_nfc=false,c_nfc_mode=IMM,c_refclk_frequency=80.0,c_simplex=true,c_simplex_mode=RX,c_stream=false,c_ufc=false,c_user_k=true,flow_mode=None,interface_mode=Framing,dataflow_config=RX-only_Simplex}" *) 
(* DowngradeIPIdentifiedWarnings="yes" *)
 module aurora_64b66b_2lanes_support 
  (
     // RX AXI Interface 
       output [127:0]    m_axi_rx_tdata, 
       output [15:0]     m_axi_rx_tkeep, 
       output            m_axi_rx_tlast,
       output            m_axi_rx_tvalid, 


     // RX User K interface
       output  [127:0]    m_axi_rx_user_k_tdata, 
       output             m_axi_rx_user_k_tvalid,
 
 
     // GTX Serial I/O
       input   [0:1]      rxp, 
       input   [0:1]      rxn, 
 
     // Error Detection Interface
       output             rx_hard_err, 
       output             rx_soft_err, 
 
     // Status
       output             rx_channel_up, 
       output  [0:1]      rx_lane_up, 
     // System Interface
       output              init_clk_out, 
       output              user_clk_out, 
       output              reset2fc, 
       input              reset_pb, 
       input              gt_rxcdrovrden_in, 
       input              power_down, 
       input              pma_init, 
       input    drp_clk_in,
 
     //-------------------- AXI4-Lite Interface -------------------------------
 
     //-------------------- Write Address Channel --------------------------
       input  [31:0]   s_axi_awaddr,
       input  [31:0]   s_axi_araddr,
       input  [31:0]   s_axi_wdata,
       input  [3:0]   s_axi_wstrb,
       input           s_axi_awvalid, 
       input           s_axi_rready, 
       input  [31:0]   s_axi_awaddr_lane1,
       input  [31:0]   s_axi_araddr_lane1,
       input  [31:0]   s_axi_wdata_lane1,
       input  [3:0]   s_axi_wstrb_lane1,
       input           s_axi_awvalid_lane1, 
       input           s_axi_rready_lane1, 
       output  [31:0]  s_axi_rdata,
       output          s_axi_awready,
       output          s_axi_wready, 
       output          s_axi_bvalid, 
       output  [1:0]   s_axi_bresp, 
       output  [1:0]   s_axi_rresp, 
       input           s_axi_bready, 
       output          s_axi_arready, 
       output          s_axi_rvalid, 
       input           s_axi_arvalid, 
       input           s_axi_wvalid, 
       output  [31:0]  s_axi_rdata_lane1,
       output          s_axi_awready_lane1,
       output          s_axi_wready_lane1, 
       output          s_axi_bvalid_lane1, 
       output  [1:0]   s_axi_bresp_lane1, 
       output  [1:0]   s_axi_rresp_lane1, 
       input           s_axi_bready_lane1, 
       output          s_axi_arready_lane1, 
       output          s_axi_rvalid_lane1, 
       input           s_axi_arvalid_lane1, 
       input           s_axi_wvalid_lane1, 
       input               init_clk_p,
       input               init_clk_n,
       output              link_reset_out, 
       output              gt_pll_lock, 
       output              sys_reset_out,

     // GTX Reference Clock Interface
 
       input              gt_refclk1_p, 
       input              gt_refclk1_n, 
 

       //------------------------ RX Margin Analysis Ports ------------------------
       output          gt0_eyescandataerror_out,
       input           gt0_eyescanreset_in,
       input           gt0_eyescantrigger_in,
       //------------------- Receive Ports - RX Equalizer Ports -------------------
       input           gt0_rxcdrhold_in,
       input           gt0_rxlpmhfovrden_in,
       input           gt0_rxdfeagchold_in,
       input           gt0_rxdfeagcovrden_in,
       input           gt0_rxdfelfhold_in,
       input           gt0_rxdfelpmreset_in,
       input           gt0_rxlpmlfklovrden_in,
       output [6:0]    gt0_rxmonitorout_out,
       input  [1:0]    gt0_rxmonitorsel_in,
       input           gt0_rxlpmen_in,
       input           gt0_rxpmareset_in,
       input           gt0_rxpcsreset_in,
       input           gt0_rxbufreset_in,
       input  [2:0]    gt0_rxprbssel_in,    
       input           gt0_rxprbscntreset_in,
       output          gt0_rxprbserr_out,
       output          gt0_rxresetdone_out,
       output [2:0]    gt0_rxbufstatus_out,
       output  [7:0]   gt0_dmonitorout_out,
       output          gt0_cplllock_out,
       //------------------------ RX Margin Analysis Ports ------------------------
       output          gt1_eyescandataerror_out,
       input           gt1_eyescanreset_in,
       input           gt1_eyescantrigger_in,
       //------------------- Receive Ports - RX Equalizer Ports -------------------
       input           gt1_rxcdrhold_in,
       input           gt1_rxlpmhfovrden_in,
       input           gt1_rxdfeagchold_in,
       input           gt1_rxdfeagcovrden_in,
       input           gt1_rxdfelfhold_in,
       input           gt1_rxdfelpmreset_in,
       input           gt1_rxlpmlfklovrden_in,
       output [6:0]    gt1_rxmonitorout_out,
       input  [1:0]    gt1_rxmonitorsel_in,
       input           gt1_rxlpmen_in,
       input           gt1_rxpmareset_in,
       input           gt1_rxpcsreset_in,
       input           gt1_rxbufreset_in,
       input  [2:0]    gt1_rxprbssel_in,    
       input           gt1_rxprbscntreset_in,
       output          gt1_rxprbserr_out,
       output          gt1_rxresetdone_out,
       output [2:0]    gt1_rxbufstatus_out,
       output  [7:0]   gt1_dmonitorout_out,
       output          gt1_cplllock_out,
       output          gt_qplllock,
       output                 mmcm_not_locked_out,
       output              tx_out_clk
 
 );
 
 
 //***********************************Port Declarations*******************************
 
 
 //************************External Register Declarations*****************************
 
 
 //********************************Wire Declarations**********************************
 
     //System Interface
       wire                 mmcm_not_locked_i ; 
       wire                 powerdown_i ; 
 
       wire                  pma_init_i; 
       wire                  pma_init_sync; 
 
  
     // clock
       (* KEEP = "TRUE" *) wire               user_clk_i; 
       (* KEEP = "TRUE" *) wire               sync_clk_i; 
       (* KEEP = "TRUE" *) wire               GTXQ0_left_i; 
       (* KEEP = "TRUE" *) wire               INIT_CLK_i  /* synthesis syn_keep = 1 */; 
       wire               drp_clk_i;
       wire    [8:0] drpaddr_in_i;
       wire    [15:0]     drpdi_in_i;
       wire    [15:0]     drpdo_out_i; 
       wire               drprdy_out_i; 
       wire               drpen_in_i; 
       wire               drpwe_in_i; 
       wire    [8:0] drpaddr_in_lane1_i;
       wire    [15:0]     drpdi_in_lane1_i;
       wire    [15:0]     drpdo_out_lane1_i; 
       wire               drprdy_out_lane1_i; 
       wire               drpen_in_lane1_i; 
       wire               drpwe_in_lane1_i; 
       wire    [7:0]      qpll_drpaddr_in='b0;
       wire    [15:0]     qpll_drpdi_in='b0;
       wire    [15:0]     qpll_drpdo_out; 
       wire               qpll_drprdy_out; 
       wire               qpll_drpen_in='b0; 
       wire               qpll_drpwe_in='b0; 
       wire    [31:0]     s_axi_awaddr_i;
       wire    [31:0]     s_axi_araddr_i;
       wire    [31:0]     s_axi_wdata_i;
       wire    [3:0]     s_axi_wstrb_i;
       wire    [31:0]     s_axi_rdata_i;
       wire               s_axi_awvalid_i; 
       wire               s_axi_arvalid_i; 
       wire               s_axi_wvalid_i; 
       wire               s_axi_rvalid_i; 
       wire               s_axi_bvalid_i; 
       wire    [1:0]      s_axi_bresp_i; 
       wire    [1:0]      s_axi_rresp_i; 
       wire               s_axi_bready_i; 
       wire               s_axi_awready_i; 
       wire               s_axi_arready_i; 
       wire               s_axi_wready_i; 
       wire               s_axi_rready_i; 
       wire    [31:0]     s_axi_awaddr_lane1_i;
       wire    [31:0]     s_axi_araddr_lane1_i;
       wire    [31:0]     s_axi_wdata_lane1_i;
       wire    [3:0]     s_axi_wstrb_lane1_i;
       wire    [31:0]     s_axi_rdata_lane1_i;
       wire               s_axi_awvalid_lane1_i; 
       wire               s_axi_arvalid_lane1_i; 
       wire               s_axi_wvalid_lane1_i; 
       wire               s_axi_rvalid_lane1_i; 
       wire               s_axi_bvalid_lane1_i; 
       wire    [1:0]      s_axi_bresp_lane1_i; 
       wire    [1:0]      s_axi_rresp_lane1_i; 
       wire               s_axi_bready_lane1_i; 
       wire               s_axi_awready_lane1_i; 
       wire               s_axi_arready_lane1_i; 
       wire               s_axi_wready_lane1_i; 
       wire               s_axi_rready_lane1_i; 
       wire               link_reset_i;
       wire               sysreset_from_vio_i;
       wire               gtreset_from_vio_i;
       wire               rx_cdrovrden_i;
       wire               gt_reset_i;
       wire               gt_reset_i_tmp;

//---{
    wire                     gt_qpllclk_quad1_i;
    wire                     gt_qpllrefclk_quad1_i;
    wire                     gt_to_common_qpllreset_i;
    wire                     gt_qpllrefclklost_i; 
    wire                     gt_qplllock_i; 
//---}
    wire                     refclk1_in;
    wire                     refclk2_in;
 
    wire                     sysreset_from_support;
      wire sysreset_to_core_sync;


    assign                     gt_qplllock =  1'b0; 

 //*********************************Main Body of Code**********************************
 
 
 
 
     //____________________________Register User I/O___________________________________
 
     // System Interface
     assign  power_down_i      =   1'b0;
    // AXI4 Lite Interface
     assign  s_axi_awaddr_i    =  32'h0;
     assign  s_axi_wdata_i     =  16'h0;
     assign  s_axi_wstrb_i     =  16'h0;
     assign  s_axi_awvalid_i   =  1'b0; 
     assign  s_axi_wvalid_i    =  1'b0; 
     assign  s_axi_arvalid_i   =  1'b0; 
     assign  s_axi_rvalid_i    =  1'b0; 
     assign  s_axi_awaddr_lane1_i    =  32'h0;
     assign  s_axi_wdata_lane1_i     =  16'h0;
     assign  s_axi_wstrb_lane1_i     =  16'h0;
     assign  s_axi_awvalid_lane1_i   =  1'b0; 
     assign  s_axi_wvalid_lane1_i    =  1'b0; 
     assign  s_axi_arvalid_lane1_i   =  1'b0; 
     assign  s_axi_rvalid_lane1_i    =  1'b0; 
 
 
     //___________________________Module Instantiations_________________________________

aurora_64b66b_2lanes_gt_common_wrapper gt_common_support
(
    .gt_qpllclk_quad1_out    (gt_qpllclk_quad1_i      ),
    .gt_qpllrefclk_quad1_out (gt_qpllrefclk_quad1_i   ),

         .GT0_GTREFCLK0_COMMON_IN             (refclk1_in), 

    //----------------------- Common Block - QPLL Ports ------------------------

    .GT0_QPLLLOCK_OUT (gt_qplllock_i),

    .GT0_QPLLLOCKDETCLK_IN (INIT_CLK_i),

    .GT0_QPLLREFCLKLOST_OUT (gt_qpllrefclklost_i),


    //---------------------- Common DRP Ports ----------------------
         .qpll_drpaddr_in (qpll_drpaddr_in),
         .qpll_drpdi_in   (qpll_drpdi_in),
         .qpll_drpclk_in  (drp_clk_in),
         .qpll_drpdo_out (qpll_drpdo_out), 
         .qpll_drprdy_out(qpll_drprdy_out), 
         .qpll_drpen_in  (qpll_drpen_in), 
         .qpll_drpwe_in  (qpll_drpwe_in)
);
     

//--- Instance of GT differential buffer ---------//
     IBUFDS_GTE2 IBUFDS_GTXE2_CLK1  
     (
         .O               (refclk1_in),
         .ODIV2           (),
         .CEB             (1'b0),
         .I               (gt_refclk1_p),
         .IB              (gt_refclk1_n)
     );

     // Instantiate a clock module for clock division.
     aurora_64b66b_2lanes_CLOCK_MODULE clock_module_i
     (
 
         .INIT_CLK_P(init_clk_p),
         .INIT_CLK_N(init_clk_n),
 
         .INIT_CLK_O(INIT_CLK_i),
         .CLK(tx_out_clk),
         .CLK_LOCKED(gt_pll_lock),
         .USER_CLK(user_clk_i),
         .SYNC_CLK(sync_clk_i),
         .MMCM_NOT_LOCKED(mmcm_not_locked_i)
     );

  //  outputs
  assign init_clk_out          =  INIT_CLK_i;
  assign user_clk_out          =  user_clk_i;
  assign sync_clk_out          =  sync_clk_i;
  assign mmcm_not_locked_out   =  mmcm_not_locked_i;
  assign tx_lock               =  gt_pll_lock;


 

 
       assign sysreset_to_core_sync = reset_pb;
 
       assign pma_init_sync = pma_init;

     // Instantiate reset module to generate system reset
     aurora_64b66b_2lanes_SUPPORT_RESET_LOGIC support_reset_logic_i
     (
         .RESET(sysreset_to_core_sync),
         .USER_CLK(user_clk_i),
         .INIT_CLK(INIT_CLK_i),
         .GT_RESET_IN(pma_init_sync),
         .SYSTEM_RESET(sysreset_from_support),
         .GT_RESET_OUT(pma_init_i)
     );

//----- Instance of _xci -----[
aurora_64b66b_2lanes aurora_64b66b_2lanes_i
     (
        // RX AXI4-S Interface
         .m_axi_rx_tdata(m_axi_rx_tdata),
         .m_axi_rx_tlast(m_axi_rx_tlast),
         .m_axi_rx_tkeep(m_axi_rx_tkeep),
         .m_axi_rx_tvalid(m_axi_rx_tvalid),
  
       

         // RX User K Interface
         .m_axi_rx_user_k_tdata(m_axi_rx_user_k_tdata),
         .m_axi_rx_user_k_tvalid(m_axi_rx_user_k_tvalid),
 
 
         // GTX Serial I/O
         .rxp(rxp),
         .rxn(rxn),
 
         //GTX Reference Clock Interface
         .refclk1_in(refclk1_in),
         .rx_hard_err(rx_hard_err),
         .rx_soft_err(rx_soft_err),


         // Status
         .rx_channel_up(rx_channel_up),
         .rx_lane_up(rx_lane_up),

 
         // System Interface
         .mmcm_not_locked(mmcm_not_locked_i),
         .user_clk(user_clk_i),
         .reset2fc(reset2fc),
         .reset_pb(sysreset_from_support),
         .gt_rxcdrovrden_in(gt_rxcdrovrden_in),
         .power_down(power_down),
         .pma_init(pma_init_i),
         .gt_pll_lock(gt_pll_lock),
         .drp_clk_in(drp_clk_in),
//---{
       .gt_qpllclk_quad1_in       (gt_qpllclk_quad1_i          ), 
       .gt_qpllrefclk_quad1_in    (gt_qpllrefclk_quad1_i       ),    

//---}
     // ---------- AXI4-Lite input signals ---------------
         .s_axi_awaddr(s_axi_awaddr),
         .s_axi_awvalid(s_axi_awvalid), 
         .s_axi_awready(s_axi_awready), 
         .s_axi_awaddr_lane1(s_axi_awaddr_lane1),
         .s_axi_awvalid_lane1(s_axi_awvalid_lane1), 
         .s_axi_awready_lane1(s_axi_awready_lane1), 
         .s_axi_wdata(s_axi_wdata),
         .s_axi_wstrb(s_axi_wstrb),
         .s_axi_wvalid(s_axi_wvalid), 
         .s_axi_wready(s_axi_wready), 
         .s_axi_bvalid(s_axi_bvalid), 
         .s_axi_bresp(s_axi_bresp), 
         .s_axi_bready(s_axi_bready), 
         .s_axi_wdata_lane1(s_axi_wdata_lane1),
         .s_axi_wstrb_lane1(s_axi_wstrb_lane1),
         .s_axi_wvalid_lane1(s_axi_wvalid_lane1), 
         .s_axi_wready_lane1(s_axi_wready_lane1), 
         .s_axi_bvalid_lane1(s_axi_bvalid_lane1), 
         .s_axi_bresp_lane1(s_axi_bresp_lane1), 
         .s_axi_bready_lane1(s_axi_bready_lane1), 
         .s_axi_araddr(s_axi_araddr),
         .s_axi_arvalid(s_axi_arvalid), 
         .s_axi_arready(s_axi_arready), 
         .s_axi_araddr_lane1(s_axi_araddr_lane1),
         .s_axi_arvalid_lane1(s_axi_arvalid_lane1), 
         .s_axi_arready_lane1(s_axi_arready_lane1), 
         .s_axi_rdata(s_axi_rdata),
         .s_axi_rvalid(s_axi_rvalid), 
         .s_axi_rresp(s_axi_rresp), 
         .s_axi_rready(s_axi_rready), 
         .s_axi_rdata_lane1(s_axi_rdata_lane1),
         .s_axi_rvalid_lane1(s_axi_rvalid_lane1), 
         .s_axi_rresp_lane1(s_axi_rresp_lane1), 
         .s_axi_rready_lane1(s_axi_rready_lane1), 
         .init_clk(INIT_CLK_i),
         .link_reset_out(link_reset_out),
    //------------------------ RX Margin Analysis Ports ------------------------
         .gt0_eyescandataerror_out         (gt0_eyescandataerror_out),
         .gt0_eyescanreset_in              (gt0_eyescanreset_in),
         .gt0_eyescantrigger_in            (gt0_eyescantrigger_in),
    //------------------- Receive Ports - RX Equalizer Ports -------------------
         .gt0_rxcdrhold_in                 (gt0_rxcdrhold_in),
         .gt0_rxlpmhfovrden_in             (gt0_rxlpmhfovrden_in),
         .gt0_rxdfeagchold_in              (gt0_rxdfeagchold_in),
         .gt0_rxdfeagcovrden_in            (gt0_rxdfeagcovrden_in),
         .gt0_rxdfelfhold_in               (gt0_rxdfelfhold_in),
         .gt0_rxdfelpmreset_in             (gt0_rxdfelpmreset_in),
         .gt0_rxlpmlfklovrden_in           (gt0_rxlpmlfklovrden_in),
         .gt0_rxmonitorout_out             (gt0_rxmonitorout_out),
         .gt0_rxmonitorsel_in              (gt0_rxmonitorsel_in),
         .gt0_rxlpmen_in                   (gt0_rxlpmen_in),
         .gt0_rxpmareset_in                (gt0_rxpmareset_in),
         .gt0_rxpcsreset_in                (gt0_rxpcsreset_in ),
         .gt0_rxbufreset_in                (gt0_rxbufreset_in ),
        .gt0_rxprbssel_in                  (gt0_rxprbssel_in     ),
        .gt0_rxprbserr_out                 (gt0_rxprbserr_out    ),
        .gt0_rxprbscntreset_in             (gt0_rxprbscntreset_in),
        .gt0_rxresetdone_out               (gt0_rxresetdone_out),
        .gt0_rxbufstatus_out               (gt0_rxbufstatus_out), 
        .gt0_dmonitorout_out               (gt0_dmonitorout_out    ),
        .gt0_cplllock_out                  (gt0_cplllock_out),
    //------------------------ RX Margin Analysis Ports ------------------------
         .gt1_eyescandataerror_out         (gt1_eyescandataerror_out),
         .gt1_eyescanreset_in              (gt1_eyescanreset_in),
         .gt1_eyescantrigger_in            (gt1_eyescantrigger_in),
    //------------------- Receive Ports - RX Equalizer Ports -------------------
         .gt1_rxcdrhold_in                 (gt1_rxcdrhold_in),
         .gt1_rxlpmhfovrden_in             (gt1_rxlpmhfovrden_in),
         .gt1_rxdfeagchold_in              (gt1_rxdfeagchold_in),
         .gt1_rxdfeagcovrden_in            (gt1_rxdfeagcovrden_in),
         .gt1_rxdfelfhold_in               (gt1_rxdfelfhold_in),
         .gt1_rxdfelpmreset_in             (gt1_rxdfelpmreset_in),
         .gt1_rxlpmlfklovrden_in           (gt1_rxlpmlfklovrden_in),
         .gt1_rxmonitorout_out             (gt1_rxmonitorout_out),
         .gt1_rxmonitorsel_in              (gt1_rxmonitorsel_in),
         .gt1_rxlpmen_in                   (gt1_rxlpmen_in),
         .gt1_rxpmareset_in                (gt1_rxpmareset_in),
         .gt1_rxpcsreset_in                (gt1_rxpcsreset_in ),
         .gt1_rxbufreset_in                (gt1_rxbufreset_in ),
        .gt1_rxprbssel_in                  (gt1_rxprbssel_in     ),
        .gt1_rxprbserr_out                 (gt1_rxprbserr_out    ),
        .gt1_rxprbscntreset_in             (gt1_rxprbscntreset_in),
        .gt1_rxresetdone_out               (gt1_rxresetdone_out),
        .gt1_rxbufstatus_out               (gt1_rxbufstatus_out), 
        .gt1_dmonitorout_out               (gt1_dmonitorout_out    ),
        .gt1_cplllock_out                  (gt1_cplllock_out),
         .sys_reset_out                            (sys_reset_out),
         .tx_out_clk                               (tx_out_clk)
     );
//----- Instance of _xci -----]
 


 endmodule
