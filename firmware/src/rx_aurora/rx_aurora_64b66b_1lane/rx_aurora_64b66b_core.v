/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ns/1ps
`default_nettype none

`ifndef SYNTHESIS
    `include "rx_aurora/rx_aurora_64b66b_1lane/ip/exdes/aurora_64b66b_1lane_exdes.v"
`endif

module rx_aurora_64b66b_core
#(
    parameter ABUSWIDTH = 16,
    parameter IDENTYFIER = 0,
    parameter AURORA_LANES = 1
)(
    input wire [3:0] RXP, RXN,
    input wire RX_CLK_IN_P, RX_CLK_IN_N,
    `ifdef GENESYS2
    input wire INIT_CLK_IN,
    input wire DRP_CLK_IN,
    `else
    input wire INIT_CLK_IN_P, INIT_CLK_IN_N,
    `endif
    output wire REFCLK1_OUT,
    output wire TX_OUT_CLK,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,

    input wire USERK_FIFO_READ,
    output wire USERK_FIFO_EMPTY,
    output wire [31:0] USERK_FIFO_DATA,

    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    output wire LOST_ERROR,
    output wire RX_LANE_UP,
    output wire RX_CHANNEL_UP,
    output wire PLL_LOCKED,
    output wire MGT_REF_SEL,

    input wire AURORA_RESET
);

localparam VERSION = 1;

//output format #ID (as parameter IDENTIFIER + 1 frame start + 16 bit data)

wire SOFT_RST;
assign SOFT_RST = (BUS_ADD==0 && BUS_WR);

wire RST;
assign RST = BUS_RST | SOFT_RST;

reg CONF_EN ;
reg CONF_SI570_SET = 0;
reg CONF_USER_K_FILTER_EN;
reg [7:0] USER_K_FILTER_MASK_1, USER_K_FILTER_MASK_2, USER_K_FILTER_MASK_3;
reg RESET_COUNTERS = 0;
reg [7:0] LOST_DATA_CNT = 8'd0;
reg [31:0] FRAME_COUNTER = 32'd0;
wire RX_HARD_ERROR, RX_SOFT_ERROR, RX_READY;
reg CONF_MGT_REF_SEL;
assign MGT_REF_SEL = CONF_MGT_REF_SEL;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 1;
        CONF_USER_K_FILTER_EN <= 1;
        CONF_MGT_REF_SEL <= 1;
        USER_K_FILTER_MASK_1 <= 8'h01;
        USER_K_FILTER_MASK_2 <= 8'h02;
        USER_K_FILTER_MASK_3 <= 8'h04;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2) begin
            CONF_EN <= BUS_DATA_IN[0];
            CONF_MGT_REF_SEL <= BUS_DATA_IN[6];
            CONF_USER_K_FILTER_EN <= BUS_DATA_IN[7];
        end
        else if(BUS_ADD == 4)
            USER_K_FILTER_MASK_1  <= BUS_DATA_IN;
        else if(BUS_ADD == 5)
            USER_K_FILTER_MASK_2  <= BUS_DATA_IN;
        else if(BUS_ADD == 6)
            USER_K_FILTER_MASK_3  <= BUS_DATA_IN;
        else if(BUS_ADD == 7) begin
            RESET_COUNTERS <= BUS_DATA_IN[0];
            CONF_SI570_SET <= BUS_DATA_IN[1];
        end
    end
end


// count the sent data packaged (in units of 32 bits)
always@(posedge BUS_CLK) begin
    if(FIFO_READ & !FIFO_EMPTY & (FRAME_COUNTER < 24'hffffff))
        FRAME_COUNTER <= FRAME_COUNTER + 1;
    if (RESET_COUNTERS)
        FRAME_COUNTER <= 32'b0;
end
/*
 * // count the number of received aurora packages (in units of 64 bit)
always@(posedge USER_CLK) begin
    if(RX_TVALID & (FRAME_COUNTER < 24'hffffff))
        FRAME_COUNTER <= FRAME_COUNTER + 1;
    if (RESET_COUNTERS)
        FRAME_COUNTER <= 32'b0;
end
*/


always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 2)
                BUS_DATA_OUT <= {CONF_USER_K_FILTER_EN, MGT_REF_SEL, RX_HARD_ERROR&PLL_LOCKED, RX_SOFT_ERROR&PLL_LOCKED, PLL_LOCKED, RX_LANE_UP, RX_READY, CONF_EN};
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= LOST_DATA_CNT;
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_1;
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_2;
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_3;
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= {6'b0, CONF_SI570_SET, RESET_COUNTERS};
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= FRAME_COUNTER[31:24];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= FRAME_COUNTER[23:16];
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= FRAME_COUNTER[15:8];
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= FRAME_COUNTER[7:0];
      else
            BUS_DATA_OUT <= 8'b0;
    end
end


wire USER_CLK;
wire RX_CLK_P, RX_CLK_N;
assign RX_CLK_P = RX_CLK_IN_P;
assign RX_CLK_N = RX_CLK_IN_N;
`ifdef GENESYS2
wire INIT_CLK;
assign INIT_CLK = INIT_CLK_IN;
`else
wire INIT_CLK_P, INIT_CLK_N;
assign INIT_CLK_P = INIT_CLK_IN_P;
assign INIT_CLK_N = INIT_CLK_IN_N;
`endif

wire RST_USER_SYNC;

wire RST_SOFT_SYNC;
cdc_reset_sync rst_pulse_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(REFCLK1_OUT), .pulse_out(RST_SOFT_SYNC));

wire CONF_EN_SYNC;
cdc_reset_sync rst_conf_en_sync (.clk_in(BUS_CLK), .pulse_in(CONF_EN), .clk_out(USER_CLK), .pulse_out(CONF_EN_SYNC));

// Aurora init
reg pma_init_r;
reg [4:0] pma_init_cnt;
always@ (posedge BUS_CLK) begin
    //pma_init_r <= AURORA_RESET | RST;
    if (AURORA_RESET | RST)
        pma_init_cnt <= 5'hf;
    if (pma_init_cnt > 5'h0) begin
        pma_init_r <= 1'b1;
        pma_init_cnt <= pma_init_cnt - 1;
    end
    else
       pma_init_r <= 1'b0;
end

wire RX_TLAST;
wire RX_TVALID;
wire [63:0] RX_TDATA;
wire [7:0] RX_TKEEP;

assign RX_READY = RX_CHANNEL_UP & RX_LANE_UP;

cdc_reset_sync rst_pulse_user_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(USER_CLK), .pulse_out(RST_USER_SYNC));

reg RX_TFIRST;
always@(posedge USER_CLK)
    if(RST_USER_SYNC)
        RX_TFIRST <= 1;
    else if(RX_TVALID & RX_TLAST)
        RX_TFIRST <= 1;
    else if(RX_TVALID)
        RX_TFIRST <= 0;


localparam DATA_SIZE_FIFO = 1+1+64;
wire byte4;
assign byte4 = (RX_TKEEP == 8'hff);
wire [DATA_SIZE_FIFO-1:0] data_to_cdc;
assign data_to_cdc = {byte4, RX_TFIRST, RX_TDATA};

wire [DATA_SIZE_FIFO-1:0] cdc_data_out;
wire read_fifo_cdc;
wire fifo_full;
wire write_out_fifo;
wire wfull, cdc_fifo_empty;
reg [1:0] byte2_cnt, byte2_cnt_prev;
reg [1:0] userk_byte2_cnt, userk_byte2_cnt_prev;

assign write_out_fifo = (byte2_cnt != 0 || byte2_cnt_prev != 0);
assign read_fifo_cdc = (byte2_cnt_prev==0 & byte2_cnt!=0);

// CDC fifo for the pixel data
cdc_syncfifo #(.DSIZE(DATA_SIZE_FIFO), .ASIZE(8)) cdc_syncfifo_i
(
    .rdata(cdc_data_out),
    .wfull(wfull),
    .rempty(cdc_fifo_empty),
    .wdata(data_to_cdc),
    .winc(RX_TVALID & CONF_EN_SYNC), .wclk(USER_CLK), .wrst(RST_USER_SYNC),
    .rinc(read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST)
    );


localparam DATA_USERK_SIZE_FIFO = 64+1;
wire [63:0] USER_K_DATA;
wire USER_K_VALID;
wire USER_K_ERR;

wire [DATA_USERK_SIZE_FIFO-1:0] userk_data_to_cdc, userk_cdc_data_out;
wire userk_fifo_full, userk_write_out_fifo, userk_wfull, userk_cdc_fifo_empty, userk_read_fifo_cdc;

reg  USER_K_VALID_delayed;
wire USERK_RX_TFIRST_COMB;
always@(posedge USER_CLK) begin
    USER_K_VALID_delayed <= USER_K_VALID;
end


// Filter to seperate monitor data from register data etc
wire USER_K_FILTER_PASSED;
assign USER_K_FILTER_PASSED = ( ( USER_K_DATA[63:56] == USER_K_FILTER_MASK_1 )
                              | ( USER_K_DATA[63:56] == USER_K_FILTER_MASK_2 )
                              | ( USER_K_DATA[63:56] == USER_K_FILTER_MASK_3 )
                              ) ? 1 : 0;

// Write to CDC fifo after applying the filter
assign USERK_RX_TFIRST_COMB = USER_K_VALID & !USER_K_VALID_delayed & (USER_K_FILTER_PASSED | ~CONF_USER_K_FILTER_EN);

assign userk_data_to_cdc = {USERK_RX_TFIRST_COMB, USER_K_DATA};
assign userk_write_out_fifo = (userk_byte2_cnt != 0 || userk_byte2_cnt_prev != 0);
assign userk_read_fifo_cdc = (userk_byte2_cnt_prev==0 & userk_byte2_cnt!=0);

// CDC fifo for the USER_K data
cdc_syncfifo #(.DSIZE(DATA_USERK_SIZE_FIFO), .ASIZE(8)) userk_cdc_syncfifo_i
(
    .rdata(userk_cdc_data_out),
    .wfull(userk_wfull),
    .rempty(userk_cdc_fifo_empty),
    .wdata(userk_data_to_cdc),
    .winc(USERK_RX_TFIRST_COMB & CONF_EN_SYNC), .wclk(USER_CLK), .wrst(RST_USER_SYNC),
    .rinc(userk_read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST)
);

wire mgtrefclk0;

 IBUFDS_GTE2 #(
      .CLKCM_CFG("TRUE"),   // Refer to Transceiver User Guide
      .CLKRCV_TRST("TRUE"), // Refer to Transceiver User Guide
      .CLKSWING_CFG(2'b11)  // Refer to Transceiver User Guide
   )
   IBUFDS_GTE2_inst (
      .O(mgtrefclk0),         // 1-bit output: Refer to Transceiver User Guide
      //.ODIV2(ODIV2), // 1-bit output: Refer to Transceiver User Guide
      .CEB(1'b0),     // 1-bit input: Refer to Transceiver User Guide
      .I(RX_CLK_P),         // 1-bit input: Refer to Transceiver User Guide
      .IB(RX_CLK_N)        // 1-bit input: Refer to Transceiver User Guide
   );

//ibert_7series_gtx_0 ibert_aurora (
//  //.TXN_O(TXN_O),              // output wire [3 : 0] TXN_O
//  //.TXP_O(TXP_O),              // output wire [3 : 0] TXP_O
//  //.RXOUTCLK_O(refclk1),    // output wire RXOUTCLK_O
//  .RXN_I(RXN),              // input wire [3 : 0] RXN_I
//  .RXP_I(RXP),              // input wire [3 : 0] RXP_I
//  .GTREFCLK0_I(mgtrefclk0),  // input wire [0 : 0] GTREFCLK0_I
//  .GTREFCLK1_I(1'b0),  // input wire [0 : 0] GTREFCLK1_I
//  .SYSCLK_I(INIT_CLK)    // input wire SYSCLK_I
//);

//BUFG mybuf ( .I(mgtrefclk0), .O(REFCLK1_OUT) );

aurora_64b66b_1lane_exdes   #( .SIMPLEX_TIMER_VALUE(10) ) aurora_frame (
    // Error signals from Aurora
    .RX_HARD_ERR(RX_HARD_ERROR),
    .RX_SOFT_ERR(RX_SOFT_ERROR),
    .RX_LANE_UP(RX_LANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),
    .REFCLK1_OUT(REFCLK1_OUT),
    .TX_OUT_CLK(TX_OUT_CLK),

    `ifdef GENESYS2
    .INIT_CLK(INIT_CLK),
    `else
    .INIT_CLK_P(INIT_CLK_P),
    .INIT_CLK_N(INIT_CLK_N),
    `endif

    .PMA_INIT(pma_init_r),

    `ifdef GENESYS2
    .GTXQ0_P(mgtrefclk0),
    .GTXQ0_N(mgtrefclk0),
    `else
    .GTXQ0_P(RX_CLK_P),
    .GTXQ0_N(RX_CLK_N),
    `endif
    .RXP(RXP),
    .RXN(RXN),

    // Error signals from the Local Link packet checker
    .DATA_ERR_COUNT(),
    //USER_K
    .USER_K_ERR(USER_K_ERR),
    .USER_K_DATA(USER_K_DATA),
    .USER_K_VALID(USER_K_VALID),

    // User IO
    .RESET(RST_USER_SYNC),

    .DRP_CLK_IN(DRP_CLK_IN),
    .USER_CLK(USER_CLK),
    .RX_TDATA(RX_TDATA),
    .RX_TVALID(RX_TVALID),
    .RX_TKEEP(RX_TKEEP),
    .RX_TLAST(RX_TLAST)
);

//ila_0 ila (
// .clk(USER_CLK),
// .probe0({RX_TFIRST, RX_TVALID, RX_TLAST, RX_TKEEP, RX_CHANNEL_UP, RX_LANE_UP, RX_SOFT_ERROR, RX_HARD_ERROR})
//);

// RX DATA
always@(posedge BUS_CLK)
    byte2_cnt_prev <= byte2_cnt;

always@(posedge BUS_CLK)
    if(RST)
        byte2_cnt <= 0;
    else if(!cdc_fifo_empty && !fifo_full && byte2_cnt == 0 )
    begin
        if(cdc_data_out[DATA_SIZE_FIFO-1])
            byte2_cnt <= 3;
        else
            byte2_cnt <= 1;
    end
    else if (!fifo_full & byte2_cnt != 0)
        byte2_cnt <= byte2_cnt - 1;

// USER DATA
always@(posedge BUS_CLK)
    userk_byte2_cnt_prev <= userk_byte2_cnt;

always@(posedge BUS_CLK)
    if(RST)
        userk_byte2_cnt <= 0;
    else if(!userk_cdc_fifo_empty && !userk_fifo_full && userk_byte2_cnt == 0 )
    begin
        userk_byte2_cnt <= 3;
    end
    else if (!userk_fifo_full & userk_byte2_cnt != 0)
        userk_byte2_cnt <= userk_byte2_cnt - 1;


reg [DATA_SIZE_FIFO-1:0] data_buf;
wire [16:0] data_out;
wire [16:0] fifo_data_out_byte [3:0];

wire byte4_sel;
assign byte4_sel = read_fifo_cdc ? cdc_data_out[DATA_SIZE_FIFO-1] : data_buf[DATA_SIZE_FIFO-1];

assign fifo_data_out_byte[0] = byte4_sel ? {1'b0, data_buf[23:16], data_buf[31:24]} : {1'b0, data_buf[55:48], data_buf[63:56]};
assign fifo_data_out_byte[1] = byte4_sel ? {1'b0, data_buf[7:0],data_buf[15:8]} : {cdc_data_out[DATA_SIZE_FIFO-2], cdc_data_out[39:32],cdc_data_out[47:40]};
assign fifo_data_out_byte[2] = {1'b0, data_buf[55:48], data_buf[63:56]} ;
assign fifo_data_out_byte[3] = {cdc_data_out[DATA_SIZE_FIFO-2], cdc_data_out[39:32],cdc_data_out[47:40]};


always@(posedge BUS_CLK)
    if(read_fifo_cdc)
        data_buf <= cdc_data_out;

wire [23:0] cdc_data;
assign cdc_data = {7'b0, data_out};
assign data_out = fifo_data_out_byte[byte2_cnt];
gerneric_fifo #(.DATA_SIZE(24), .DEPTH(1024*8))  fifo_i
(   .clk(BUS_CLK), .reset(RST),
    .write(write_out_fifo),
    .read(FIFO_READ),
    .data_in(cdc_data),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .data_out(FIFO_DATA[23:0]),
    .size()
);

// USER K
reg [DATA_USERK_SIZE_FIFO-1:0] userk_data_buf;
always@(posedge BUS_CLK)
    if(userk_read_fifo_cdc)
        userk_data_buf <= userk_cdc_data_out;

wire [16:0] userk_fifo_data_out_byte [3:0];

assign userk_fifo_data_out_byte[0] = {1'b0,                                   userk_data_buf[23:16],     userk_data_buf[31:24]};
assign userk_fifo_data_out_byte[1] = {1'b0,                                   userk_data_buf[7:0],       userk_data_buf[15:8]};
assign userk_fifo_data_out_byte[2] = {1'b0,                                   userk_data_buf[55:48],     userk_data_buf[63:56]};
assign userk_fifo_data_out_byte[3] = {userk_cdc_data_out[DATA_SIZE_FIFO-2],   userk_cdc_data_out[39:32], userk_cdc_data_out[47:40]};


wire [16:0] userk_data_out;
wire [23:0] userk_cdc_data;
assign userk_data_out = userk_fifo_data_out_byte[userk_byte2_cnt];
assign userk_cdc_data = {7'b0, userk_data_out};

gerneric_fifo #(.DATA_SIZE(24), .DEPTH(1024))  userk_fifo_i
(   .clk(BUS_CLK), .reset(RST),
    .write(userk_write_out_fifo),
    .read(USERK_FIFO_READ),
    .data_in(userk_cdc_data),
    .full(userk_fifo_full),
    .empty(USERK_FIFO_EMPTY),
    .data_out(USERK_FIFO_DATA[23:0]),
    .size()
);

always@(posedge USER_CLK) begin
    if(RST_USER_SYNC)
        LOST_DATA_CNT <= 0;
    else if (wfull && RX_TVALID && LOST_DATA_CNT != -1)
        if (LOST_DATA_CNT < 8'hff)
            LOST_DATA_CNT <= LOST_DATA_CNT +1;
end

assign FIFO_DATA[31:24]       =  {IDENTYFIER[7:0]};
assign USERK_FIFO_DATA[31:24] =  {8'b0};

assign LOST_ERROR = LOST_DATA_CNT != 0;

endmodule
