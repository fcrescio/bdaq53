/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */
`timescale 1ns/1ps
`default_nettype none

`include "rrp_arbiter/rrp_arbiter.v"

module rx_aurora_64b66b
#(
    parameter BASEADDR = 16'h0000,
    parameter HIGHADDR = 16'h0000,
    parameter ABUSWIDTH = 16,
    parameter IDENTYFIER = 0
)(
    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    inout wire [7:0] BUS_DATA,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    input wire [3:0] RXP, RXN,
    input wire RX_CLK_IN_P, RX_CLK_IN_N,
    `ifdef GENESYS2
    input wire INIT_CLK_IN,
    input wire DRP_CLK_IN,
    `else
    input wire INIT_CLK_IN_P, INIT_CLK_IN_N,
    `endif
    output wire REFCLK1_OUT,
    output wire TX_OUT_CLK,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,

    output wire LOST_ERROR,
    output wire RX_LANE_UP,
    output wire RX_CHANNEL_UP,
    output wire PLL_LOCKED,
    output wire MGT_REF_SEL,

    input wire AURORA_RESET
);

//localparam AURORA_NR_OF_LANES = 4;     //as defined in python test
`ifdef AURORA_4LANE
    localparam AURORA_NR_OF_LANES = 4;
`elsif AURORA_2LANE
    localparam AURORA_NR_OF_LANES = 2;
`elsif AURORA_1LANE
    localparam AURORA_NR_OF_LANES = 1;
`else
    localparam AURORA_NR_OF_LANES = 1;
`endif

wire IP_RD, IP_WR;
wire [ABUSWIDTH-1:0] IP_ADD;
wire [7:0] IP_DATA_IN;
wire [7:0] IP_DATA_OUT;

bus_to_ip #( .BASEADDR(BASEADDR), .HIGHADDR(HIGHADDR), .ABUSWIDTH(ABUSWIDTH) ) i_bus_to_ip
(
    .BUS_RD(BUS_RD),
    .BUS_WR(BUS_WR),
    .BUS_ADD(BUS_ADD),
    .BUS_DATA(BUS_DATA),

    .IP_RD(IP_RD),
    .IP_WR(IP_WR),
    .IP_ADD(IP_ADD),
    .IP_DATA_IN(IP_DATA_IN),
    .IP_DATA_OUT(IP_DATA_OUT)
);


localparam ARB_WIDTH = 2;

wire FIFO_USER_K_READ;
wire FIFO_USER_K_EMPTY;
wire [31:0] FIFO_USER_K_DATA;

wire ARB_READY_OUT;
wire ARB_WRITE_OUT;
wire ARB_GRANT_USER_K, ARB_GRANT_NORMAL;
//wire [ARB_WIDTH-1:0] ARB_GRANT;
wire [31:0] ARB_DATA_OUT, FIFO_NORMAL_DATA;
wire FIFO_NORMAL_EMPTY;

rrp_arbiter
#(
    .WIDTH(ARB_WIDTH)
) i_rrp_arbiter
(
    .RST(BUS_RST),
    .CLK(BUS_CLK),

    .WRITE_REQ({!FIFO_USER_K_EMPTY, !FIFO_NORMAL_EMPTY}),      // indicate will of writing data
    .HOLD_REQ({2'b00}),                                  // wait for writing for given stream (priority)
    .DATA_IN({FIFO_USER_K_DATA, FIFO_NORMAL_DATA}),     // incoming data for arbitration
    .READ_GRANT({ARB_GRANT_USER_K, ARB_GRANT_NORMAL}),  // indicate to stream that data has been accepted

    .READY_OUT(ARB_READY_OUT),              // indicates ready for outgoing stream
    .WRITE_OUT(ARB_WRITE_OUT),              // indicates will of write to outgoing stream
    .DATA_OUT(ARB_DATA_OUT)                 // outgoing data stream
);


// map FIFO signals to arbiter
assign ARB_READY_OUT = FIFO_READ;
assign FIFO_DATA = {ARB_GRANT_USER_K, ARB_DATA_OUT[23:0]};    // 16 bits of data, 8 bits frame information, 8 bits of additional IDs, flags etc
assign FIFO_EMPTY = ~ARB_WRITE_OUT;


rx_aurora_64b66b_core
#(
    .ABUSWIDTH(ABUSWIDTH),
    .IDENTYFIER(IDENTYFIER),
    .AURORA_LANES(AURORA_NR_OF_LANES)
) i_aurora_rx_core
(
    .BUS_CLK(BUS_CLK),
    .BUS_RST(BUS_RST),
    .BUS_ADD(IP_ADD),
    .BUS_DATA_IN(IP_DATA_IN),
    .BUS_RD(IP_RD),
    .BUS_WR(IP_WR),
    .BUS_DATA_OUT(IP_DATA_OUT),

    .RXP(RXP), .RXN(RXN),
    .RX_CLK_IN_P(RX_CLK_IN_P), .RX_CLK_IN_N(RX_CLK_IN_N),
    `ifdef GENESYS2
        .INIT_CLK_IN(INIT_CLK_IN),
        .DRP_CLK_IN(DRP_CLK_IN),
    `else
        .INIT_CLK_IN_P(INIT_CLK_IN_P), .INIT_CLK_IN_N(INIT_CLK_IN_N),
    `endif
    .REFCLK1_OUT(REFCLK1_OUT),
    .TX_OUT_CLK(TX_OUT_CLK),

    .FIFO_READ(ARB_GRANT_NORMAL),   // FIFO_READ
    .FIFO_EMPTY(FIFO_NORMAL_EMPTY),
    .FIFO_DATA(FIFO_NORMAL_DATA),

    .USERK_FIFO_READ(ARB_GRANT_USER_K),
    .USERK_FIFO_EMPTY(FIFO_USER_K_EMPTY),
    .USERK_FIFO_DATA(FIFO_USER_K_DATA),

    .LOST_ERROR(LOST_ERROR),

    .RX_LANE_UP(RX_LANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),
    .MGT_REF_SEL(MGT_REF_SEL),

    .AURORA_RESET(AURORA_RESET)
    );

endmodule
