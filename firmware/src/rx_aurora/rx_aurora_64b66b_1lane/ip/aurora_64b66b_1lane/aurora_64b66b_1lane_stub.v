// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (lin64) Build 2086221 Fri Dec 15 20:54:30 MST 2017
// Date        : Thu Mar 22 11:56:30 2018
// Host        : lpnhelucie.in2p3.fr running 64-bit Scientific Linux release 6.9 (Carbon)
// Command     : write_verilog -force -mode synth_stub
//               /scratch/fcrescio/rd53/bdaq53/firmware/src/rx_aurora/rx_aurora_64b66b_1lane/ip/aurora_64b66b_1lane/aurora_64b66b_1lane_stub.v
// Design      : aurora_64b66b_1lane
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k325tffg900-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "aurora_64b66b_v11_2_3, Coregen v14.3_ip3, Number of lanes = 1, Line rate is double1.28Gbps, Reference Clock is double160.0MHz, Interface is Framing, Flow Control is None, User K and is operating in DUPLEX configuration" *)
module aurora_64b66b_1lane(m_axi_rx_tdata, m_axi_rx_tlast, 
  m_axi_rx_tkeep, m_axi_rx_tvalid, m_axi_rx_user_k_tdata, m_axi_rx_user_k_tvalid, rxp, rxn, 
  refclk1_in, rx_hard_err, rx_soft_err, rx_channel_up, rx_lane_up, mmcm_not_locked, user_clk, 
  reset2fc, reset_pb, gt_rxcdrovrden_in, power_down, pma_init, gt_pll_lock, drp_clk_in, 
  gt_qpllclk_quad4_in, gt_qpllrefclk_quad4_in, drpaddr_in, drpdi_in, drpdo_out, drprdy_out, 
  drpen_in, drpwe_in, init_clk, link_reset_out, sys_reset_out, tx_out_clk)
/* synthesis syn_black_box black_box_pad_pin="m_axi_rx_tdata[0:63],m_axi_rx_tlast,m_axi_rx_tkeep[0:7],m_axi_rx_tvalid,m_axi_rx_user_k_tdata[0:63],m_axi_rx_user_k_tvalid,rxp[0:0],rxn[0:0],refclk1_in,rx_hard_err,rx_soft_err,rx_channel_up,rx_lane_up[0:0],mmcm_not_locked,user_clk,reset2fc,reset_pb,gt_rxcdrovrden_in,power_down,pma_init,gt_pll_lock,drp_clk_in,gt_qpllclk_quad4_in,gt_qpllrefclk_quad4_in,drpaddr_in[8:0],drpdi_in[15:0],drpdo_out[15:0],drprdy_out,drpen_in,drpwe_in,init_clk,link_reset_out,sys_reset_out,tx_out_clk" */;
  output [0:63]m_axi_rx_tdata;
  output m_axi_rx_tlast;
  output [0:7]m_axi_rx_tkeep;
  output m_axi_rx_tvalid;
  output [0:63]m_axi_rx_user_k_tdata;
  output m_axi_rx_user_k_tvalid;
  input [0:0]rxp;
  input [0:0]rxn;
  input refclk1_in;
  output rx_hard_err;
  output rx_soft_err;
  output rx_channel_up;
  output [0:0]rx_lane_up;
  input mmcm_not_locked;
  input user_clk;
  output reset2fc;
  input reset_pb;
  input gt_rxcdrovrden_in;
  input power_down;
  input pma_init;
  output gt_pll_lock;
  input drp_clk_in;
  input gt_qpllclk_quad4_in;
  input gt_qpllrefclk_quad4_in;
  input [8:0]drpaddr_in;
  input [15:0]drpdi_in;
  output [15:0]drpdo_out;
  output drprdy_out;
  input drpen_in;
  input drpwe_in;
  input init_clk;
  output link_reset_out;
  output sys_reset_out;
  output tx_out_clk;
endmodule
