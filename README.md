[![build status](https://gitlab.cern.ch/silab/bdaq53/badges/master/build.svg)](https://gitlab.cern.ch/silab/bdaq53/commits/master)

# bdaq53
DAQ and verification environment for the [RD53A](https://cds.cern.ch/record/2287593) prototype based on the [Basil](https://github.com/SiLab-Bonn/basil) framework.
The firmware is compatible with the BDAQ53 + [Mercury+ KX2](https://www.enclustra.com/en/products/fpga-modules/mercury-kx2/) 
system and the [Xilinx KC705](https://www.xilinx.com/products/boards-and-kits/ek-k7-kc705-g.html) evaluation board.



### Installation

- Install [conda](https://conda.io/miniconda.html) for python

- Install dependencies and bdaq53:
```
conda install numpy bitarray pyyaml scipy numba pytables matplotlib tqdm pyzmq blosc psutil
pip install git+https://gitlab.cern.ch/silab/bdaq53.git@master
```

### Usage
- [Flash appropriate bit file](https://gitlab.cern.ch/silab/bdaq53/wikis/fpga-configuration), which is attached to the latest [tag](https://gitlab.cern.ch/silab/bdaq53/tags)

- Check if everything is correctly connected:
```
ping 192.168.10.16
```

- Run a scan:
```
bdaq53 scan_digital
```
- For help, run:
```
bdaq53 --help
```
- Use the online monitor:

    ```
    bdaq53_monitor
    ```

## Support
For questions and comments, visit https://mattermost.web.cern.ch/rd53-testing

## Development
If you want to help in the development of bdaq53, a few extra steps are needed:

- Clone bdaq53 repository from slected branch
```bash
git clone -b development https://gitlab.cern.ch/silab/bdaq53.git
```

- Download folder with test data and example files (if needed)
```bash
git submodule update --init --recursive
```

- Setup bdaq53
```bash
cd bdaq53
python setup.py develop
```

- Create a branch for your changes and once you're done, create a merge request to the development branch

### Firmware

- Download and Setup [Basil](https://github.com/SiLab-Bonn/basil):
```bash
git clone -b development https://github.com/SiLab-Bonn/basil; 
cd basil
python setup.py develop
cd ..
```

- Download and install Vivado [2017.1](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vivado-design-tools/2017-1.html)** + valid licence (The web edition is sufficient for BDAQ53 but not for KC705).

- Set the environmental variable `XILINX_VIVADO` for Vivado according to your xilinx installation directory (default should be `/opt/XILINX/Vivado/2017.1/`):
```bash
export XILINX_VIVADO={your_xilinx_install_directory}/Vivado/2017.1
```

- Download the latest Kintex 7 version of SiTCP and extract the archive to the `/firmware/SiTCP` folder [Website](http://research.kek.jp/people/uchida/technologies/SiTCP/), [Download](https://sitcp.bbtech.co.jp/modules/d3downloads/).

- Generate a bit file

    You can find bitfiles and memory configuration files for BDAQ53 and KC705 [here](https://gitlab.cern.ch/silab/bdaq53/tags). They can be used to configure the FPGA or to flash the non volatile QSPI memory chips.

    If you prefer to create your own configuration, select the desired hardware platform by running the corresponding TCL script from the Vivado TCL console.
    ```bash
    cd {YOUR_BDAQ53_PATH}/firmware/vivado
    source project_bdaq53.tcl
    ```
    __If you use the KC705 board__, first uncomment the line `` `define KC705`` in rd53a_daq.v, comment `` `define BDAQ53`` and then use the 
    ```bash
    source project_kc705.tcl
    ```
    script to create the project.

### Simulation

In order to run the simulation environment with the RTL model of RD53A, you can follow the same instructions sa above, but skip the bitfile generation.
- Install the large-file-system extension of git to be able to download [our](https://github.com/SiLab-Bonn/bdaq53_data)
  large binary files for testing:
  https://github.com/git-lfs/git-lfs/wiki/Installation

- Download and setup [cocotb](https://github.com/potentialventures/cocotb):
```bash
pip install cocotb
```

- Export the RD53A chip design repository folder to the env. variable `RD53A_SRC`. It will be used as the DUT within the simulation environment:
```bash
export RD53A_SRC={your RD53A chip design directory}
```

- The simulation supports the HDL simulators IUS and Questa. If needed, install one of them.

    ***Questa***: The free simulator package is sufficient. After installation, run:
    ```bash
    export PATH={your Questa directory}/questasim/linux_x86_64:$PATH
    ./install.linux64
    ```

- Setup the simulator, by sourcing one of the `setup_[ius,questa].sh` scripts in `/tests`. This compiles the necessary libraries for the selected simulator.

    For example:
    ```bash
    source setup_questa.sh
    ```

- Run one of the Python tests "*test_....py*" in the `/tests` folder.


### Repository structure
`/firmware` Firmware sources
-  `/src`  Verilog sources and constraint files for the RD53A-DAQ firmware.
-  `/vivado` - TCL script to generate a Vivado project from the source files.

`/bdaq53` Software package including BASIL drivers and configuration.
-  `/scans` Different scans and scan configuration files
-  `/analysis` Software for data anlysis and plotting

`/tests`
Testbench and unittests to run a cosimulation with the DAQ firmare and the RD53 digital design.