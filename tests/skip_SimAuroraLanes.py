#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import logging

import utils
from bdaq53.scans.test_registers import RegisterTest


local_configuration = {'addresses': [1]}


class TestAuroraLanes(unittest.TestCase):
    def test_test_registers(self):
        for lanes in [1, 2, 4]:
            logging.info('Test register write/read with %i Aurora Lane(s)' % (lanes))
            self.test = RegisterTest(utils.setup_cocotb(activelanes_rx=lanes))
            self.test.start(tx_lanes=lanes, **local_configuration)
            
            for address in self.test.values.keys():
                self.assertEqual(self.test.values[address], self.test.results[address])
                
            utils.close_sim(self.test.get_chip())


    def tearDown(self):
        utils.close_sim(self.test.get_chip())

if __name__ == '__main__':
    unittest.main()
