#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import numpy as np
import logging

import utils
from bdaq53.rd53a import RD53A
from bdaq53.fifo_readout import FifoReadout
from bdaq53.analysis import analysis_utils


class TestFifoReadout(unittest.TestCase):
    def setUp(self):
        self.chip = RD53A(utils.setup_cocotb())
        self.chip.init()


    def test_fifo_readout(self):
        logging.info('Starting FIFO readout test')

        # Configure cmd encoder
        self.chip['cmd'].reset()
        self.chip['cmd'].start()

        # Wait for PLL lock
        self.assertTrue(self.chip.wait_for_pll_lock())

        # Setup Aurora
        fr = FifoReadout(self.chip)
        self.chip.set_aurora(write=True)
        self.chip.write_ecr(write=True)
        self.assertTrue(self.chip.wait_for_aurora_sync())
       
        fr.print_readout_status()
        fr.start(fill_buffer=True)

        # insert empty triggers
        indata = self.chip.send_trigger(trigger=0b1111, write=False)*4
        self.chip.write_command(indata)

        # progress simulation
        for _ in range(100):
            self.chip['rx'].get_rx_ready()

        fr.stop()
        fr.print_readout_status()
        
        rawdata = np.concatenate([item[0] for item in fr.data])
        
        hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve = analysis_utils.init_outs(len(rawdata), 0)
        analysis_utils.interpret_data(rawdata, hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, 0)

        self.assertEqual(len(hits), 32)


    def tearDown(self):
        utils.close_sim(self.chip)


if __name__ == '__main__':
    unittest.main()