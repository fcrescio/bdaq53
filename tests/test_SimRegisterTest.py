#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import unittest
import shutil

import utils
from bdaq53.scans.test_registers import RegisterTest


local_configuration = {'addresses': [1, 2, 3, 4, 5, 6, 7, 8, 9,10]}


class TestRegisterTest(unittest.TestCase):
    def test_test_registers(self):
        self.test = RegisterTest(utils.setup_cocotb())
        self.test.start(**local_configuration)
        self.assertTrue(self.test.analyze())



    def tearDown(self):
        utils.close_sim(self.test.get_chip())
        shutil.rmtree('output_data/', ignore_errors=True)

if __name__ == '__main__':
    unittest.main()
