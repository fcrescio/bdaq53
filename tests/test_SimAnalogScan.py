#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib # workaround for matplotlib segmentation fault
import unittest
import logging
import shutil
import tables as tb
import numpy as np

import utils
from bdaq53.scans.scan_analog import AnalogScan

configuration = {
    "n_injections": 2,
    
    "mask_step": 2,
    "start_column": 160,
    "stop_column": 161,
    
    'VCAL_HIGH': 4000,
    'VCAL_MED': 1000,

}

class TestAnalogScan(unittest.TestCase):
    def test_scan_analog(self):
        logging.info('Starting analog scan test')
        
        self.scan = AnalogScan(utils.setup_cocotb(20))
        self.scan.start(**configuration)
        self.scan.analyze()
        
        ''' Assert raw data '''
        with tb.open_file(self.scan.output_filename + '_interpreted.h5', 'r+') as in_file_h5:
            hist_occ = in_file_h5.root.HistOcc[:]
            
        self.assertEqual(np.sum(hist_occ), 192*configuration['n_injections'], 'Incorrect number of hits has been recorded!')
        self.assertEqual(hist_occ[160,:].tolist(), [configuration['n_injections']]*192)
         

    def tearDown(self):
        utils.close_sim(self.scan.get_chip())
        shutil.rmtree('output_data/', ignore_errors=True)


if __name__ == '__main__':
    unittest.main()