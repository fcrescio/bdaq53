#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib  # workaround
import os
import unittest

import tables as tb

from bdaq53.analysis import analysis
import utils

import bdaq53
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data',
                                           'fixtures'))


class TestAnalysis(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        try:
            test_files = ['digital_scan', 'analog_scan', 'threshold_scan']
            for test_file in test_files:
                os.remove(os.path.join(data_folder,
                                       test_file + '_interpreted.h5'))
                os.remove(os.path.join(data_folder,
                                       test_file + '_interpreted.pdf'))

            os.remove(os.path.join(data_folder, 'last_scan.pdf'))
            os.remove(os.path.join(data_folder, 'analog_scan_clustered.h5'))
            os.remove(os.path.join(data_folder, 'analog_scan_clustered.pdf'))
            os.remove(os.path.join(data_folder,
                                   'analog_scan_clustered_prop.h5'))
            os.remove(os.path.join(data_folder,
                                   'analog_scan_clustered_prop.pdf'))
        except OSError:
            pass

    def test_dig_scan_ana(self):
        ''' Test analysis of digital scan data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan.h5')
        with analysis.AnalyzeRawData(raw_data_file=raw_data_file) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'digital_scan_interpreted.h5'),
            os.path.join(data_folder, 'digital_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_ana_scan_ana(self):
        ''' Test analysis of analog scan data '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        with analysis.AnalyzeRawData(raw_data_file=raw_data_file) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_interpreted.h5'),
            os.path.join(data_folder, 'analog_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_thr_scan_ana(self):
        ''' Test analysis of threshold scan data '''
        raw_data_file = os.path.join(data_folder, 'threshold_scan.h5')
        with analysis.AnalyzeRawData(raw_data_file=raw_data_file) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_interpreted.h5'),
            os.path.join(data_folder, 'threshold_scan_interpreted_result.h5'),
            node_names=['Hits', 'HistOcc', 'HistRelBCID', 'HistTot',
                        'HistSCurve'],
            exact=True)
        self.assertTrue(data_equal, msg=error_msg)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_interpreted.h5'),
            os.path.join(data_folder, 'threshold_scan_interpreted_result.h5'),
            node_names=['NoiseMap', 'ThresholdMap'],
            exact=False,
            # Threshold/Noise can be 2% off due to insufficient fit
            # implementation
            rtol=0.02)
        self.assertTrue(data_equal, msg=error_msg)

    def test_clustering_full(self):
        ''' Test cluster results against fixture '''
        with analysis.AnalyzeRawData(
                raw_data_file=os.path.join(data_folder,
                                           'analog_scan.h5'),
                analyzed_data_file=os.path.join(
                    data_folder,
                    'analog_scan_clustered.h5'),
                cluster_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_clustered.h5'),
            os.path.join(data_folder, 'analog_scan_clustered_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_clustering_properties(self):
        ''' Check validity of cluster results '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        analyzed_data_file = os.path.join(
            data_folder, 'analog_scan_clustered_prop.h5')
        with analysis.AnalyzeRawData(raw_data_file=raw_data_file,
                                     analyzed_data_file=analyzed_data_file,
                                     cluster_hits=True) as a:
            a.analyze_data()

        # Deduce properties from hit table
        with tb.open_file(analyzed_data_file) as in_file:
            hits = in_file.root.Hits[:]
            # Omit late hits do prevent double counting
            # Only ToT < 14 is clustered
            hits = hits[hits['tot'] < 14]
            n_hits = hits.shape[0]
            tot_sum = hits['tot'].sum()

        with tb.open_file(analyzed_data_file) as in_file:
            self.assertEqual(in_file.root.Cluster[:]['size'].sum(), n_hits)
            self.assertEqual(in_file.root.Cluster[:]['tot'].sum(), tot_sum)


if __name__ == '__main__':
    unittest.main()
