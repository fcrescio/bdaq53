

#---------------------------------------------------------------------
#  Take data file and inteprets hits and triggers, passes them to chip
#--------------------------------------------------------------------
# Author/s: T. Hemperek

import cocotb
import logging
from cocotb.binary import BinaryValue
from cocotb.triggers import RisingEdge, ReadOnly, Timer
from cocotb.drivers import BusDriver
from cocotb.result import ReturnValue
from cocotb.clock import Clock

from basil.utils.BitLogic import BitLogic

import csv
import requests
import sys
import numpy as np
from __builtin__ import int


class HitDataFile(BusDriver):

    _signals = ['CLK_BX', 'HIT', 'TRIGGER', 'RESET_TB']

    def __init__(self, entity, filename):
        BusDriver.__init__(self, entity, "", entity.CLK_BX)

        val = '0' * len(self.bus.HIT)
        self.hit = BinaryValue(bits=len(self.bus.HIT), value=val )
        self.filename = filename
        logging.info('Loading file... '+ filename)

    @cocotb.coroutine
    def run(self):

        self.bus.HIT <= self.hit
        self.bus.TRIGGER <= 0

        bx = 0
        total_triggers = 0
        total_hits = 0

        while 1:
            yield RisingEdge(self.clock)
            yield ReadOnly()
            res = self.bus.RESET_TB.value.integer
            if(res == 0):
                break

        while 1:
            yield RisingEdge(self.clock)
            yield ReadOnly()
            res = self.bus.RESET_TB.value.integer
            if(res == 1):
                break

        bv = BitLogic(len(self.hit))
        tot_hist = np.full([len(self.hit)], 0, dtype = np.uint8)
        trigger = 0

        with open(self.filename) as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',')
            for file_row in csv_reader:

                bcid = int(file_row[0])
                col = int(file_row[1])
                row = int(file_row[2])
                tot = int(file_row[3])
                trg = int(file_row[4])

                #print('loading bcid=', bcid, 'mc=', col, 'reg=', row, 'tot=', tot, 'trg=', trg, 'bx=', bx)

                while bcid > bx:
                    yield RisingEdge(self.clock)
                    yield Timer(5000)

                    for pix in np.where(tot_hist>0)[0]:
                        bv[pix] = 1
                        total_hits += 1

                    self.hit.assign(str(bv));
                    self.bus.HIT <= self.hit
                    self.bus.TRIGGER <= trigger

                    tot_hist[tot_hist>0] -= 1
                    trigger = 0
                    bv.setall(False)

                    bx += 1

                if bcid == bx:
                    if trg:
                        trigger = 1
                        total_triggers += 1

                    if( (col >= 0) and (col < 400) and  (row >= 0) and (row < 384) ):
                        pixn = ((row)+(col*384))
                        tot_hist[pixn] = tot_hist[pixn] + tot
                else:
                    raise ValueError("Error")


        yield RisingEdge(self.clock)
        yield Timer(5000)
        self.bus.HIT <= 0
        self.bus.TRIGGER <= 0
        logging.info('End of self.filename')
        logging.info('%u triggers and %u hits sent to DUT',total_triggers, total_hits)
        print total_triggers,'triggers and',total_hits,'hits sent to DUT'