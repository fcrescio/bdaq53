#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib # workaround for matplotlib segmentation fault
import time
import os
import yaml
import logging
import subprocess
import basil
import zmq
import pkg_resources
import inspect
import tables as tb
import numpy as np

from tqdm import tqdm
from tables.exceptions import NoSuchNodeError
from contextlib import contextmanager

from rd53a import RD53A
from fifo_readout import FifoReadout


VERSION = pkg_resources.get_distribution("bdaq53").version
loglevel = logging.getLogger('RD53A').getEffectiveLevel()

def get_software_version():
    try:
        rev = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).strip()
        branch = subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip()
        return branch + '@' + rev
    except:
        return VERSION


class MetaTable(tb.IsDescription):
    index_start = tb.UInt32Col(pos=0)
    index_stop = tb.UInt32Col(pos=1)
    data_length = tb.UInt32Col(pos=2)
    timestamp_start = tb.Float64Col(pos=3)
    timestamp_stop = tb.Float64Col(pos=4)
    scan_param_id = tb.UInt16Col(pos=5)
    error = tb.UInt32Col(pos=6)
    trigger = tb.Float64Col(pos=7)


def send_data(socket, data, scan_par_id, name='ReadoutData'):
    '''Sends the data of every read out (raw data and meta data)

        via ZeroMQ to a specified socket
    '''

    data_meta_data = dict(
        name=name,
        dtype=str(data[0].dtype),
        shape=data[0].shape,
        timestamp_start=data[1],  # float
        timestamp_stop=data[2],  # float
        error=data[3],  # int
        scan_par_id=scan_par_id
    )
    try:
        socket.send_json(data_meta_data,flags=zmq.SNDMORE | zmq.NOBLOCK)
        # PyZMQ supports sending numpy arrays without copying any data
        socket.send(data[0], flags=zmq.NOBLOCK)
    except zmq.Again:
        pass


class ScanBase(object):
    '''
        Basic run meta class.
        Base class for scan- / tune- / analyze-class.
    '''

    def __init__(self, dut_conf=None):
        self.working_dir = os.path.join(os.getcwd(), "output_data")
        if not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)
            
        self.timestamp = time.strftime("%Y%m%d_%H%M%S")
        self.run_name = self.timestamp + '_' + self.scan_id
        self.output_filename = os.path.join(self.working_dir, self.run_name)
        
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        self.setup_logfile()

        self.logger.info('Initializing %s...', self.__class__.__name__)
        self.chip = RD53A(dut_conf)


    def get_basil_dir(self):
        return str(os.path.dirname(os.path.dirname(basil.__file__)))


    def get_chip(self):
        return self.chip


    def load_disable_mask(self, **kwargs):
        mask = np.ones((400,192), dtype=bool)
        disable = kwargs.get('disable', None)
        
        if self.maskfile:
            try:
                with tb.open_file(self.maskfile, 'r') as infile:
                    mask = infile.root.disable_mask[:]
            except NoSuchNodeError:
                self.logger.debug('Specified maskfile does not include a disable_mask!')
                pass
                
        if disable is not None:
            if isinstance(disable, list):
                for tup in disable:
                    mask[tup[0], tup[1]] = False
            elif isinstance(disable, np.ndarray):
                mask = disable
            
        for col in range(400):
            for row in range(192):
                if not mask[col, row]:
                    self.chip.enable_mask[col, row] = False

    def load_enable_mask(self, **kwargs):
        self.chip.enable_mask[kwargs.get('start_column', 0):kwargs.get('stop_column', 400),
                              kwargs.get('start_row', 0):kwargs.get('stop_row', 192)] = True
    
    
    def prepare_injection_masks(self, start_column, stop_column, start_row, stop_row, mask_step, column_step, progressbar=None):
        mask_data = []
        crange = column_step if column_step < (stop_column - start_column) else (stop_column - start_column)
        
        if not progressbar:
            pbar = tqdm(total=crange*mask_step*len(range(start_column, stop_column, column_step)[::8]))
        
        for start in range(start_column, stop_column, column_step)[::8]:
            stop = start + column_step * 8
            if stop > stop_column: stop = stop_column
            
            for c in range(crange):
                for m in range(mask_step):
                    self.chip.injection_mask[:,:] = False
                    self.chip.injection_mask[start+c:stop:column_step, start_row+m:stop_row:mask_step] = True
                    
                    write_range = []
                    for i in range(start+c,stop,column_step):
                        write_range.append(i)
                        if not i == 0:
                            write_range.append(i-1)
                    
                    mask_data.append(self.chip.write_masks(write_range, write=False))
                    if not progressbar:
                        pbar.update(1)
                    else:
                        progressbar.update(1)
            
            self.chip.injection_mask[:,:] = False
            mask_data.append(self.chip.write_masks(range(start,stop), write=False))
            
        if not progressbar:
            pbar.close()
        return mask_data
    
    
    def save_disable_mask(self, update=True):
        self.logger.info('Writing disable mask to file...')
        if not self.maskfile:
            self.maskfile = os.path.join(self.working_dir, self.timestamp + '_mask.h5')
        
        new_mask = self.disable_mask
        with tb.open_file(self.maskfile, 'a') as out_file:
            try:
                if update:
                    new_mask = out_file.root.disable_mask[:]
                    self.logger.debug('Updating existing mask...')
                    for col in range(400):
                        for row in range(192):
                            if not self.disable_mask[col, row]:
                                new_mask[col, row] = False
                out_file.remove_node(out_file.root.disable_mask)
            except NoSuchNodeError:
                self.logger.debug('Specified maskfile does not include a disable_mask yet!')
            
            out_file.create_carray(out_file.root,
                                   name='disable_mask',
                                   title='Disable mask',
                                   obj=new_mask,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            self.logger.info('Closing disable mask file: %s' % (self.maskfile))
            
    
    def save_tdac_mask(self):
        self.logger.info('Writing TDAC mask to file...')
        if not self.maskfile:
            self.maskfile = os.path.join(self.working_dir, self.timestamp + '_mask.h5')
        
        with tb.open_file(self.maskfile, 'a') as out_file:
            try:
                out_file.remove_node(out_file.root.TDAC_mask)
            except NoSuchNodeError:
                self.logger.debug('Specified maskfile does not include a TDAC_mask yet!')
            
            out_file.create_carray(out_file.root,
                                       name='TDAC_mask',
                                       title='TDAC mask',
                                       obj=self.TDAC_mask,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))
            self.logger.info('Closing TDAC mask file: %s' % (self.maskfile))
    

    def get_latest_maskfile(self):
        candidates = []
        for _, _, files in os.walk(self.working_dir):
            for f in files:
                if 'mask' in f and f.split('.')[-1] == 'h5':
                    candidates.append({'name':f, 'modified':os.path.getmtime(os.path.join(self.working_dir, f))})
                    
        if len(candidates) == 0:
            self.maskfile = None
            return
        
        maskfile = candidates[0]
        for candidate in candidates:
            if candidate['modified'] > maskfile['modified']:
                maskfile = candidate
        
        self.maskfile = os.path.join(self.working_dir, maskfile['name'])


    def configure(self, **kwargs):
        '''
            Configuring step before scan start
        '''
        
        self.logger.info('Configuring chip...')
        self.chip.set_dacs(**kwargs)
        self.chip.set_tdac(**kwargs)
        self.load_enable_mask(**kwargs)
        self.load_disable_mask(**kwargs)
        if self.chip.hw_map[self.chip.board_version] != 'SIMULATION':
            self.chip.write_masks()
    
    
    def start(self, **kwargs):
        '''
            Prepares the scan and starts the actual test routine
        '''
        
        self._first_read = False
        self.scan_param_id = 0
        
        self.chip.power_on(**kwargs)
        self.chip.init()
        
        self.fifo_readout = FifoReadout(self.chip)
        
        self.chip.init_communication()
        self.chip.reset_chip()
        
        self.maskfile = kwargs.get('maskfile', None)
        if self.maskfile == 'auto':
            self.get_latest_maskfile()
            kwargs['maskfile'] = self.maskfile
        
        self.configure(**kwargs)
        
        filename = self.output_filename + '.h5'
        filter_raw_data = tb.Filters(complib='blosc', complevel=5, fletcher32=False)
        self.filter_tables = tb.Filters(complib='zlib', complevel=5, fletcher32=False)
        self.h5_file = tb.open_file(filename, mode='w', title=self.scan_id)
        self.raw_data_earray = self.h5_file.create_earray(self.h5_file.root, name='raw_data', atom=tb.UIntAtom(),
                                                         shape=(0,), title='raw_data', filters=filter_raw_data)
        self.meta_data_table = self.h5_file.create_table(self.h5_file.root, name='meta_data', description=MetaTable,
                                                        title='meta_data', filters=self.filter_tables)
        
        self.meta_data_table.attrs.run_name = self.run_name
        self.meta_data_table.attrs.scan_id = self.scan_id
        self.meta_data_table.attrs.dacs = yaml.dump(self.chip.dacs)
        self.meta_data_table.attrs.chip_id = kwargs.get('chip_id', '0x0000')
        self.meta_data_table.attrs.software_version = get_software_version()
        
        tdac = kwargs.pop('TDAC', None)
        disable = kwargs.pop('disable', None)
        if isinstance(tdac, int):
            kwargs['TDAC'] = tdac
            
        defaults = inspect.getargspec(self.scan)
        for ix, key in enumerate(defaults.args[1:]):
            if not key in kwargs:
                kwargs[key] = defaults.defaults[ix]
        
        self.meta_data_table.attrs.kwargs = yaml.dump(kwargs)
        kwargs['TDAC'] = tdac
        kwargs['disable'] = disable
        
        # Setup data sending
        socket_addr = kwargs.pop('send_data', 'tcp://127.0.0.1:5500')
        if socket_addr:
            try:
                self.context = zmq.Context()
                self.socket = self.context.socket(zmq.PUB)  # publisher socket
                self.socket.bind(socket_addr)
                self.logger.debug('Sending data to server %s', socket_addr)
            except zmq.error.ZMQError:
                self.logger.exception('Cannot connect to socket for data sending.')
                self.socket = None
        else:
            self.socket = None

        if self.chip.periphery:
            self.meta_data_table.attrs.ps_currents_before = yaml.dump(self.chip.get_power(log=True))
        
        self.scan(**kwargs)
        
        if self.chip.periphery:
            self.meta_data_table.attrs.ps_currents_after = yaml.dump(self.chip.get_power())

        self.fifo_readout.print_readout_status()

        # Read all important chip values and dump to yaml
        if self.chip.hw_map[self.chip.board_version] != 'SIMULATION':
            voltages, currents = self.chip.get_chip_status()
            self.meta_data_table.attrs.dac_voltages = voltages
            self.meta_data_table.attrs.dac_currents = currents

        self.logger.info('Closing raw data file: %s', self.output_filename + '.h5')
        self.h5_file.close()

        if self.socket:
            self.logger.debug('Closing socket connection')
            self.socket.close()
            self.socket = None

        self.chip['cmd'].reset()


    def analyze(self):
        raise NotImplementedError('ScanBase.analyze() not implemented')


    def scan(self, **kwargs):
        raise NotImplementedError('ScanBase.scan() not implemented')


    @contextmanager
    def readout(self, *args, **kwargs):
        timeout = kwargs.pop('timeout', 10.0)

        # self.fifo_readout.readout_interval = 10
        if not self._first_read:
            self.fifo_readout.print_readout_status()
            self._first_read = True
            
            self.chip['rx'].reset()
            self.chip.wait_for_aurora_sync()
        
        self.start_readout(*args, **kwargs)
        yield
        self.fifo_readout.stop(timeout=timeout)


    def start_readout(self, scan_param_id=0, *args, **kwargs):
        # Pop parameters for fifo_readout.start
        callback = kwargs.pop('callback', self.handle_data)
        clear_buffer = kwargs.pop('clear_buffer', False)
        fill_buffer = kwargs.pop('fill_buffer', False)
        reset_sram_fifo = kwargs.pop('reset_sram_fifo', True)
        errback = kwargs.pop('errback', self.handle_err)
        no_data_timeout = kwargs.pop('no_data_timeout', None)
        self.scan_param_id = scan_param_id
        self.fifo_readout.start(reset_sram_fifo=reset_sram_fifo, fill_buffer=fill_buffer, clear_buffer=clear_buffer,
                                callback=callback, errback=errback, no_data_timeout=no_data_timeout)


    def handle_data(self, data_tuple):
        '''
            Handling of the data.
        '''
#         get_bin = lambda x, n: format(x, 'b').zfill(n)

        total_words = self.raw_data_earray.nrows

        self.raw_data_earray.append(data_tuple[0])
        self.raw_data_earray.flush()

        len_raw_data = data_tuple[0].shape[0]
        self.meta_data_table.row['timestamp_start'] = data_tuple[1]
        self.meta_data_table.row['timestamp_stop'] = data_tuple[2]
        self.meta_data_table.row['error'] = data_tuple[3]
        self.meta_data_table.row['data_length'] = len_raw_data
        self.meta_data_table.row['index_start'] = total_words
        total_words += len_raw_data
        self.meta_data_table.row['index_stop'] = total_words
        self.meta_data_table.row['scan_param_id'] = self.scan_param_id
        
        self.meta_data_table.row.append()
        self.meta_data_table.flush()

        if self.socket:
            send_data(self.socket, data=data_tuple, scan_par_id=self.scan_param_id)


    def handle_err(self, exc):
        msg = '%s' % exc[1]
        if msg:
            self.logger.error('%s Aborting run...', msg)
        else:
            self.logger.error('Aborting run...')


    def setup_logfile(self):
        self.fh = logging.FileHandler(self.output_filename + '.log')
        self.fh.setLevel(loglevel)
        self.fh.setFormatter(logging.Formatter("%(asctime)s - [%(name)-15s] - %(levelname)-7s %(message)s"))
        for lg in logging.Logger.manager.loggerDict.itervalues():
            if isinstance(lg, logging.Logger):
                lg.addHandler(self.fh)
                
        return self.fh


    def close_logfile(self):
        for lg in logging.Logger.manager.loggerDict.itervalues():
            if isinstance(lg, logging.Logger):
                lg.removeHandler(self.fh)


    def close(self):
        self.chip.close()
        self.close_logfile()
