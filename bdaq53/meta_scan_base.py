#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib # workaround
import time
import os
import shutil
import logging
import subprocess
import pkg_resources

import tables as tb

from rd53a import RD53A
from scan_base import ScanBase


VERSION = pkg_resources.get_distribution("bdaq53").version
loglevel = logging.getLogger('RD53A').getEffectiveLevel()


def get_software_version():
    try:
        rev = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).strip()
        branch = subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip()
        return branch + '@' + rev
    except:
        return VERSION


class MetaScanBase(ScanBase):
    '''
        Basic run meta class.
        Base class for scan- / tune- / analyze-class.
    '''

    def __init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        
        self.working_dir = os.path.join(os.getcwd(), "output_data")
        if not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)
        self.timestamp = time.strftime("%Y%m%d_%H%M%S")
        self.run_name =  self.timestamp + '_' + self.scan_id
        self.output_filename = os.path.join(self.working_dir, self.run_name)
        self.setup_logfile()
                
        self.logger.info('Initializing %s...' % self.scan_id)
        self.scans = []


    def start(self, **kwargs):
        '''
            Prepares the scan and starts the actual test routine
        '''
        
        self.scan(**kwargs)
        self.clean_up()

    
    def clean_up(self):
        self.logger.info('Cleaning up output data...')
        
        with tb.open_file(self.output_filename + '.h5', mode='w', title=self.run_name) as outfile:
            for scanfile in self.scans:
                with tb.open_file(scanfile.output_filename + '.h5', mode='r') as infile:
                    group = outfile.create_group(outfile.root, infile.title)
                    infile.copy_node(infile.root, group)
                os.remove(scanfile.output_filename + '.log')
                os.remove(scanfile.output_filename + '.h5')
        
                    
        with tb.open_file(self.output_filename + '_interpreted.h5', mode='w', title=self.run_name) as outfile:
            for scanfile in self.scans:
                with tb.open_file(scanfile.output_filename + '_interpreted.h5', mode='r') as infile:
                    group = outfile.create_group(outfile.root, infile.title)
                    infile.copy_node(infile.root, group)
                os.remove(scanfile.output_filename + '_interpreted.h5')
                try:
                    os.remove(scanfile.output_filename + '_interpreted.pdf')
                except OSError:
                    pass

    def analyze(self):
        pass
