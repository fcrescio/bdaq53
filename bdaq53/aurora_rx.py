#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from basil.HL.RegisterHardwareLayer import RegisterHardwareLayer
import logging

loglevel = logging.getLogger('RD53A').getEffectiveLevel()


class aurora_rx(RegisterHardwareLayer):
    '''
    '''

    _registers = {'RESET':      {'descr': {'addr': 0, 'size': 8, 'properties': ['writeonly']}},
                  'VERSION':    {'descr': {'addr': 0, 'size': 8, 'properties': ['ro']}},

                  'EN':                 {'descr': {'addr': 2, 'size': 1, 'offset': 0}},
                  'RX_READY':           {'descr': {'addr': 2, 'size': 1, 'offset': 1, 'properties': ['readonly']}},
                  'RX_LANE_UP':         {'descr': {'addr': 2, 'size': 1, 'offset': 2, 'properties': ['readonly']}},
                  'PLL_LOCKED':         {'descr': {'addr': 2, 'size': 1, 'offset': 3, 'properties': ['readonly']}},
                  'RX_HARD_ERROR':      {'descr': {'addr': 2, 'size': 1, 'offset': 4, 'properties': ['readonly']}},
                  'RX_SOFT_ERROR':      {'descr': {'addr': 2, 'size': 1, 'offset': 5, 'properties': ['readonly']}},
                  'MGT_REF_SEL':        {'descr': {'addr': 2, 'size': 1, 'offset': 6}},
                  'USER_K_FILTER_EN' :  {'descr': {'addr': 2, 'size': 1, 'offset': 7}},

                  'LOST_COUNT':         {'descr': {'addr': 3, 'size': 8, 'properties': ['ro']}},

                  'USER_K_FILTER_MASK_1': {'descr': {'addr': 4, 'size': 8}},
                  'USER_K_FILTER_MASK_2': {'descr': {'addr': 5, 'size': 8}},
                  'USER_K_FILTER_MASK_3': {'descr': {'addr': 6, 'size': 8}},

                  'RESET_COUNTERS':     {'descr': {'addr': 7, 'size': 1, 'offset': 0}},
                  'SI570_IS_CONFIGURED':{'descr': {'addr': 7, 'size': 1, 'offset': 1}},


                  'FRAME_COUNTER':      {'descr': {'addr': 8, 'size': 32}}
                  }
    _require_version = "==1"

    def __init__(self, intf, conf):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
                
        super(aurora_rx, self).__init__(intf, conf)

    def reset(self):
        '''Soft reset the module.'''
        self.RESET = 0

    def set_en(self, value):
        self.EN = value

    def get_en(self):
        return self.EN

    # Aurora link established
    def get_rx_ready(self):
        return self.RX_READY

    # Aurora PLL
    def get_pll_locked(self):
        return self.PLL_LOCKED

    def get_lost_count(self):
        return self.LOST_COUNT

    def set_mgt_ref(self, value):
        if value == "int":
            self.logger.info('MGT: Switching to on-board (Si570) oscillator')
            self.MGT_REF_SEL = 1
        elif value == "ext":
            self.logger.info('MGT: Switching to external (SMA) clock source')
            self.MGT_REF_SEL = 0

    def get_mgt_ref(self):
        value = self.MGT_REF_SEL
        if value == 0:
            return 'EXT (SMA)'
        elif value == 1:
            return 'INT (Si570)'

    def get_RX_HARD_ERROR(self):
        return self.RX_HARD_ERROR

    def get_RX_SOFT_ERROR(self):
        return self.RX_SOFT_ERROR

    def get_USER_K_FILTER_MASK(self):
        return self.USER_K_FILTER_MASK

    def set_USER_K_FILTER_MASK(self, mask, value):
        if mask == 1:
            self.USER_K_FILTER_MASK_1 = value
        elif mask == 2:
            self.USER_K_FILTER_MASK_2 = value
        elif mask == 3:
            self.USER_K_FILTER_MASK_3 = value
        else:
            self.logger.error("USER_K_FILTER_MASK: Parameters: mask_number[1,2], value[byte]")

    def get_USER_K_FILTER_EN(self):
        return self.USER_K_FILTER_EN

    def set_USER_K_FILTER_EN(self, value):
        self.USER_K_FILTER_EN = value

    def get_frame_count(self):
        return self.FRAME_COUNTER

    def set_Si570_is_configured(self, value):
        self.SI570_IS_CONFIGURED = value

    def get_Si570_is_configured(self):
        return self.SI570_IS_CONFIGURED

    def reset_counters(self):
        self.RESET_COUNTERS = True;
        self.RESET_COUNTERS = False;
