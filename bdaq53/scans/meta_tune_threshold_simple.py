#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This meta script first scans over the global threshold at fixed TDAC of 7 and sets the chip to the mean optimal VTH.
    It then scans over the local threshold (TDAC) with fixed global threshold to find the optimal TDAC value for each pixel.
'''

import zlib # Workaround
import os
import yaml

import numpy as np
import tables as tb

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_threshold import ThresholdScan


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'            : 1.25,
    'VDDD'            : 1.25,
    'VDDA_cur_lim'    : 0.7,
    'VDDD_cur_lim'    : 0.7,
    
    # Scan parameters
    'column_step'     : 1,
    'start_column'    : 160,
    'stop_column'     : 168,
    'start_row'       : 0,
    'stop_row'        : 192,
    'maskfile'        : None,
    
    # Threshold scan parameters
    'VCAL_MED'        : 500,
    'VCAL_HIGH_start' : 500,
    'VCAL_HIGH_stop'  : 1200,
    'VCAL_HIGH_step'  : 10,

    # General DAC settings
    'Vthreshold_LIN'  : 350,
    'REF_KRUM_LIN'    : 250,
    'LDAC_LIN'        : 130
    }


class MetaTDACTuning(MetaScanBase):
    scan_id = 'meta_tdac_tuning'

    def start(self, **kwargs):
        self.scan(**kwargs)
        self.clean_up()


    def scan(self, **kwargs):
        VCAL_MED = kwargs.get('VCAL_MED', 500)
        th = np.zeros((16,400,192))
        
        scan_range_mask = np.zeros((400,192), dtype = bool)
        scan_range_mask[kwargs.get('start_column', 0):kwargs.get('stop_column', 400),
                        kwargs.get('start_row', 0):kwargs.get('stop_row', 192)] = True

        for tdac in range(16):
            kwargs['TDAC'] = tdac
            self.logger.info('Running threshold scan for TDAC %d' %(tdac))
            scan = ThresholdScan()
            scan.logger.addHandler(self.fh)
            scan.start(**kwargs)
            scan.analyze(create_pdf=True)
            scan.close()
            
            with tb.open_file(scan.output_filename + '_interpreted.h5') as in_file:
                th[tdac] = in_file.root.ThresholdMap[:]
                ''' Debugging output '''
#                 np.set_printoptions(linewidth = 120)
#                 th_hist = np.histogram(th[tdac][scan_range_mask].flatten(), bins=25)
#                 self.logger.info('Threshold for TDAC=%d %s' %(tdac, str(th_hist[0])))
            
            os.remove(scan.output_filename + '.log')
            os.remove(scan.output_filename + '.h5')
            os.remove(scan.output_filename + '_interpreted.h5')
            os.remove(scan.output_filename + '_interpreted.pdf')
        
        mean = np.mean(th[:,scan_range_mask])
        self.logger.info('Mean threshold is %f' %(mean))
        self.tdac = np.argmin(np.abs(th - mean),axis = 0)
        self.tdac[~scan_range_mask] = 15
        tdac_dist = np.bincount(self.tdac[scan_range_mask].flatten())
        self.logger.info('Final TDAC dstibiution: %s' % (str(tdac_dist)))
        
        self.logger.info('Applying TDAC mask and running final threshold scan...')
        kwargs['VCAL_HIGH_start'] = int(mean) - 50 + VCAL_MED
        kwargs['VCAL_HIGH_stop'] = int(mean) + 50 + VCAL_MED
        kwargs['VCAL_HIGH_step'] = 2
        kwargs['TDAC'] = self.tdac
        
        self.ts = ThresholdScan()
        self.scans.append(self.ts)
        self.ts.logger.addHandler(self.fh)
        self.ts.start(**kwargs)
        self.ts.analyze(create_pdf=True)
        self.ts.close()
        
        self.logger.info('Tuning finished!')

        
    def analyze(self, create_mask_file=True):
        if create_mask_file:
            self.logger.info('Writing TDAC mask to file...')
            self.maskfile = os.path.join(self.working_dir, '_'.join(self.run_name.split('_')[:2]) + '_TDAC_mask.h5')
            with tb.open_file(self.maskfile, 'w') as out_file:
                out_file.create_carray(out_file.root,
                                       name='TDAC_mask',
                                       title='TDAC mask',
                                       obj=self.tdac,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))
            self.logger.info('Closing TDAC mask file: %s' % (self.maskfile))
        return self.tdac


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    mtt = MetaTDACTuning()
    mtt.start(**configuration)
    mtt.analyze()
