#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a digital pulse into
    enabled pixels to test the digital part of the chip.
'''

import yaml
import numpy as np
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'         : 1.25,
    'VDDD'         : 1.25,
    'VDDA_cur_lim' : 0.7,
    'VDDD_cur_lim' : 0.7,
    
    # Scan parameters
    'start_column' : 0,
    'stop_column'  : 400,
    'start_row'    : 0,
    'stop_row'     : 192,
    'maskfile'     : 'auto'
    }


def set_logo_mask(self, image='../logo.bmp'):
    from pylab import imread
    
    img = np.invert(imread(image).astype('bool'))
    
    self.chip.enable_mask[105:295, 1:191] = np.transpose(img[:, :, 0])
    self.chip.injection_mask[105:295, 1:191] = np.transpose(img[:, :, 0])
    
    self.chip.enable_mask[:,0] = False
    self.chip.injection_mask[:,0] = False
    self.chip.enable_mask[:,-1] = False
    self.chip.injection_mask[:,-1] = False
    self.chip.enable_mask[0:105,:] = False
    self.chip.injection_mask[0:105,:] = False
    self.chip.enable_mask[295:400,:] = False
    self.chip.injection_mask[295:400,:] = False



class DigitalScan(ScanBase):
    scan_id = "digital_scan"


    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=4, column_step=8, n_injections=100, **kwargs):
        '''
        Digital scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        column_step : int
            Column-stepsize of the injection mask. Every column_stepth row is injected at once, up to a maximum of 8.
        n_injections : int
            Number of injections.
        '''
        
        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step, column_step)
        
        self.logger.info('Starting scan...')     
        pbar = tqdm(total=len(mask_data)) 
        with self.readout():
            for mask in mask_data:
                self.chip.write_command(mask)
                self.chip.inject_digital(repetitions=n_injections)
                pbar.update(1)
                    
        pbar.close()
        self.logger.info('Scan finished')


    def analyze(self, create_pdf=True):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5', create_pdf=create_pdf) as a:
            a.analyze_data()
            if create_pdf:
                a.logger.info('Creating selected plots...')
                a.create_parameter_page()
                a.create_occupancy_map()
                a.create_tot_plot()
                a.create_rel_bcid_plot()
                if a.cluster_hits:
                    a.create_cluster_size_plot()
                    a.create_cluster_tot_plot()
                    a.create_cluster_shape_plot()


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = DigitalScan()
    scan.start(**configuration)
    scan.analyze()
