#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a specified charge into
    enabled pixels to test the analog front-end.
'''

import yaml
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'         : 1.25,
    'VDDD'         : 1.25,
    'VDDA_cur_lim' : 0.7,
    'VDDD_cur_lim' : 0.7,
    
    # Scan parameters
    'start_column' : 0,
    'stop_column'  : 400,
    'start_row'    : 0,
    'stop_row'     : 192,
    'maskfile'     : 'auto',
    
    # DAC settings
    'VCAL_MED'     : 500,
    'VCAL_HIGH'    : 1300
    }


class AnalogScan(ScanBase):
    scan_id = "analog_scan"
    
    
    def configure(self, VCAL_MED=1000, VCAL_HIGH=4000, **kwargs):
        super(AnalogScan, self).configure(**kwargs)
        self.chip.setup_analog_injection(vcal_high=VCAL_HIGH, vcal_med=VCAL_MED)


    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=4, column_step=8, n_injections=100, **kwargs):
        '''
        Analog scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        column_step : int
            Column-stepsize of the injection mask. Every column_stepth row is injected at once, up to a maximum of 8.
        n_injections : int
            Number of injections.
            
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            VCAL_HIGH DAC value.
        '''
        
        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step, column_step)
        
        self.logger.info('Starting scan...')     
        pbar = tqdm(total=len(mask_data)) 
        with self.readout():
            for mask in mask_data:
                self.chip.write_command(mask)
                self.chip.inject_analog_single(repetitions=n_injections)
                pbar.update(1)
                    
        pbar.close()
        self.logger.info('Scan finished')

        
    def analyze(self, create_pdf=True):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5', create_pdf=create_pdf) as a:
            a.analyze_data()
            if create_pdf:
                a.logger.info('Creating selected plots...')
                a.create_parameter_page()
                a.create_occupancy_map()
                a.create_tot_plot()
                a.create_rel_bcid_plot()
                if a.cluster_hits:
                    a.create_cluster_size_plot()
                    a.create_cluster_tot_plot()
                    a.create_cluster_shape_plot()
            


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = AnalogScan()
    scan.start(**configuration)
    scan.analyze()
