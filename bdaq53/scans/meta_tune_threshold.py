#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This meta script first scans over the global threshold at fixed TDAC of 7 and sets the chip to the mean optimal VTH.
    It then scans over the local threshold (TDAC) with fixed global threshold to find the optimal TDAC value for each pixel.
'''

import zlib # Workaround
import os
import shutil
import yaml

import numpy as np
import tables as tb

from matplotlib.backends.backend_pdf import PdfPages

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_threshold import ThresholdScan
from bdaq53.scans.tune_global_threshold import ThresholdTuning
from bdaq53.scans.tune_local_threshold import TDACTuning
from bdaq53.analysis import plotting


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'            : 1.25,
    'VDDD'            : 1.25,
    'VDDA_cur_lim'    : 0.7,
    'VDDD_cur_lim'    : 0.7,
    
    # Scan parameters
    'start_column'    : 128,
    'stop_column'     : 264,
    'start_row'       : 0,
    'stop_row'        : 192,
    'maskfile'        : None,
    
    # Global threshold tuning parameters
    'VCAL_MED'        : 500,
    'VCAL_HIGH'       : 1000,
    
    'VTH_name'        : 'Vthreshold_LIN',
    'VTH_start'       : 380,
    'VTH_stop'        : 280,
    'VTH_step'        : 2,

    # Final threshold scan parameters
    'VCAL_HIGH_start' : 800,
    'VCAL_HIGH_stop'  : 1200,
    'VCAL_HIGH_step'  : 5
    }


class MetaThrTuning(MetaScanBase):
    scan_id = 'meta_threshold_tuning'

    def start(self, **kwargs):
        self.scan(**kwargs)
        self.clean_up()


    def scan(self, **kwargs):
        self.logger.info('Centering TDAC and starting global threshold tuning...')
        kwargs['TDAC'] = 7
        
        self.gt = ThresholdTuning()
        self.scans.append(self.gt)
        self.gt.logger.addHandler(self.fh)
        self.gt.start(**kwargs)
        vth_opt = self.gt.analyze(create_pdf=False, **kwargs)
        kwargs[kwargs['VTH_name']] = vth_opt
        self.logger.info('Found optimal %s = %i.' % (kwargs['VTH_name'], vth_opt))
        self.gt.close()
        
        self.logger.info('Setting global threshold to optimum and starting TDAC tuning...')
        self.lt = TDACTuning()
        self.scans.append(self.lt)
        self.lt.logger.addHandler(self.fh)
        self.lt.start(**kwargs)
        self.lt.analyze(create_pdf=False, create_mask_file=True)
        kwargs['maskfile'] = self.lt.maskfile
        self.logger.info('Found optimal TDAC values for selected pixels.')
        self.lt.close()
        
        self.logger.info('Applying TDAC mask and running final threshold scan...')
        self.ts = ThresholdScan()
        self.scans.append(self.ts)
        self.ts.logger.addHandler(self.fh)
        self.ts.start(**kwargs)
        self.ts.analyze(create_pdf=False)
        
        self.logger.info('Tuning finished!')

        
    def analyze(self, level='preliminary'):
        with tb.open_file(self.output_filename + '_interpreted.h5', mode='r', title=self.run_name) as infile:
            self.logger.info('Creating plots...')
            output_pdf = PdfPages(self.output_filename + '_interpreted.pdf')
            attributes = yaml.load(infile.root.local_threshold_tuning.Hits.attrs.kwargs)
            dacs = yaml.load(infile.root.local_threshold_tuning.Hits.attrs.dacs)
            chip_id = infile.root.local_threshold_tuning.Hits.attrs.chip_id
            software_version = infile.root.local_threshold_tuning.Hits.attrs.software_version
            
            mask = np.ones((400,192), dtype=bool)
        
            maskfile = attributes.get('maskfile', None)
            disable = attributes.get('disable', None)
            
            if maskfile:
                try:
                    with tb.open_file(maskfile, 'r') as mask_infile:
                        mask = mask_infile.root.disable_mask[:]
                except tb.exceptions.NoSuchNodeError:
                    self.logger.debug('Specified maskfile does not include a disable_mask!')
                    pass
                    
            if disable:
                for tup in disable:
                    mask[tup[0], tup[1]] = False

            mask = np.invert(mask)

            mask[:attributes.get('start_column', 0),:] = True
            mask[attributes.get('stop_column', 400):,:] = True
            mask[:,:attributes.get('start_row', 0)] = True
            mask[:,attributes.get('stop_row', 192):] = True
            
            plotting.write_parameters(scan_id=self.scan_id, run_name=self.run_name, attributes=attributes, dacs=dacs, sw_ver=software_version, chip_id=chip_id, level=level, filename=output_pdf)
            plotting.plot_tot(hist=infile.root.local_threshold_tuning.HistTot[:].T, chip_id=chip_id, level=level, filename=output_pdf)
            plotting.plot_relative_bcid(hist=infile.root.local_threshold_tuning.HistRelBCID[:].T, chip_id=chip_id, level=level, filename=output_pdf)
            
            plotting.plot_distribution(infile.root.local_threshold_tuning.ThresholdMap[:].T,
                                           start_column=attributes['start_column'],
                                           stop_column=attributes['stop_column'],
                                           plot_range=range(16),
                                           title='TDAC distribution',
                                           x_axis_title='TDAC',
                                           y_axis_title='#',
                                           chip_id=chip_id,
                                           level=level, 
                                           filename=output_pdf)
            
            plot_range = [v-attributes.get('VCAL_MED') for v in range(attributes.get('VCAL_HIGH_start'),
                                                attributes.get('VCAL_HIGH_stop'),
                                                attributes.get('VCAL_HIGH_step'))]
            plotting.plot_scurves(scurves=infile.root.threshold_scan.HistSCurve[:].T,
                                  scan_parameters=plot_range,
                                  start_column=attributes['start_column'],
                                  stop_column=attributes['stop_column'],
                                  start_row=attributes['start_row'],
                                  stop_row=attributes['stop_row'],
                                  scan_parameter_name='$\Delta$ VCAL',
                                  chip_id=chip_id,
                                  level=level, 
                                  filename=output_pdf)
            plotting.plot_distribution(infile.root.threshold_scan.ThresholdMap[:].T,
                                       start_column=attributes['start_column'],
                                       stop_column=attributes['stop_column'],
                                       plot_range=plot_range,
                                       title='Threshold distribution',
                                       x_axis_title='$\Delta$ VCAL',
                                       y_axis_title='#',
                                       chip_id=chip_id,
                                       level=level, 
                                       filename=output_pdf)
            plotting.plot_occupancy(hist=np.ma.masked_array(infile.root.threshold_scan.ThresholdMap[:].T, mask.T),
                                    z_label='Threshold',
                                    title='Threshold',
                                    chip_id=chip_id,
                                    level=level,
                                    filename=output_pdf)
            plotting.plot_distribution(infile.root.threshold_scan.NoiseMap[:].T,
                                               start_column=attributes['start_column'],
                                               stop_column=attributes['stop_column'],
                                               title='Noise distribution',
                                               x_axis_title='$\Delta$ VCAL',
                                               y_axis_title='#',
                                               chip_id=chip_id,
                                               level=level,
                                               filename=output_pdf)
            plotting.plot_occupancy(hist=np.ma.masked_array(infile.root.threshold_scan.NoiseMap[:].T, mask.T),
                                    z_label='Noise',
                                    z_max='median',
                                    title='Noise',
                                    chip_id=chip_id,
                                    level=level,
                                    filename=output_pdf)
            
        output_pdf.close()
        shutil.copyfile(self.output_filename + '_interpreted.pdf', os.path.join(os.path.split(self.output_filename)[0], 'last_scan.pdf'))
        self.logger.info('All done!')


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    mtt = MetaThrTuning()
    mtt.start(**configuration)
    mtt.analyze()
