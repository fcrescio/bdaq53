#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan sends triggers without injection
    into enabled pixels to identify noisy pixels.
'''

import zlib # workaround
import yaml

import tables as tb
import numpy as np

from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'          : 1.25,
    'VDDD'          : 1.25,
    'VDDA_cur_lim'  : 0.7,
    'VDDD_cur_lim'  : 0.7,

    # Scan parameters
    'start_column'  : 0,
    'stop_column'   : 400,
    'start_row'     : 0,
    'stop_row'      : 192,
    'maskfile'      : None,
    
    'n_triggers'    : 1e6
    }


class NoiseOccScan(ScanBase):
    scan_id = "noise_occupancy_scan"

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192,
             n_triggers=1e6, wait_cycles=10, **kwargs):
        '''
        Noise occupancy scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.

        n_triggers : int
            Number of triggers to send.
        wait_cycles : int
            Time to wait in between trigger packages in units of sync commands.
        '''
        
        self.n_pixels = (stop_column-start_column)*(stop_row-start_row)
        n_triggers = int(n_triggers)
        if n_triggers > 50000:
            steps = range(0, n_triggers, 50000)
        else:
            steps = [n_triggers]
            
        trigger_data = self.chip.send_trigger(trigger=0b1111, write=False)
        trigger_data += self.chip.write_sync_01(write=False)*wait_cycles
        
        self.logger.info('Starting scan...')     
        pbar = tqdm(total=len(steps)*50000)        
        with self.readout():
            for _ in steps:
                self.chip.write_command(trigger_data, repetitions=50000)
                pbar.update(50000)
                    
        pbar.close()
        self.logger.info('Scan finished')


    def analyze(self, create_pdf=True, create_mask_file=True):
        with analysis.AnalyzeRawData(raw_data_file=self.output_filename + '.h5', create_pdf=create_pdf) as a:
            a.analyze_data()
            if create_pdf:
                a.logger.info('Creating selected plots...')
                a.create_parameter_page()
                a.create_occupancy_map()
                a.create_tot_plot()
                a.create_rel_bcid_plot()
                if a.cluster_hits:
                    a.create_cluster_size_plot()
                    a.create_cluster_tot_plot()
                    a.create_cluster_shape_plot()
            
        with tb.open_file(self.output_filename + '_interpreted.h5') as in_file:
            hits = in_file.root.Hits[:]
        self.disable_mask = np.ones((400,192), dtype=bool)
        for hit in hits:
            self.disable_mask[hit['col'], hit['row']] = False
        if create_mask_file: self.save_disable_mask(update=False)
            
        n_hit_pixels = np.count_nonzero(np.concatenate(np.invert(self.disable_mask)))
        
        return n_hit_pixels, round(float(n_hit_pixels)/float(self.n_pixels)*100.,2), len(hits), self.disable_mask


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = NoiseOccScan()
    scan.start(**configuration)
    print scan.analyze()[:2]
