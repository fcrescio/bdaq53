#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#


'''
    This meta script tunes the chip to the lowest possible threshold by running a noise occupancy scan in a loop,
    increasing the local threshold of noisy pixels until they are quiet or disabled. The tuning is finished if either
    a specified amount of pixels is disabled or the TDAC distribution is centered.
'''

import zlib # Workaround
import os
import yaml

import numpy as np

from bdaq53.meta_scan_base import MetaScanBase
from bdaq53.scans.scan_noise_occupancy import NoiseOccScan
from bdaq53.scans.scan_threshold import ThresholdScan


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'            : 1.25,
    'VDDD'            : 1.25,
    'VDDA_cur_lim'    : 0.7,
    'VDDD_cur_lim'    : 0.7,
    
    # Scan parameters
    'start_column'    : 128,
    'stop_column'     : 264,
    'start_row'       : 0,
    'stop_row'        : 192,
    'maskfile'        : None,
    
    # Noise occupancy scan parameters
    'n_triggers'      : 1e5,
    
    # Threshold tuning parameters
    'VTH_name'        : 'Vthreshold_LIN',
    'VTH_start'       : 380,
    'VTH_step'        : 1,
    'limit'           : 1,
    'vth_offset'      : 5,

    # Final threshold scan parameters
    'VCAL_MED'        : 500,
    'VCAL_HIGH_start' : 500,
    'VCAL_HIGH_stop'  : 700,
    'VCAL_HIGH_step'  : 5
    }


class MetaNoiseTuning(MetaScanBase):
    scan_id = 'meta_threshold_noise_tuning'

    def scan(self, limit=1, vth_offset=5, **kwargs):
        mf = kwargs.get('maskfile', None)
        if mf is not None:
            self.maskfile = mf
        else:
            self.maskfile = os.path.join(self.working_dir, '_'.join(self.run_name.split('_')[:2]) + '_mask.h5')
        
        def remove_scanfiles():
            os.remove(nos.output_filename + '.log')
            os.remove(nos.output_filename + '.h5')
            os.remove(nos.output_filename + '_interpreted.h5')
            os.remove(nos.output_filename + '_interpreted.pdf')
        
        
        start_column, stop_column = kwargs.get('start_column', 0), kwargs.get('stop_column', 400)
        start_row, stop_row = kwargs.get('start_row', 0), kwargs.get('stop_row', 192)
        n_pixels = (stop_column - start_column)*(stop_row-start_row)
        
        self.disable_mask = np.ones((400, 192), dtype=bool)
        self.TDAC_mask = np.zeros((400, 192), dtype=int)
        self.TDAC_mask[start_column:stop_column,start_row:stop_row] = 15
        
        for vth in range(kwargs.get('VTH_start'), 0, -1*kwargs.get('VTH_step', 1)):
            self.vth = vth
            kwargs[kwargs.get('VTH_name')] = self.vth
            self.logger.info('Set %s to %i' % (kwargs.get('VTH_name'), vth))
            
            iteration = 0
            while True:
                iteration += 1
                kwargs['TDAC'] = self.TDAC_mask
                kwargs['disable'] = self.disable_mask
                
                nos = NoiseOccScan()
                nos.logger.addHandler(self.fh)    
                nos.start(**kwargs)
                _, _, n_total_hits, hit_pixels = nos.analyze(create_pdf=True, create_mask_file=False)
                nos.close()
                remove_scanfiles()
                
                for col in range(start_column, stop_column):
                    for row in range(kwargs.get('start_row', 0), kwargs.get('stop_row', 192)):
                        if hit_pixels[col, row] == False:
                            if self.TDAC_mask[col, row] > 0:
                                self.TDAC_mask[col, row] -= 1
                            else:
                                self.disable_mask[col, row] = False
                
                this_tdac = self.TDAC_mask[start_column:stop_column,:].flatten()
                mean_tdac = np.mean(this_tdac)
                bc = np.bincount(this_tdac, minlength=16)
                n_disabled_pixels = (self.disable_mask == False).sum()
                r_disabled_pixels = float(n_disabled_pixels)/float(n_pixels)*100.
                np.set_printoptions(linewidth = 120)
                self.logger.info('TDAC distribution in iteration %i at %s = %i:\n%s\nMean TDAC is %1.2f\n%i pixels are disabled. This corresponds to %1.2f%% of %i total pixels.' % (
                    iteration, kwargs.get('VTH_name'), self.vth, str(bc), mean_tdac, n_disabled_pixels, r_disabled_pixels,n_pixels))
                
                if n_total_hits < 0.005*n_pixels:
                    break 
            
            self.save_disable_mask(update=False)
            self.save_tdac_mask()
            
            if r_disabled_pixels >= limit or mean_tdac < 7:
                break
        
        
        self.result_vth = self.vth + vth_offset
        kwargs[kwargs.get('VTH_name')] = self.result_vth
        kwargs['TDAC'] = self.TDAC_mask
        kwargs['disable'] = self.disable_mask
        
        self.th = ThresholdScan()
        self.th.logger.addHandler(self.fh)
        self.th.start(**kwargs)
        self.th.analyze()
        self.th.close()
        
    
    def analyze(self):
        return self.result_vth


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    mnt = MetaNoiseTuning()
    mnt.start(**configuration)
    mnt.analyze()
    