#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from __future__ import print_function
import multiprocessing as mp

import numba
import logging

import numpy as np

from numba import njit
from tqdm import tqdm


from bdaq53.register_utils import RD53ARegisterParser

# Word defines
USERK_FRAME_ID = 0x01000000
HEADER_ID = 0x00010000


@njit
def translate_mapping(core_column, region, pixel_id):
    '''
        Translate mapping between raw data format (core column and region) and absolute column and row.

        ----------
        Parameters:
            core_column : int
                Core column number [0:49]
            region : int
                Region in the core column [0:383]
            pixel_id : int
                Pixel in the region [0:3]

        Returns:
            column : int
                Absolute column number [0:399]
            row : int
                Absolute row number [0:191]
    '''

    if pixel_id > 3:
        raise ValueError('pixel_id cannot be larger than 3!')
    column = core_column * 8 + pixel_id
    if region % 2 == 1:
        column += 4
    row = int(region / 2)

    return column, row


@njit
def build_event(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, hit_buffer, hit_buffer_i, min_bcid, scan_param_id):
    '''
        Fill result data structures
        called at the end of an event.
    '''

    # Copy hits from buffer to result data structure
    for i in range(hit_buffer_i):
        hits[data_out_i] = hit_buffer[i]
        # Set minimum BCID of event hits
        hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] - min_bcid
        if hits[data_out_i]['rel_bcid'] >= 32: # TODO: Workaround for wrong bcid (on first event?)
            hits[data_out_i]['rel_bcid'] = 0

        hits[data_out_i]['scan_param_id'] = scan_param_id
        # Fill histograms
        hist_rel_bcid[hits[data_out_i]['rel_bcid']] += 1
        hist_occ[hits[data_out_i]['col'], hits[data_out_i]['row']] += 1
        hist_tot[hits[data_out_i]['tot']] += 1
        hist_scurve[hits[data_out_i]['col']*192+hits[data_out_i]['row'], scan_param_id] += 1

        data_out_i += 1

    return data_out_i


@njit
def add_hits(data_word, hit_buffer, hit_buffer_i, event_number, trg_id, bcid):
    multicol = (data_word >> 26) & 0x3f
    region = (data_word >> 16) & 0x3ff

    for i in range(4):
        col, row = translate_mapping(multicol, region, i)
        tot = (data_word >> i * 4) & 0xf
        
        if col < 400 and row < 192:
            if tot != 255 and tot != 15:
                hit_buffer[hit_buffer_i]['bcid'] = bcid
                hit_buffer[hit_buffer_i]['event_number'] = event_number
                hit_buffer[hit_buffer_i]['trigger_id'] = trg_id
                hit_buffer[hit_buffer_i]['col'] = col
                hit_buffer[hit_buffer_i]['row'] = row
                hit_buffer[hit_buffer_i]['tot'] = tot
                hit_buffer_i += 1
        else:
            # TODO: handle and log corrupted data
            print('Corrupted data detected!')
            pass

    return hit_buffer_i


@njit
def interpret_data(rawdata, hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, scan_param_id):
    event_number = 0
    data_cnt = 0
    data_header = False
    data_out_i = 0
    this_bcid = 0
    prev_bcid = -1
    
    # Hit buffer to store actual event hits needed to set parameters calculated at the end of an event
    hit_buffer_i = 0  # Hit buffer index
    hit_buffer = np.empty_like(hits) # possible speedup with np.empty_like(hits)
  
    # Per event variables
    trig_id = 0
    min_bcid = 0
  
    for word in rawdata:
        if (word & USERK_FRAME_ID):  # skip USER_K frame
            continue
              
        if (word & HEADER_ID):  # data header
            data_header = True
            data_cnt = 0
  
        # reassemble full data word from two FPGA data words
        if data_cnt == 0:
            data_word = word & 0xffff
        else:
            data_word = data_word << 16 | word & 0xffff
  
        if data_cnt >= 1:  # full data word
            if (data_header == True):  # data word is a header
                data_header = False
                
                this_bcid = data_word & 0x7fff
                trig_id = (data_word >> 20) & 0x1f
                
                # TODO: corruption check if trig_id is always prev_trig_id +1 
                
                if ((prev_bcid+1) & 0x7fff) != this_bcid:    # when bcid changes for more then 1 -> new event
                    data_out_i = build_event(hits, data_out_i,  # Result hit array to be filled
                                        hist_occ, hist_tot, hist_rel_bcid, hist_scurve,  # Result histograms to be filled
                                        hit_buffer, hit_buffer_i,
                                        min_bcid, scan_param_id)
                    
                    event_number += 1
                          
                    # Reset per event variables
                    hit_buffer_i = 0
                    min_bcid = this_bcid
                
                prev_bcid = this_bcid
                    
            else:  # data word is hit data
                hit_buffer_i = add_hits(data_word,
                                        hit_buffer, hit_buffer_i,
                                        event_number,
                                        trig_id,
                                        this_bcid)
            data_cnt = 0
        else:
            data_cnt += 1
  
    # TODO: Add last event data (to be removed when chunking is implemented
    data_out_i = build_event(hits, data_out_i,  # Result hit array to be filled
                            hist_occ, hist_tot, hist_rel_bcid, hist_scurve,  # Result histograms to be filled
                            hit_buffer, hit_buffer_i,
                            min_bcid, scan_param_id)
  
    return data_out_i


def interpret_userk_data(rawdata):
    userk_data = np.zeros(shape=rawdata.shape[0],
                          dtype={'names': ['AuroraKWord', 'Status', 'Data1', 'Data1_AddrFlag', 'Data1_Addr', 'Data1_Data', 'Data0', 'Data0_AddrFlag', 'Data0_Addr', 'Data0_Data'],
                                 'formats': ['uint8', 'uint8', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16']})
    userk_word_cnt = 0
    userk_block_cnt = 0
    userk_framelength = 2
    block_temp = 0
    userk_data_i = 0

    for word in rawdata:
        if (word & USERK_FRAME_ID):
            if userk_word_cnt == 0:
                userk_word = word & 0xffff
            else:
                userk_word = userk_word << 16 | word & 0xffff

            if userk_block_cnt == 2 * userk_framelength - 1:
                block_temp = (userk_word & 0x3) << 32 | block_temp
                userk_block = userk_word >> 2
                Data1 = userk_block & 0x7ffffff
                Data0 = (block_temp >> 8) & 0x7ffffff
                userk_data[userk_data_i]['AuroraKWord'] = block_temp & 0xff
                userk_data[userk_data_i]['Status'] = (userk_block >> 30) & 0xf
                userk_data[userk_data_i]['Data1'] = Data1
                userk_data[userk_data_i]['Data1_AddrFlag'] = (Data1 >> 25) & 0x1
                userk_data[userk_data_i]['Data1_Addr'] = (Data1 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data1_Data'] = (Data1 >> 0) & 0xffff
                userk_data[userk_data_i]['Data0'] = Data0
                userk_data[userk_data_i]['Data0_AddrFlag'] = (Data0 >> 25) & 0x1
                userk_data[userk_data_i]['Data0_Addr'] = (Data0 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data0_Data'] = (Data0 >> 0) & 0xffff
                userk_data_i += 1

                userk_block_cnt = 0

            else:
                userk_block_cnt += 1

            # interpret received packet as user k
            if userk_word_cnt >= userk_framelength - 1:
                userk_word_cnt = 0
                block_temp = userk_word
            else:
                userk_word_cnt += 1

    return userk_data[:userk_data_i]


def process_userk(userk_in):
    rp = RD53ARegisterParser()
    
    userk_out = np.zeros(userk_in.shape[0] * 2, dtype={'names': ['Address', 'Name', 'Data'],
                                                       'formats': ['uint16', 'S30', 'uint16']})
    userk_counter = 0
    for i in userk_in:
        AuroraKWord = i['AuroraKWord']
        if AuroraKWord == 0:
            userk_out[userk_counter]['Address'] = i['Data1_Addr']
            userk_out[userk_counter]['Name'] = str(
                rp.get_name(hex(i['Data1_Addr'])))
            userk_out[userk_counter]['Data'] = i['Data1']

            userk_out[userk_counter + 1]['Address'] = i['Data0_Addr']
            userk_out[
                userk_counter + 1]['Name'] = str(rp.get_name(hex(i['Data0_Addr'])))
            userk_out[userk_counter + 1]['Data'] = i['Data0']

            userk_counter = userk_counter + 2

        if AuroraKWord == 1:
            userk_out[userk_counter]['Address'] = i['Data1_Addr']
            userk_out[userk_counter]['Name'] = str(
                rp.get_name(hex(i['Data1_Addr'])))
            userk_out[userk_counter]['Data'] = i['Data1']

            userk_counter = userk_counter + 1

        if AuroraKWord == 2:
            userk_out[userk_counter]['Address'] = i['Data0_Addr']
            userk_out[userk_counter]['Name'] = str(
                rp.get_name(hex(i['Data0_Addr'])))
            userk_out[userk_counter]['Data'] = i['Data0']

            userk_counter = userk_counter + 1

    userk_out = userk_out[:userk_counter]

    for i in userk_out:
        logging.debug('Address= %s \t\t\tData= %s \t\t\tName= %s ', hex(i['Address']), hex(i['Data']), str(rp.get_name(hex(i['Address']))))

    return userk_out


#     def process_monitor(self, userk_in):
#         monitor_out = np.zeros(userk_in.shape[0] * 2, dtype={'names': ['Address', 'Data'],
#                                                              'formats': ['uint16', 'uint16']})
#         monitor_counter = 0
#
#         for i in userk_in:
#             AuroraKWord = i['AuroraKWord']
#             if AuroraKWord == 3:
#                 monitor_out[monitor_counter]['Address'] = i['Data1_Addr']
#                 monitor_out[monitor_counter]['Data'] = i['Data1']
#
#                 monitor_out[monitor_counter + 1]['Address'] = i['Data0_Addr']
#                 monitor_out[monitor_counter + 1]['Data'] = i['Data0']
#
#                 monitor_counter = monitor_counter + 2
#
#             if AuroraKWord == 2:
#                 monitor_out[monitor_counter]['Address'] = i['Data1_Addr']
#                 monitor_out[monitor_counter]['Data'] = i['Data1']
#
#                 monitor_counter = monitor_counter + 1
#
#             if AuroraKWord == 1:
#                 monitor_out[monitor_counter]['Address'] = i['Data0_Addr']
#                 monitor_out[monitor_counter]['Data'] = i['Data0']
#
#                 monitor_counter = monitor_counter + 1
#
#         monitor_out = monitor_out[:monitor_counter]
#
#         for i in monitor_out:
#             logger.info(
#                 'Address= %s \t\t\tData= %s ', hex(i['Address']), hex(i['Data']))
#
#         return monitor_out


def init_outs(n_hits, n_scan_params):
    hist_occ = np.zeros(shape=(400, 192), dtype=np.uint32)
    hist_tot = np.zeros(shape=(16), dtype=np.uint32)
    hist_rel_bcid = np.zeros(shape=(32), dtype=np.uint32)
    hist_scurve = np.zeros(shape=(76800, n_scan_params), dtype=np.uint32)
    hits = np.zeros(shape=n_hits,
                    dtype={'names': ['event_number', 'trigger_id', 'bcid', 'rel_bcid', 'col', 'row', 'tot', 'scan_param_id'],
                           'formats': ['int64', 'uint8', 'uint16', 'uint8', 'uint16', 'uint16', 'uint8', 'uint8']})

    return hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve


@njit(locals={'cluster_shape': numba.int64})
def calc_cluster_shape(cluster_array):
    '''Boolean 8x8 array to number.
    '''
    cluster_shape = 0
    indices_x, indices_y = np.nonzero(cluster_array)
    for index in np.arange(indices_x.size):
        cluster_shape += 2**xy2d_morton(indices_x[index], indices_y[index])
    return cluster_shape


@njit(numba.int64(numba.uint32, numba.uint32))
def xy2d_morton(x, y):
    ''' Tuple to number.

    See: https://stackoverflow.com/questions/30539347/
         2d-morton-code-encode-decode-64bits
    '''
    x = (x | (x << 16)) & 0x0000FFFF0000FFFF
    x = (x | (x << 8)) & 0x00FF00FF00FF00FF
    x = (x | (x << 4)) & 0x0F0F0F0F0F0F0F0F
    x = (x | (x << 2)) & 0x3333333333333333
    x = (x | (x << 1)) & 0x5555555555555555

    y = (y | (y << 16)) & 0x0000FFFF0000FFFF
    y = (y | (y << 8)) & 0x00FF00FF00FF00FF
    y = (y | (y << 4)) & 0x0F0F0F0F0F0F0F0F
    y = (y | (y << 2)) & 0x3333333333333333
    y = (y | (y << 1)) & 0x5555555555555555

    return x | (y << 1)


def imap_bar(func, args, n_processes=None):
    ''' Apply function func to interable args with progressbar
    '''
    p = mp.Pool(n_processes)
    res_list = []
    with tqdm(total=len(args)) as pbar:
        for _, res in tqdm(enumerate(p.imap(func, args))):
            pbar.update()
            res_list.append(res)
    pbar.close()
    p.close()
    p.join()
    return res_list


def get_threshold(x, y, n_injections):
    ''' Fit less approximation of threshold from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''
    qmin, qmax = x.min(), x.max()
    n = x.shape[0]
    M = y.sum()
    d = (qmax - qmin) / n
    return qmax - d * M / n_injections


def get_noise(x, y, n_injections):
    ''' Fit less approximation of noise from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''
    qmin, qmax = x.min(), x.max()
    n = x.shape[0]
    M = y.sum()
    d = (qmax - qmin) / n
    mu = qmax - d * M / n_injections
    mu1 = y[x < mu].sum()
    mu2 = (n_injections - y[x > mu]).sum()
    return d * (mu1 + mu2) / n_injections * np.sqrt(np.pi / 2.)

