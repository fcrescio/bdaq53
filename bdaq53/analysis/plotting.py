#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib # workaround
import numpy as np
import math
import logging
from collections import OrderedDict

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import colors, cm
from matplotlib.backends.backend_pdf import PdfPages


loglevel = logging.getLogger('RD53A').getEffectiveLevel()

logger = logging.getLogger('Plotting')
logger.setLevel(loglevel)


def gauss(x, *p):
    amplitude, mu, sigma = p
    return amplitude * np.exp(- (x - mu)**2.0 / (2.0 * sigma**2.0))
#     mu, sigma = p
#     return 1.0 / (sigma * np.sqrt(2.0 * np.pi)) * np.exp(- (x - mu)**2.0 / (2.0 * sigma**2.0))


def add_text(fig, chip_id, level, qualitative, internal):
    ax_text = fig.add_subplot(GridSpec(10, 1)[0], zorder=999)
    ax_text.axis('off')
    if qualitative:
        ax_text.text(0, 1.5, 'RD53A qualitative', fontsize=12, color='#07529a')
        ax_text.text(0.7, 1.5, 'Chip S/N: 0x0000', fontsize=12, color='#07529a')
    else:
        ax_text.text(0, 1.5, 'RD53A %s' % (level), fontsize=12, color='#07529a')
        ax_text.text(0.7, 1.5, 'Chip S/N: %s' % (chip_id), fontsize=12, color='#07529a')
    if internal:
        ax_text.text(0, 0, 'RD53 Internal', fontsize=16, color='r', rotation=45, bbox=dict(boxstyle='round', facecolor='white', edgecolor='red', alpha=0.7))

def write_parameters(scan_id, run_name, chip_id, attributes, dacs, sw_ver, level='preliminary', filename=None):
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.axis('off')

    ax.text(0.01, 1, 'This is a bdaq53 %s for chip %s.\nThese results are %s.\nRun name: %s' % (scan_id, chip_id, level, run_name), fontsize=10)
    ax.text(0.9, -0.11, 'Software version: %s' % (sw_ver), fontsize=3)
    
    tb_dict = OrderedDict(sorted(dacs.items()))
    attr = OrderedDict(sorted(attributes.items()))
    for key, value in attr.items():
        if key == 'disable_mask':
            continue
        tb_dict[key] = str(value) + '*'
    
    tb_list = []
    for i in range(0,len(tb_dict.keys()),2):
        try:
            key1 = tb_dict.keys()[i]
            value1 = tb_dict[key1]
            try:
                key2 = tb_dict.keys()[i+1]
                value2 = tb_dict[key2]
            except:
                key2 = ''
                value2 = ''
            tb_list.append([key1, value1, '', key2, value2])
        except:
            pass
        
    widths = [0.2,0.2, 0.1, 0.2,0.2]
    labels = ['Parameter', 'Value', '', 'Parameter', 'Value']
    table = ax.table(cellText=tb_list, colWidths=widths, colLabels=labels, cellLoc='left', loc='center')
    table.scale(0.8, 0.8)
    
    for key, cell in table.get_celld().items():
        row, col = key
        if row == 0:
            cell.set_color('grey')
        if col == 2:
            cell.set_color('white')
 
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)


def plot_1d_hist(hist, yerr=None, title=None, x_axis_title=None, y_axis_title=None, x_ticks=None, color='r', plot_range=None, log_y=False, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    fig = Figure()
    FigureCanvas(fig)
    add_text(fig, chip_id=chip_id, level=level, qualitative=qualitative, internal=internal)
    ax = plt.subplot2grid((10,1), (0,1), rowspan=9, fig=fig)
    
    hist = np.array(hist)
    if plot_range is None:
        plot_range = range(0, len(hist))
    plot_range = np.array(plot_range)
    plot_range = plot_range[plot_range < len(hist)]
    if yerr is not None:
        ax.bar(x=plot_range, height=hist[plot_range], color=color, align='center', yerr=yerr)
    else:
        ax.bar(x=plot_range, height=hist[plot_range], color=color, align='center')
    ax.set_xlim((min(plot_range) - 0.5, max(plot_range) + 0.5))
    
    ax.set_title(title)
    if x_axis_title is not None:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is not None:
        ax.set_ylabel(y_axis_title)
    if x_ticks is not None:
        ax.set_xticks(plot_range)
        ax.set_xticklabels(x_ticks)
        ax.tick_params(which='both', labelsize=8)
    if np.allclose(hist, 0.0):
        ax.set_ylim((0, 1))
    else:
        if log_y:
            ax.set_yscale('log')
    ax.grid(True)
    
    if qualitative:
        ax.xaxis.set_major_formatter(plt.NullFormatter())
        ax.xaxis.set_minor_formatter(plt.NullFormatter())
        ax.yaxis.set_major_formatter(plt.NullFormatter())
        ax.yaxis.set_minor_formatter(plt.NullFormatter())
    
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)
        

def plot_tot(hist, title=None, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    if title is None:
        if qualitative:
            title = 'Time-over-Threshold distribution'
        else:
            title = ('Time-over-Threshold distribution' + r' ($\Sigma$ = %d)' % (np.sum(hist)))
    plot_1d_hist(hist=hist, title=title, log_y=True, plot_range=range(0, 16), x_axis_title='ToT code [25 ns]', y_axis_title='#', color='b', chip_id=chip_id, level=level, qualitative=qualitative, internal=internal, filename=filename)


def plot_relative_bcid(hist, title=None, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    if title is None:
        if qualitative:
            title = 'Relative BCID'
        else:
            title = ('Relative BCID' + r' ($\Sigma$ = %d)' % (np.sum(hist))) 
    plot_1d_hist(hist=hist, title=title, log_y=True, plot_range=range(0,32), x_axis_title='Relative BCID [25 ns]', y_axis_title='#', chip_id=chip_id, level=level, qualitative=qualitative, internal=internal, filename=filename)


def plot_cl_size(hist, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    ''' Create 1D cluster size plot w/wo log y-scale '''
    plot_1d_hist(hist=hist, title='Cluster size',
                 log_y=False, plot_range=range(0, 10),
                 x_axis_title='Cluster size',
                 y_axis_title='#', chip_id=chip_id,
                 level=level, qualitative=qualitative,
                 internal=internal, filename=filename)
    plot_1d_hist(hist=hist, title='Cluster size (log)',
                 log_y=True, plot_range=range(0, 100),
                 x_axis_title='Cluster size',
                 y_axis_title='#', chip_id='0x0000',
                 level='preliminary', qualitative=qualitative,
                 internal=internal, filename=filename)


def plot_cl_tot(hist, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    ''' Create 1D cluster size plot w/wo log y-scale '''
    plot_1d_hist(hist=hist, title='Cluster ToT',
                 log_y=False, plot_range=range(0, 96),
                 x_axis_title='Cluster ToT [25 ns]',
                 y_axis_title='#', chip_id=chip_id,
                 level=level, qualitative=qualitative,
                 internal=internal, filename=filename)


def plot_cl_shape(hist, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    ''' Create a histogram with selected cluster shapes '''
    x = np.arange(12)
    fig = Figure()
    _ = FigureCanvas(fig)
    add_text(fig, chip_id=chip_id, level=level, qualitative=qualitative, internal=internal)
    ax = plt.subplot2grid((10,1), (0,1), rowspan=19, fig=fig)
    selected_clusters = hist[[1, 3, 5, 6, 9, 13, 14, 7, 11, 19, 261, 15]]
    ax.bar(x, selected_clusters, align='center')
    ax.xaxis.set_ticks(x)
    fig.subplots_adjust(bottom=0.2)
    ax.set_xticklabels([u"\u2004\u2596",
                        # 2 hit cluster, horizontal
                        u"\u2597\u2009\u2596",
                        # 2 hit cluster, vertical
                        u"\u2004\u2596\n\u2004\u2598",
                        u"\u259e",  # 2 hit cluster
                        u"\u259a",  # 2 hit cluster
                        u"\u2599",  # 3 hit cluster, L
                        u"\u259f",  # 3 hit cluster
                        u"\u259b",  # 3 hit cluster
                        u"\u259c",  # 3 hit cluster
                        # 3 hit cluster, horizontal
                        u"\u2004\u2596\u2596\u2596",
                        # 3 hit cluster, vertical
                        u"\u2004\u2596\n\u2004\u2596\n\u2004\u2596",
                        # 4 hit cluster
                        u"\u2597\u2009\u2596\n\u259d\u2009\u2598"])
    ax.set_title('Cluster shapes')
    ax.set_xlabel('Cluster shape')
    ax.set_ylabel('#')
    ax.grid(True)
    ax.set_yscale('log')
    ax.set_ylim(ymin=1e-1)
    
    if qualitative:
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
    
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)


def plot_occupancy(hist, title='Occupancy', z_label='#', z_max=None, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    if z_max == 'median':
        z_max = 2 * np.ma.median(hist)
    elif z_max == 'maximum' or z_max is None:
        z_max = np.ma.max(hist)
    if z_max < 1 or hist.all() is np.ma.masked:
        z_max = 1.0

    fig = Figure()
    FigureCanvas(fig)
    add_text(fig, chip_id=chip_id, level=level, qualitative=qualitative, internal=internal)
    ax = plt.subplot2grid((10,1), (0,1), rowspan=19, fig=fig)
    ax.set_adjustable('box-forced')
    extent = [0.5, 400.5, 192.5, 0.5]
    bounds = np.linspace(start=0, stop=z_max, num=255, endpoint=True)
    cmap = cm.get_cmap('plasma')
    cmap.set_bad('w', 1.0)
    norm = colors.BoundaryNorm(bounds, cmap.N)

    im = ax.imshow(hist, interpolation='none', aspect='equal', cmap=cmap, norm=norm, extent=extent)  # TODO: use pcolor or pcolormesh
    ax.set_ylim((192.5, 0.5))
    ax.set_xlim((0.5, 400.5))
    if qualitative:
        ax.set_title(title)
    else:
        ax.set_title(title + r' ($\Sigma$ = {0})'.format((0 if hist.all() is np.ma.masked else np.ma.sum(hist))))
    ax.set_xlabel('Column')
    ax.set_ylabel('Row')

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cb = fig.colorbar(im, cax=cax, ticks=np.linspace(start=0, stop=z_max, num=9, endpoint=True))
    cb.set_label(z_label)
    
    if qualitative:
        ax.xaxis.set_major_formatter(plt.NullFormatter())
        ax.xaxis.set_minor_formatter(plt.NullFormatter())
        ax.yaxis.set_major_formatter(plt.NullFormatter())
        ax.yaxis.set_minor_formatter(plt.NullFormatter())
        cb.formatter = plt.NullFormatter()
        cb.update_ticks()
    
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)
        
        
def plot_scurves(scurves, scan_parameters, start_column, stop_column, start_row, stop_row, title='S-curves', ylabel='Occupancy', scan_parameter_name=None, min_x=None, max_x=None, extend_bin_width=True, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    max_occ = np.max(scurves) + 5 #Maybe?
    x_bins = scan_parameters#np.arange(-0.5, max(scan_parameters) + 1.5)
    y_bins = np.arange(-0.5, max_occ + 0.5)
    n_pixel = (stop_column - start_column) * (stop_row - start_row)
    
    param_count = scurves.shape[0]
    hist = np.empty([param_count, max_occ], dtype=np.uint32)
    
    for param in range(param_count):
        hist[param] = np.bincount(scurves[param,start_column*192+start_row:(stop_column-1)*192+stop_row], minlength=max_occ)

    fig = Figure()
    FigureCanvas(fig)
    add_text(fig, chip_id=chip_id, level=level, qualitative=qualitative, internal=internal)
    ax = plt.subplot2grid((10,1), (0,1), rowspan=19, fig=fig)
    fig.patch.set_facecolor('white')
    cmap = cm.get_cmap('cool')
    if np.allclose(hist, 0.0) or hist.max() <= 1:
        z_max = 1.0
    else:
        z_max = hist.max()
    # for small z use linear scale, otherwise log scale
    if z_max <= 10.0:
        bounds = np.linspace(start=0.0, stop=z_max, num=255, endpoint=True)
        norm = colors.BoundaryNorm(bounds, cmap.N)
    else:
        bounds = np.linspace(start=1.0, stop=z_max, num=255, endpoint=True)
        norm = colors.LogNorm()

    im = ax.pcolormesh(x_bins, y_bins, hist.T, norm=norm, rasterized=True)
    
    if z_max <= 10.0:
        cb = fig.colorbar(im, ticks=np.linspace(start=0.0, stop=z_max, num=min(11, math.ceil(z_max) + 1), endpoint=True), fraction=0.04, pad=0.05)
    else:
        cb = fig.colorbar(im, fraction=0.04, pad=0.05)
    cb.set_label("#")
    ax.set_title(title + ' for %d pixel(s)' % (n_pixel))
    if scan_parameter_name is None:
        ax.set_xlabel('Scan parameter')
    else:
        ax.set_xlabel(scan_parameter_name)
    ax.set_ylabel(ylabel)
    
    if qualitative:
        ax.xaxis.set_major_formatter(plt.NullFormatter())
        ax.xaxis.set_minor_formatter(plt.NullFormatter())
        ax.yaxis.set_major_formatter(plt.NullFormatter())
        ax.yaxis.set_minor_formatter(plt.NullFormatter())
        cb.formatter = plt.NullFormatter()
        cb.update_ticks()
    
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)

def plot_distribution(data, start_column, stop_column, plot_range=None, x_axis_title=None, y_axis_title=None, title=None, chip_id='0x0000', level='preliminary', qualitative=False, internal=False, filename=None):
    data = data[:,start_column:stop_column]
    try:
        if plot_range is None:
            diff = np.amax(data) - np.amin(data)
            if (np.amax(data)) > np.median(data)*5:
                plot_range = np.arange(np.amin(data), np.median(data)*5, diff/100.)
            else:
                plot_range = np.arange(np.amin(data), np.amax(data)+diff/100., diff/100.)
        
        tick_size = plot_range[1] - plot_range[0]
        
        hist, bins =  np.histogram(np.ravel(data), bins=plot_range)
        
        bin_centres = (bins[:-1] + bins[1:]) / 2
        p0 = (np.amax(hist), np.mean(bins), (max(plot_range)-min(plot_range))/3)
        
        try:
            coeff, _ = curve_fit(gauss, bin_centres, hist, p0=p0)
        except:
            coeff = None
            logger.error('Gauss fit failed!')
            
        if coeff is not None:
            points = np.linspace(min(plot_range), max(plot_range), 500)
            gau = gauss(points, *coeff)
        
        fig = Figure()
        FigureCanvas(fig)
        add_text(fig, chip_id=chip_id, level=level, qualitative=qualitative, internal=internal)
        ax = plt.subplot2grid((10,1), (0,1), rowspan=19, fig=fig)
        
        ax.bar(bins[:-1], hist, width=tick_size, align='edge')
        if coeff is not None:
            ax.plot(points, gau, "r-", label='Normal distribution')
    
        ax.set_xlim((min(plot_range) - 0.5, max(plot_range) + 0.5))
        ax.set_title(title)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid(True)
        
        if coeff is not None and not qualitative:
            textright = '$\mu=%.2f$\n$\sigma=%.2f$' % (abs(coeff[1]), abs(coeff[2]))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax.text(0.85, 0.9, textright, transform=ax.transAxes, fontsize=8, verticalalignment='top', bbox=props)
        
        if qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
        
        if not filename:
            fig.show()
        elif isinstance(filename, PdfPages):
            filename.savefig(fig)
        else:
            fig.savefig(filename)
    except:
        logger.error("Plot failed!")


def fit_LIN(x, y):
    if len(x)<15:
        raise ValueError('Too few measured points. 15 or more points are expected.')
    
    fitrange=int(len(x)*0.4)
    convergence=False
    
    while not (convergence):
        right = np.polyfit(x[-fitrange:-1], y[-fitrange:-1], 1, full = True)
        left = np.polyfit(x[0:fitrange], y[0:fitrange], 1, full = True)
        if abs(right[0][0]) > abs(left[0][0]):
            x=x[1:]
            y=y[1:]
        else:
            x=x[:-1]
            y=y[:-1]
        if ((abs(right[0][0]/left[0][0]))<1.01 and abs(right[0][0]/left[0][0])>0.99):
            convergence=True
    return right[0][0], right[0][1], x, y


def plot_adc(data, title=None, x_axis_title=None, y_axis_title=None, y_range_max=None, chip_id='0x0000', level='preliminary', internal=False, qualitative=False, filename=None):
    x=[]
    y=[]
    for elements in data:
        x=np.append(x,elements[0])
        y=np.append(y,elements[1])
    
    fig = Figure()
    FigureCanvas(fig)
    add_text(fig, chip_id=chip_id, level=level, qualitative=qualitative, internal=internal)
    ax = plt.subplot2grid((10,1), (0,1), rowspan=19, fig=fig)
    ax.set_title(str(title))
    ax.plot(x, y,'bx')
    ax2 = ax.twinx()
    ax2.plot(x, y*0.00019, linestyle='None')
    ax2.set_ylabel('ADC [V]')

    try:
        m,b,_,_=fit_LIN(x,y)
        ax.plot(x, x * m + b, 'r-')
        ax2.plot(x, (x * m + b) * 0.00019, linestyle='None')
        if y[0]<y[-1]:
            if b>=0:
                ax.text(np.min(x), np.max(y), ('f(x) = ' + str(round(m,2))  + ' x + ' + str(round(b,2))))
            else:
                ax.text(np.min(x), np.max(y), ('f(x) = ' + str(round(m,2))  + ' x - ' + str(abs(round(b,2)))))
        else:
            if b>=0:
                ax.text(0, 0, ('f(x) = ' + str(round(m,2)) + ' x + ' + str(round(b,2))))
            else:
                ax.text(0, 0, ('f(x) = ' + str(round(m,2)) + ' x - ' + str(round(b,2))))

    except:
        logger.info('Linear Fit Failed')

    if x_axis_title is None or qualitative:
        ax.set_xlabel('Register Setting [LSB]')
    else:
        ax.set_xlabel(x_axis_title)
    if y_axis_title is None:
        ax.set_ylabel('Voltmeter [V]')
    else:
        ax.set_ylabel(y_axis_title)
    if y_range_max is not None:
        ax.set_ylim(0, y_range_max)

    
    if qualitative:
        ax.xaxis.set_major_formatter(plt.NullFormatter())
        ax.xaxis.set_minor_formatter(plt.NullFormatter())
        ax.yaxis.set_major_formatter(plt.NullFormatter())
        ax.yaxis.set_minor_formatter(plt.NullFormatter())
    
    if not filename:
        fig.show()
    elif isinstance(filename, PdfPages):
        filename.savefig(fig)
    else:
        fig.savefig(filename)



if __name__ == "__main__":
    pass
