#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Script to convert the raw data and to plot all histograms
'''

from __future__ import division

import zlib  # workaround for matplotlib segmentation fault
import shutil
import os.path
import numpy as np
import logging
import yaml
import multiprocessing as mp
import tables as tb

from tqdm import tqdm
from functools import partial
from scipy.optimize import curve_fit
from scipy.special import erf
from matplotlib.backends.backend_pdf import PdfPages
from pixel_clusterizer.clusterizer import HitClusterizer

from bdaq53.analysis import analysis_utils
from bdaq53.analysis import plotting


loglevel = logging.getLogger('RD53A').getEffectiveLevel()

np.warnings.filterwarnings('ignore')


def scurve(x, A, mu, sigma):
    return 0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


def zcurve(x, A, mu, sigma):
    return -0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


def fit_scurve(scurve_data, scan_param_range, n_injections, sigma_0, invert_x):
    '''
        Fit one pixel data with Scurve.
        Has to be global for the multiprocessing module.

        Returns:
            (mu, sigma, chi2/ndf)
    '''
    scurve_data = np.array(scurve_data)
    # Only fit data that is fittable
    if np.all(scurve_data == 0):
        return (0., 0., 0.)
    if n_injections and scurve_data.max() < 0.2 * n_injections:
        return (0., 0., 0.)

    # Calculate data errors, Binomial errors
    yerr = np.sqrt(scurve_data *
                   (1. - scurve_data.astype(np.float) / n_injections))
    # Set minimum error != 0, needed for fit minimizers
    # Set arbitrarly to error of 0.5 injections
    min_err = np.sqrt(0.5 - 0.5 / n_injections)
    yerr[yerr < min_err] = min_err
    # Additional hits not following fit model set high error
    sel_bad = scurve_data > n_injections
    yerr[sel_bad] = (scurve_data - n_injections)[sel_bad]

    # Calculate threshold start value:
    mu = analysis_utils.get_threshold(x=scan_param_range, y=scurve_data,
                                      n_injections=n_injections)

    # Set fit start values
    p0 = [n_injections, mu, sigma_0]
    try:
        if invert_x:
            popt = curve_fit(f=zcurve, xdata=scan_param_range,
                             ydata=scurve_data, p0=p0, sigma=yerr,
                             absolute_sigma=True if np.any(yerr) else False)[0]
            chi2 = np.sum((scurve_data - zcurve(scan_param_range, *popt))**2)
        else:
            popt = curve_fit(f=scurve, xdata=scan_param_range,
                             ydata=scurve_data, p0=p0, sigma=yerr,
                             absolute_sigma=True if np.any(yerr) else False,
                             method='lm')[0]
            chi2 = np.sum((scurve_data - scurve(scan_param_range, *popt))**2)
    except RuntimeError:  # fit failed
        return (0., 0., 0.)

#     import matplotlib.pyplot as plt
#     plt.clf()
#     x = np.linspace(min(scan_param_range),
#                     max(scan_param_range), 1000)
#     plt.plot(x, scurve(x, *p0), '--')
#     plt.plot(x, scurve(x, *popt), '-')
#     plt.errorbar(scan_param_range, scurve_data, yerr=yerr, fmt='o')
#     plt.show()

    return (popt[1], popt[2], chi2 / (scurve_data.shape[0] - 3 - 1))


class AnalyzeRawData(object):
    """
        A class to analyze RD53A raw data
    """

    def __init__(self, raw_data_file=None, analyzed_data_file=None,
                 create_pdf=True, scan_parameter_name=None,
                 level='preliminary', qualitative=False, internal=False,
                 cluster_hits=False, chunk_size=100000000):
        ''' 
            Parameters
            ----------
            raw_data_file : string or tuple, list
                A string or a list of strings with the raw data file name(s). File ending (.h5)
                does not not have to be set.
            analyzed_data_file : string
                The file name of the output analyzed data file. File ending (.h5)
                Does not have to be set.
            create_pdf : boolean
                Creates interpretation plots into one PDF file. Only active if raw_data_file is given.
            scan_parameter_name : string or iterable
                The name/names of scan parameter(s) to be used during analysis. If not set the scan parameter
                table is used to extract the scan parameters. Otherwise no scan parameter is set.
            level : string
                The level of results. E.g. 'Preliminary'
            cluster_hits : boolean
                Create cluster table, histograms and plots
        '''
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(loglevel)
        
        for h in logging.getLogger('RD53A').handlers:
            if isinstance(h, logging.FileHandler):
                self.logger.addHandler(h)

        self.raw_data_file = raw_data_file
        self.analyzed_data_file = analyzed_data_file
        self.create_pdf = create_pdf
        self.scan_parameter_name = scan_parameter_name
        self.level = level
        self.qualitative = qualitative
        self.internal = internal
        self.cluster_hits = cluster_hits
        self.chunk_size = chunk_size
        self.output_pdf = None
        
        if not os.path.isfile(raw_data_file):
            raise IOError('Raw data file does not exist.')

        if not self.analyzed_data_file:
            self.analyzed_data_file = raw_data_file[:-3] + '_interpreted.h5'
            
        if self.create_pdf:
            self.output_pdf_path = self.analyzed_data_file[:-3] + '.pdf'
            self.output_pdf = PdfPages(self.output_pdf_path)

        # Hit buffer for word at event alignment
        self.last_words = None
        
        self._setup_clusterizer()


    def __enter__(self):
        return self


    def __exit__(self, *exc_info):
        if self.output_pdf is not None and isinstance(self.output_pdf, PdfPages):
            self.logger.info( 'Closing output PDF file: %s', str(self.output_pdf._file.fh.name))
            self.output_pdf.close()
            shutil.copyfile(self.output_pdf_path, os.path.join(os.path.split(self.output_pdf_path)[0], 'last_scan.pdf'))
        
        for h in self.logger.handlers:
            if isinstance(h, logging.FileHandler):
                self.logger.removeHandler(h)
                

    def _setup_clusterizer(self):
        ''' Define data structure and settings for hit clusterizer package '''
        # Define all field names and data types
        hit_fields = {'event_number': 'event_number',
                      'trigger_id': 'trigger_id',
                      'bcid': 'bcid',
                      'rel_bcid': 'frame',
                      'col': 'column',
                      'row': 'row',
                      'tot': 'charge',
                      'scan_param_id': 'scan_param_id'
                      }
        hit_dtype = np.dtype([('event_number', '<i8'),
                              ('trigger_id', 'u1'),
                              ('bcid', '<u2'),
                              ('rel_bcid', 'u1'),
                              ('col', '<u2'),
                              ('row', '<u2'),
                              ('tot', 'u1'),
                              ('scan_param_id', 'u1')])
        cluster_fields = {'event_number': 'event_number',
                          'column': 'column',
                          'row': 'row',
                          'size': 'n_hits',
                          'id': 'ID',
                          'tot': 'charge',
                          'scan_param_id': 'scan_param_id',
                          'seed_col': 'seed_column',
                          'seed_row': 'seed_row',
                          'mean_col': 'mean_column',
                          'mean_row': 'mean_row'}
        self.cluster_dtype = np.dtype([('event_number', '<i8'),
                                       ('id', '<u2'),
                                       ('size', '<u2'),
                                       ('tot', '<u2'),
                                       ('seed_col', '<u1'),
                                       ('seed_row', '<u2'),
                                       ('mean_col', '<f4'),
                                       ('mean_row', '<f4'),
                                       ('dist_col', '<u4'),
                                       ('dist_row', '<u4'),
                                       ('cluster_shape', '<i8'),
                                       ('scan_param_id', 'u1')])

        if self.cluster_hits:  # Allow analysis without clusterizer installed
            # Define end of cluster function to calculate cluster shape
            # and cluster distance in column and row direction
            def end_of_cluster_function(hits, clusters, cluster_size,
                                        cluster_hit_indices, cluster_index,
                                        cluster_id, charge_correction,
                                        noisy_pixels, disabled_pixels,
                                        seed_hit_index):
                hit_arr = np.zeros((15, 15), dtype=np.bool_)
                center_col = hits[cluster_hit_indices[0]].column
                center_row = hits[cluster_hit_indices[0]].row
                hit_arr[7, 7] = 1
                min_col = hits[cluster_hit_indices[0]].column
                max_col = hits[cluster_hit_indices[0]].column
                min_row = hits[cluster_hit_indices[0]].row
                max_row = hits[cluster_hit_indices[0]].row
                for i in cluster_hit_indices[1:]:
                    if i < 0:  # Not used indeces = -1
                        break
                    diff_col = np.int32(hits[i].column - center_col)
                    diff_row = np.int32(hits[i].row - center_row)
                    if np.abs(diff_col) < 8 and np.abs(diff_row) < 8:
                        hit_arr[7 + hits[i].column - center_col,
                                7 + hits[i].row - center_row] = 1
                    if hits[i].column < min_col:
                        min_col = hits[i].column
                    if hits[i].column > max_col:
                        max_col = hits[i].column
                    if hits[i].row < min_row:
                        min_row = hits[i].row
                    if hits[i].row > max_row:
                        max_row = hits[i].row

                if max_col - min_col < 8 and max_row - min_row < 8:
                    # Make 8x8 array
                    col_base = 7 + min_col - center_col
                    row_base = 7 + min_row - center_row
                    cluster_arr = hit_arr[col_base:col_base + 8,
                                          row_base:row_base + 8]
                    # Finally calculate cluster shape
                    # uint64 desired, but numexpr and others limited to int64
                    if cluster_arr[7, 7] == 1:
                        cluster_shape = np.int64(-1)
                    else:
                        cluster_shape = np.int64(analysis_utils.calc_cluster_shape(cluster_arr))
                else:
                    # Cluster is exceeding 8x8 array
                    cluster_shape = np.int64(-1)

                clusters[cluster_index].cluster_shape = cluster_shape
                clusters[cluster_index].dist_col = max_col - min_col + 1
                clusters[cluster_index].dist_row = max_row - min_row + 1

            # Initialize clusterizer with custom hit/cluster fields
            self.clz = HitClusterizer(
                hit_fields=hit_fields,
                hit_dtype=hit_dtype,
                cluster_fields=cluster_fields,
                cluster_dtype=self.cluster_dtype,
                min_hit_charge=0,
                max_hit_charge=13,
                column_cluster_distance=3,
                row_cluster_distance=3,
                frame_cluster_distance=2,
                ignore_same_hits=True)

            # Set end_of_cluster function for shape and distance calculation
            self.clz.set_end_of_cluster_function(end_of_cluster_function)

    def _range_of_parameter(self, meta_data):
        _, index = np.unique(meta_data['scan_param_id'], return_index=True)
        start = meta_data[index]['index_start']
        stop = np.append(start[:-1] + np.diff(start), meta_data[-1]['index_stop'])
        return np.column_stack((start, stop))


    def _words_per_parameter(self, par_range, data, chunk_size):
        for scan_param, (start, stop) in enumerate(par_range):
            for i in range(start, stop, chunk_size): #This can be dangerous. Should recognize event boundaries.
                yield scan_param, data[i:min(i+chunk_size, stop)]
                
                
    def analyze_adc_data(self):
        self.logger.info('Analyzing register data...')
        
        with tb.open_file(self.raw_data_file) as in_file:
            meta_data = in_file.root.meta_data[:]
            par_range = self._range_of_parameter(meta_data)
            userk_out = np.zeros(par_range.shape[0], dtype={'names': ['RegValue', 'AdcValue'],
                                                              'formats': ['int16', 'int16']})

            for scan_param_id, words in self._words_per_parameter(par_range, in_file.root.raw_data, self.chunk_size):
                userk_out[scan_param_id]['AdcValue'] = analysis_utils.process_userk(analysis_utils.interpret_userk_data(words))[-1]['Data']
                userk_out[scan_param_id]['RegValue'] = meta_data[scan_param_id][5]

            with tb.open_file(self.analyzed_data_file, 'w', title=in_file.title) as out_file:
                register_table = out_file.create_table(out_file.root, 'Hits', userk_out,
                                                  title='register_data',
                                                  filters=tb.Filters(complib='blosc',
                                                                     complevel=5,
                                                                     fletcher32=False))
                
                register_table.attrs.scan_id = in_file.root.meta_data.attrs.scan_id
                register_table.attrs.run_name = in_file.root.meta_data.attrs.run_name
                register_table.attrs.kwargs = in_file.root.meta_data.attrs.kwargs
                register_table.attrs.dacs = in_file.root.meta_data.attrs.dacs
                register_table.attrs.chip_id = in_file.root.meta_data.attrs.chip_id
                register_table.attrs.software_version = in_file.root.meta_data.attrs.software_version

            
    def analyze_data(self):
        '''
        '''
        self.logger.info('Analyzing data...')
        with tb.open_file(self.raw_data_file) as in_file:
            n_words = in_file.root.raw_data.shape[0]
            meta_data = in_file.root.meta_data[:]
            par_range = self._range_of_parameter(meta_data)
            hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve = analysis_utils.init_outs(n_hits=self.chunk_size, n_scan_params=max(np.unique(meta_data['scan_param_id']))+1)
            
            with tb.open_file(self.analyzed_data_file, 'w', title=in_file.title) as out_file:
                hit_table = out_file.create_table(out_file.root, name='Hits',
                                                  description=hits.dtype,
                                                  title='hit_data',
                                                  filters=tb.Filters(complib='blosc',
                                                                     complevel=5,
                                                                     fletcher32=False))
                
                hit_table.attrs.scan_id = in_file.root.meta_data.attrs.scan_id
                hit_table.attrs.run_name = in_file.root.meta_data.attrs.run_name
                hit_table.attrs.kwargs = in_file.root.meta_data.attrs.kwargs
                hit_table.attrs.dacs = in_file.root.meta_data.attrs.dacs
                hit_table.attrs.chip_id = in_file.root.meta_data.attrs.chip_id
                hit_table.attrs.software_version = in_file.root.meta_data.attrs.software_version
 
                if self.cluster_hits:
                    cluster_table = out_file.create_table(
                                        out_file.root, name='Cluster',
                                        description=self.cluster_dtype,
                                        title='Cluster',
                                        filters=tb.Filters(complib='blosc',
                                                           complevel=5,
                                                           fletcher32=False))
                    hist_cs_size = np.zeros(shape=(100, ), dtype=np.uint16)
                    hist_cs_tot = np.zeros(shape=(100, ), dtype=np.uint16)
                    hist_cs_shape = np.zeros(shape=(300, ), dtype=np.int16)
 
                pbar = tqdm(total=n_words)
                for scan_param_id, words in self._words_per_parameter(par_range, in_file.root.raw_data, self.chunk_size):
                    n_hits = analysis_utils.interpret_data(rawdata=words,
                                                           hits=hits,
                                                           hist_occ=hist_occ,
                                                           hist_tot=hist_tot,
                                                           hist_rel_bcid=hist_rel_bcid,
                                                           hist_scurve=hist_scurve,
                                                           scan_param_id=scan_param_id)
                    hit_table.append(hits[:n_hits])
                    hit_table.flush()

                    if self.cluster_hits:
                        _, cluster = self.clz.cluster_hits(hits[:n_hits])
                        cluster_table.append(cluster)
                        hist_cs_size += np.bincount(cluster['size'], minlength=100)[:100].astype(np.uint16)
                        hist_cs_tot += np.bincount(cluster['tot'], minlength=100)[:100].astype(np.uint16)
                        hist_cs_shape += np.bincount(cluster['cluster_shape'][np.logical_and(cluster['cluster_shape']>0 , cluster['cluster_shape']<300)], minlength=300)[:300].astype(np.uint16)

                    pbar.update(words.shape[0])
                pbar.close()
        
        self._create_additional_hit_data(hist_occ, hist_tot, hist_rel_bcid, hist_scurve)
        if self.cluster_hits:
            self._create_additional_cluster_data(hist_cs_size, hist_cs_tot, hist_cs_shape)
            
        return hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve
            

    def _create_additional_hit_data(self, hist_occ, hist_tot, hist_rel_bcid, hist_scurve):
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            attributes = yaml.load(out_file.root.Hits.attrs.kwargs)
            scan_id = out_file.root.Hits.attrs.scan_id
            
            out_file.create_carray(out_file.root,
                                   name='HistOcc',
                                   title='Occupancy Histogram',
                                   obj=hist_occ,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistTot',
                                   title='ToT Histogram',
                                   obj=hist_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistRelBCID',
                                   title='Relativ BCID Histogram',
                                   obj=hist_rel_bcid,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

            if scan_id in ['threshold_scan',
                           'global_threshold_tuning',
                           'local_threshold_tuning']:
                n_injections = attributes.get('n_injections', None)
                if n_injections is None:
                    raise RuntimeError('Numbers of injections unknown!')
                out_file.create_carray(out_file.root,
                                       name='HistSCurve',
                                       title='Scurve Data',
                                       obj=hist_scurve,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

                if scan_id == 'threshold_scan': 
                    scan_param_range = [v-attributes.get('VCAL_MED') for v in range(attributes.get('VCAL_HIGH_start'), attributes.get('VCAL_HIGH_stop'), attributes.get('VCAL_HIGH_step'))]
                    self.threshold_map, self.noise_map, self.chi2_map = self.fit_scurves_multithread(hist_scurve, scan_param_range, n_injections)
                elif scan_id == 'global_threshold_tuning':
                    scan_param_range = range(attributes.get('VTH_start'),
                                        attributes.get('VTH_stop'),
                                        -1*attributes.get('VTH_step'))
                    self.threshold_map, self.noise_map, self.chi2_map = self.fit_scurves_multithread(hist_scurve, scan_param_range, n_injections=n_injections, invert_x=True)
                elif scan_id == 'local_threshold_tuning':
                    scan_param_range = range(16)
                    threshold_map, noise_map, self.chi2_map = self.fit_scurves_multithread(hist_scurve, scan_param_range, n_injections=n_injections, invert_x=False)
                    self.threshold_map, self.noise_map = np.around(threshold_map), np.around(noise_map)
                    self.threshold_map = self.scurve_simple_mean(hist_scurve)

                out_file.create_carray(out_file.root,
                                       name='ThresholdMap',
                                       title='Threshold Map',
                                       obj=self.threshold_map,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))
                out_file.create_carray(out_file.root,
                                       name='NoiseMap',
                                       title='Noise Map',
                                       obj=self.noise_map,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

                out_file.create_carray(out_file.root,
                                       name='Chi2Map',
                                       title='Chi2 / ndf Map',
                                       obj=self.chi2_map,
                                       filters=tb.Filters(complib='blosc',
                                                          complevel=5,
                                                          fletcher32=False))

    def _create_additional_cluster_data(self, hist_cs_size, hist_cs_tot, hist_cs_shape):
        '''
            Store cluster histograms in analyzed data file
        '''
        with tb.open_file(self.analyzed_data_file, 'r+') as out_file:
            out_file.create_carray(out_file.root,
                                   name='HistClusterSize',
                                   title='Cluster Size Histogram',
                                   obj=hist_cs_size,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterTot',
                                   title='Cluster ToT Histogram',
                                   obj=hist_cs_tot,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))
            out_file.create_carray(out_file.root,
                                   name='HistClusterShape',
                                   title='Cluster Shape Histogram',
                                   obj=hist_cs_shape,
                                   filters=tb.Filters(complib='blosc',
                                                      complevel=5,
                                                      fletcher32=False))

    def fit_scurves_multithread(self, scurves, scan_param_range,
                                n_injections=None, invert_x=False):
        self.logger.info("Start S-curve fit on %d CPU core(s)", mp.cpu_count())

        scurves = np.array(scurves)
        scan_param_range = np.array(scan_param_range)

        # Calculate noise median for fit start value
        sigmas = []
        for curve in scurves:
            # TODO: n_injections is not defined, can this happen?
            if not n_injections:
                n_injections = curve.max()
            # Calculate from pixels with valid data (maximum = n_injections)
            if curve.max() == n_injections:
                sigma = analysis_utils.get_noise(x=scan_param_range,
                                                 y=curve,
                                                 n_injections=n_injections)
                sigmas.append(sigma)
        sigma_0 = np.median(sigmas)

#         for curve in scurves:
#             if max(curve) > n_injections / 2.:
#                 fit_scurve(curve, scan_param_range, n_injections, sigma_0, invert_x)

        partialfit_scurve = partial(fit_scurve,
                                    scan_param_range=scan_param_range,
                                    n_injections=n_injections,
                                    sigma_0=sigma_0,
                                    invert_x=invert_x)

        result_list = analysis_utils.imap_bar(partialfit_scurve,
                                              scurves.tolist())
        result_array = np.array(result_list)
        self.logger.info("S-curve fit finished")

        thr = result_array[:, 0]
        sig = result_array[:, 1]
        chi2ndf = result_array[:, 2]
        thr2D = np.reshape(thr, (50 * 8, 192))
        sig2D = np.reshape(sig, (50 * 8, 192))
        chi2ndf2D = np.reshape(chi2ndf, (50 * 8, 192))
        return thr2D, sig2D, chi2ndf2D

    def scurve_simple_mean(self, scurve_data, max_occ=100):
        result_list = []
        for scurve in scurve_data:
            min_diff, min_idx = max_occ, 0
            for i, v in enumerate(scurve):
                diff = abs(v - max_occ/2)
                if diff < min_diff:
                    min_diff = diff 
                    min_idx = i
            
            if max(scurve) < max_occ/2:
                min_idx = 15
            if min(scurve) > max_occ/2:
                min_idx = 0
            result_list.append(min_idx)
            
        thr = np.array(result_list)
        thr2D = np.reshape(thr, (50*8, 192))
        return thr2D
    
    def mask_disabled_pixels(self, attributes):
        mask = np.ones((400,192), dtype=bool)
        
        maskfile = attributes.get('maskfile', None)
        disable = attributes.get('disable', None)
        
        if maskfile:
            try:
                with tb.open_file(maskfile, 'r') as infile:
                    mask = infile.root.disable_mask[:]
            except tb.exceptions.NoSuchNodeError:
                self.logger.debug('Specified maskfile does not include a disable_mask!')
                pass
                
        if disable:
            for tup in disable:
                mask[tup[0], tup[1]] = False

        mask = np.invert(mask)

        mask[:attributes.get('start_column', 0),:] = True
        mask[attributes.get('stop_column', 400):,:] = True
        mask[:,:attributes.get('start_row', 0)] = True
        mask[:,attributes.get('stop_row', 192):] = True
            
        return mask
    
    
    ''' User callable plotting functions '''
    def create_parameter_page(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            dacs = yaml.load(in_file.root.Hits.attrs.dacs)
            run_name = in_file.root.Hits.attrs.run_name
            scan_id = in_file.root.Hits.attrs.scan_id
            software_version = in_file.root.Hits.attrs.software_version
            chip_id = in_file.root.Hits.attrs.chip_id
            
        if not self.qualitative:
            plotting.write_parameters(scan_id, run_name, chip_id, attributes, dacs, software_version, level=self.level, filename=self.output_pdf)

    def create_occupancy_map(self, **kwargs):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            mask = self.mask_disabled_pixels(yaml.load(in_file.root.Hits.attrs.kwargs))

            plotting.plot_occupancy(hist=np.ma.masked_array(in_file.root.HistOcc[:].T, mask.T), chip_id=chip_id, level=self.level, qualitative=self.qualitative, internal=self.internal, filename=self.output_pdf, **kwargs)
            
    def create_tot_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            plotting.plot_tot(hist=in_file.root.HistTot[:].T, chip_id=chip_id, level=self.level, qualitative=self.qualitative, internal=self.internal, filename=self.output_pdf)
            
    def create_rel_bcid_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            plotting.plot_relative_bcid(hist=in_file.root.HistRelBCID[:].T, chip_id=chip_id, level=self.level, qualitative=self.qualitative, internal=self.internal, filename=self.output_pdf)
            
    def create_scurves_plot(self, scan_parameter_name=None):
        if scan_parameter_name is None:
            scan_parameter_name = 'Scan parameter'
        with tb.open_file(self.analyzed_data_file) as in_file:
            scan_id = in_file.root.Hits.attrs.scan_id
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            chip_id = in_file.root.Hits.attrs.chip_id
            if scan_id == 'threshold_scan':
                scan_parameter_range = [v-attributes.get('VCAL_MED') for v in range(attributes.get('VCAL_HIGH_start'),
                                        attributes.get('VCAL_HIGH_stop'),
                                        attributes.get('VCAL_HIGH_step'))]
                scan_parameter_name = '$\Delta$ VCAL'
            elif scan_id == 'global_threshold_tuning':
                scan_parameter_range = range(attributes.get('VTH_start'),
                                        attributes.get('VTH_stop'),
                                        -1*attributes.get('VTH_step'))
            elif scan_id == 'local_threshold_tuning':
                scan_parameter_range = range(16)
                
            plotting.plot_scurves(scurves=in_file.root.HistSCurve[:].T,
                                  scan_parameters=scan_parameter_range,
                                  start_column=attributes['start_column'],
                                  stop_column=attributes['stop_column'],
                                  start_row=attributes['start_row'],
                                  stop_row=attributes['stop_row'],
                                  scan_parameter_name=scan_parameter_name,
                                  chip_id=chip_id,
                                  level=self.level,
                                  qualitative=self.qualitative,
                                  internal=self.internal,
                                  filename=self.output_pdf)
                
    def create_threshold_plot(self, scan_parameter_name=None):
        if scan_parameter_name is None:
            scan_parameter_name = 'Scan parameter'
        with tb.open_file(self.analyzed_data_file) as in_file:
            scan_id = in_file.root.Hits.attrs.scan_id
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            chip_id = in_file.root.Hits.attrs.chip_id
            if scan_id == 'threshold_scan':
                plot_range = [v-attributes.get('VCAL_MED') for v in range(attributes.get('VCAL_HIGH_start'),
                                        attributes.get('VCAL_HIGH_stop'),
                                        attributes.get('VCAL_HIGH_step'))]
                scan_parameter_name = '$\Delta$ VCAL'
            elif scan_id == 'global_threshold_tuning':
                plot_range = range(attributes.get('VTH_start'),
                                        attributes.get('VTH_stop'),
                                        -1*attributes.get('VTH_step'))
            elif scan_id == 'local_threshold_tuning':
                plot_range = range(16)
            plotting.plot_distribution(in_file.root.ThresholdMap[:].T,
                                   start_column=attributes['start_column'],
                                   stop_column=attributes['stop_column'],
                                   plot_range=plot_range,
                                   title='Threshold distribution',
                                   x_axis_title=scan_parameter_name,
                                   y_axis_title='#',
                                   chip_id=chip_id,
                                   level=self.level,
                                   qualitative=self.qualitative,
                                   internal=self.internal,
                                   filename=self.output_pdf)
            
    def create_noise_plot(self, scan_parameter_name=None):
        if scan_parameter_name is None:
            scan_parameter_name = 'Scan parameter'
        with tb.open_file(self.analyzed_data_file) as in_file:
            attributes = yaml.load(in_file.root.Hits.attrs.kwargs)
            chip_id = in_file.root.Hits.attrs.chip_id
            plotting.plot_distribution(in_file.root.NoiseMap[:].T,
                                       start_column=attributes['start_column'],
                                       stop_column=attributes['stop_column'],
                                       title='Noise distribution',
                                       x_axis_title=scan_parameter_name,
                                       y_axis_title='#',
                                       chip_id=chip_id,
                                       level=self.level,
                                       qualitative=self.qualitative,
                                       internal=self.internal,
                                       filename=self.output_pdf)
            
    def create_threshold_map(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            mask = self.mask_disabled_pixels(yaml.load(in_file.root.Hits.attrs.kwargs))
            # Mask not converged fits (chi2 = 0)
            sel = in_file.root.Chi2Map[:] > 0.
            mask[~sel] = True

            plotting.plot_occupancy(hist=np.ma.masked_array(in_file.root.ThresholdMap[:].T, mask.T),
                                    z_label='Threshold',
                                    title='Threshold',
                                    chip_id=chip_id,
                                    level=self.level,
                                    qualitative=self.qualitative,
                                    internal=self.internal,
                                    filename=self.output_pdf)
            
    def create_noise_map(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            mask = self.mask_disabled_pixels(yaml.load(in_file.root.Hits.attrs.kwargs))
            # Mask not converged fits (chi2 = 0)
            sel = in_file.root.Chi2Map[:] > 0.
            mask[~sel] = True
            
            plotting.plot_occupancy(hist=np.ma.masked_array(in_file.root.NoiseMap[:].T, mask.T),
                                    z_label='Noise',
                                    z_max='median',
                                    title='Noise',
                                    chip_id=chip_id,
                                    level=self.level,
                                    qualitative=self.qualitative,
                                    internal=self.internal,
                                    filename=self.output_pdf)

    def create_chi2_map(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            mask = self.mask_disabled_pixels(yaml.load(in_file.root.Hits.attrs.kwargs))
            chi2 = in_file.root.Chi2Map[:]
            # Mask not converged fits (chi2 = 0)
            sel = chi2 > 0.
            mask[~sel] = True

            plotting.plot_occupancy(hist=np.ma.masked_array(chi2.T, mask.T),
                                    z_label='Chi2/ndf.',
                                    z_max='median',
                                    title='Chi2 over ndf of S-Curve fits',
                                    chip_id=chip_id,
                                    level=self.level,
                                    filename=self.output_pdf)

    def create_cluster_size_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            plotting.plot_cl_size(in_file.root.HistClusterSize[:], chip_id=chip_id, level=self.level, qualitative=self.qualitative, internal=self.internal, filename=self.output_pdf)
            
    def create_cluster_tot_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            plotting.plot_cl_tot(in_file.root.HistClusterTot[:], chip_id=chip_id, level=self.level, qualitative=self.qualitative, internal=self.internal, filename=self.output_pdf)
            
    def create_cluster_shape_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            plotting.plot_cl_shape(in_file.root.HistClusterShape[:], chip_id=chip_id, level=self.level, qualitative=self.qualitative, internal=self.internal, filename=self.output_pdf)
            
    def create_dac_linearity_plot(self):
        with tb.open_file(self.analyzed_data_file) as in_file:
            chip_id = in_file.root.Hits.attrs.chip_id
            reg_name = yaml.load(in_file.root.Hits.attrs.kwargs)['DAC']
            plotting.plot_adc(in_file.root.Hits, str(reg_name)+' Linearity', y_axis_title='ADC [LSB]', x_axis_title=reg_name,
                              chip_id=chip_id, level=self.level, qualitative=self.qualitative, internal=self.internal, filename = self.output_pdf)



    

if __name__ == "__main__":
    pass
